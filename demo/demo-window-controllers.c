/*
 * demo-window-controllers.c
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "demo-window-controllers"

#include "demo-window-private.h"

static gboolean
on_scroll_event_scroll_cb (DemoWindow               *self,
                           gdouble                   dx,
                           gdouble                   dy,
                           GtkEventControllerScroll *scroll)
{
  g_return_val_if_fail (DEMO_IS_WINDOW (self), FALSE);
  g_return_val_if_fail (GTK_IS_EVENT_CONTROLLER_SCROLL (scroll), FALSE);

  return forger_context_handle_event_scroll (self->context, scroll, dx, dy);
}

static void
on_drag_gesture_drag_update_cb (DemoWindow     *self,
                                gdouble         offset_x,
                                gdouble         offset_y,
                                GtkGestureDrag *drag_gesture)
{
  g_return_if_fail (DEMO_IS_WINDOW (self));
  g_return_if_fail (GTK_IS_GESTURE_DRAG (drag_gesture));

  forger_context_handle_event_drag_update (self->context, drag_gesture,
                                           offset_x, offset_y);
}

static void
on_click_gesture_pressed_cb (DemoWindow      *self,
                             gint             n_press,
                             gdouble          x,
                             gdouble          y,
                             GtkGestureClick *click_gesture)
{
  g_return_if_fail (DEMO_IS_WINDOW (self));
  g_return_if_fail (GTK_IS_GESTURE_CLICK (click_gesture));

  forger_context_handle_event_click_pressed (self->context,
                                             click_gesture,
                                             n_press, x, y);
}

void
_demo_window_attach_controllers (DemoWindow *self)
{
  g_assert (DEMO_IS_WINDOW (self));

  self->drag_gesture = (GtkGestureDrag *)gtk_gesture_drag_new ();
  gtk_gesture_single_set_button (GTK_GESTURE_SINGLE (self->drag_gesture),
                                 GDK_BUTTON_MIDDLE);

  self->scroll_event = (GtkEventControllerScroll *)gtk_event_controller_scroll_new (GTK_EVENT_CONTROLLER_SCROLL_VERTICAL);
  self->click_gesture = (GtkGestureClick *)gtk_gesture_click_new ();

  gtk_widget_add_controller (GTK_WIDGET (self->canvas), GTK_EVENT_CONTROLLER (self->drag_gesture));
  gtk_widget_add_controller (GTK_WIDGET (self->canvas), GTK_EVENT_CONTROLLER (self->scroll_event));
  gtk_widget_add_controller (GTK_WIDGET (self->canvas), GTK_EVENT_CONTROLLER (self->click_gesture));

  g_signal_connect_swapped (self->drag_gesture, "drag-update", G_CALLBACK (on_drag_gesture_drag_update_cb), self);
  g_signal_connect_swapped (self->scroll_event, "scroll", G_CALLBACK (on_scroll_event_scroll_cb), self);
  g_signal_connect_swapped (self->click_gesture, "pressed", G_CALLBACK (on_click_gesture_pressed_cb), self);
}

void
_demo_window_detach_controllers (DemoWindow *self)
{
  g_assert (DEMO_IS_WINDOW (self));

  gtk_widget_remove_controller (GTK_WIDGET (self->canvas), GTK_EVENT_CONTROLLER (self->drag_gesture));
  gtk_widget_remove_controller (GTK_WIDGET (self->canvas), GTK_EVENT_CONTROLLER (self->click_gesture));
  gtk_widget_remove_controller (GTK_WIDGET (self->canvas), GTK_EVENT_CONTROLLER (self->scroll_event));
}
