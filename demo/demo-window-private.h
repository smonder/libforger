/*
 * demo-window-private.h
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <glib/gi18n.h>
#include "demo-window.h"

G_BEGIN_DECLS

struct _DemoWindow
{
  GtkApplicationWindow parent_instance;

	/* Template widgets */
	GtkHeaderBar * header_bar;
	ForgerCanvas * canvas;

  /* Canvas event controllers */
  GtkGestureDrag * drag_gesture;
  GtkGestureClick * click_gesture;
  GtkEventControllerScroll * scroll_event;

  /* Forger */
  ForgerContext * context;
  ForgerScene * scene;
  ForgerCamera * camera;
  ForgerContainer * test_entities_container;
  ForgerMaterial * material;
  ForgerMaterial * edges_material;
  ForgerMaterial * spline_material;
};


void    _demo_window_init_actions       (DemoWindow * self);
void    _demo_window_init_tests         (DemoWindow * self);
void    _demo_window_attach_controllers (DemoWindow *self);
void    _demo_window_detach_controllers (DemoWindow *self);

G_END_DECLS
