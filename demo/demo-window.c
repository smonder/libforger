/*
 * demo-window.c
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
#define G_LOG_DOMAIN "libforger-demo-window"

#include "config.h"
#include "forger-debug.h"

#include "demo-window-private.h"

G_DEFINE_FINAL_TYPE (DemoWindow, demo_window, GTK_TYPE_APPLICATION_WINDOW)


/*
 * -----< GTK_WIDGET CLASS IMPLEMENTATION >----- *
 */
static void
on_canvas_realize (ForgerCanvas *canvas,
                   DemoWindow   *self)
{
  g_assert (DEMO_IS_WINDOW (self));
  g_assert (FORGER_IS_CANVAS (canvas));

  self->context = forger_context_new ();
  forger_canvas_set_context (self->canvas, self->context);

  self->scene = forger_context_get_scene (self->context);
  self->test_entities_container = FORGER_CONTAINER (forger_container_new ("Test entities"));
  forger_container_append_child (forger_scene_get_root (self->scene),
                                 FORGER_ENTITY (self->test_entities_container));

  _demo_window_attach_controllers (self);
  _demo_window_init_actions (self);
  _demo_window_init_tests (self);
}

static void
demo_window_realize (GtkWidget *widget)
{
  DemoWindow *self = (DemoWindow *)widget;
  GdkGLContext *gl_context;
  GdkSurface *surface;
  GError *error = NULL;

  GTK_WIDGET_CLASS (demo_window_parent_class)->realize (widget);

  surface = gtk_native_get_surface (GTK_NATIVE (self));
  gl_context = gdk_surface_create_gl_context (surface, &error);

  if (!gl_context)
    g_error ("Failed to create OpenGL Context %s", error->message);

  gdk_gl_context_set_allowed_apis (gl_context, GDK_GL_API_GL);
  gdk_gl_context_set_required_version (gl_context,
                                       OPENGL_VERSION_MAJOR_REQUIRED,
                                       OPENGL_VERSION_MINOR_REQUIRED);
  gdk_gl_context_set_forward_compatible (gl_context, FALSE);

#ifdef ENABLE_TRACING
  gdk_gl_context_set_debug_enabled (gl_context, TRUE);
#endif

  self->canvas = FORGER_CANVAS (forger_canvas_new_for_gl_context (gl_context));
  forger_canvas_set_bgcolor_name (self->canvas, "white");
  gtk_window_set_child (GTK_WINDOW (self), GTK_WIDGET (self->canvas));

  g_signal_connect (self->canvas, "realize", G_CALLBACK (on_canvas_realize), self);
}

static void
demo_window_unrealize (GtkWidget *widget)
{
  DemoWindow *self = (DemoWindow *)widget;

  _demo_window_detach_controllers (self);

  GTK_WIDGET_CLASS (demo_window_parent_class)->unrealize (widget);
}


/*
 * -----< G_OBJECT CLASS IMPLEMENTATION >----- *
 */
static void
demo_window_finalize (GObject *object)
{
  DemoWindow *self = (DemoWindow *)object;

  g_clear_object (&self->context);

  G_OBJECT_CLASS (demo_window_parent_class)->finalize (object);
}

static void
demo_window_class_init (DemoWindowClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->finalize = demo_window_finalize;

  widget_class->realize = demo_window_realize;
  widget_class->unrealize = demo_window_unrealize;

  gtk_widget_class_set_template_from_resource (widget_class, "/io/sam/libforger/demo/demo-window.ui");
	gtk_widget_class_bind_template_child (widget_class, DemoWindow, header_bar);
}

static void
demo_window_init (DemoWindow *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}


/*
 * -----< CONSTRUCTORS >----- *
 */
DemoWindow *
demo_window_new (void)
{
  return g_object_new (DEMO_TYPE_WINDOW, NULL);
}

