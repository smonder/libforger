/*
 * demo-window-tests.c
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "demo-window-tests"

#include "config.h"
#include "forger-debug.h"

#include "demo-window-private.h"


void
_demo_window_init_tests (DemoWindow * self)
{
  ForgerMap *diffuse;
  ForgerOperation *mouse_picker;
  self->camera = forger_camera_new (_("Default Camera"),
                                    _("The default camera that is created with context."),
                                    45.0f, -1.0f, 1.0f);
  forger_container_append_child (self->test_entities_container,
                                 FORGER_ENTITY (self->camera));

  forger_context_bind_camera (self->context, self->camera);

  mouse_picker = forger_mouse_picker_get_default ();
  forger_context_bind_operation (self->context, mouse_picker);

  self->material = forger_material_diffuse_new ("Test Diffuse Material");
  forger_material_diffuse_set_color_name (FORGER_MATERIAL_DIFFUSE (self->material),
                                          "#ff0000ff");

  diffuse = forger_map_image_new_from_resources ("/io/sam/libforger/demo/textures/mountains.svg");
  if (forger_map_set_buffer (diffuse, NULL))
    forger_material_add_map (self->material, diffuse, FORGER_TEXTURE_DIFFUSE);

  self->spline_material = forger_material_spline_new ("Test Spline Material");
  forger_material_spline_set_color_name (FORGER_MATERIAL_SPLINE (self->spline_material), "blue");
}
