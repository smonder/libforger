/*
 * demo-window-actions.c
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
#define G_LOG_DOMAIN "demo-window-actions"

#include "config.h"
#include "forger-debug.h"
#include "demo-window-private.h"


static void
demo_window_create_quad_mesh_action (GSimpleAction *action,
                                     GVariant      *parameter,
                                     gpointer       user_data)
{
	DemoWindow *self = user_data;
  ForgerEntity *quad;
  const gfloat side_length = 200.0f;
  const gfloat origin_x = 100.0f;
  const gfloat origin_y = 100.0f;
  GArray * vertices;
  ForgerVertex vertices_data [] = {
      FORGER_VERTEX(origin_x - side_length / 2.0f, origin_y - side_length / 2.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f),
      FORGER_VERTEX(origin_x + side_length / 2.0f, origin_y - side_length / 2.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f),
      FORGER_VERTEX(origin_x + side_length / 2.0f, origin_y + side_length / 2.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f),
      FORGER_VERTEX(origin_x - side_length / 2.0f, origin_y + side_length / 2.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f)
  };
  ForgerMeshFace *face;

  FORGER_ENTRY;
	g_assert (DEMO_IS_WINDOW (self));

  forger_canvas_make_current (self->canvas);
  quad = forger_mesh_new ();
  forger_entity_set_name (quad, _("Test Quad Mesh"));

  vertices = g_array_new (FALSE, FALSE, sizeof(ForgerVertex));
  g_array_append_vals (vertices, vertices_data, G_N_ELEMENTS (vertices_data));

  face = forger_mesh_face_new ();
  forger_mesh_face_push_triangle (face, &(ForgerTriangle){0, 1, 2});
  forger_mesh_face_push_triangle (face, &(ForgerTriangle){2, 3, 0});
  forger_mesh_face_push_edge (face, &(ForgerMeshEdge){0, 1});
  forger_mesh_face_push_edge (face, &(ForgerMeshEdge){1, 2});
  forger_mesh_face_push_edge (face, &(ForgerMeshEdge){2, 3});
  forger_mesh_face_push_edge (face, &(ForgerMeshEdge){3, 0});
  forger_mesh_append_face (FORGER_MESH (quad), face, vertices);

  forger_entity_set (quad,
                     FORGER_COMPONENT_MATERIAL,
                     FORGER_ENTITY_COMPONENT (self->material));
  forger_container_append_child (self->test_entities_container, quad);

  FORGER_EXIT;
}

static void
demo_window_create_rectangle_action (GSimpleAction *action,
                                     GVariant      *parameter,
                                     gpointer       user_data)
{
	DemoWindow *self = user_data;
  ForgerEntity *rectangle;
  GdkRectangle rect = {0, 0, 300, 600};
  GdkRGBA edge_color;

  graphene_point3d_t vertices [] = {
      {(gfloat)rect.x - (gfloat)rect.width / 2.0f, (gfloat)rect.y - (gfloat)rect.height / 2.0f, 0.0f},
      {(gfloat)rect.x + (gfloat)rect.width / 2.0f, (gfloat)rect.y - (gfloat)rect.height / 2.0f, 0.0f},
      {(gfloat)rect.x + (gfloat)rect.width / 2.0f, (gfloat)rect.y + (gfloat)rect.height / 2.0f, 0.0f},
      {(gfloat)rect.x - (gfloat)rect.width / 2.0f, (gfloat)rect.y + (gfloat)rect.height / 2.0f, 0.0f}
  };

  FORGER_ENTRY;
	g_assert (DEMO_IS_WINDOW (self));

  forger_canvas_make_current (self->canvas);

  gdk_rgba_parse (&edge_color, "red");
  rectangle = forger_raw_spline_new (_("Test Rectangle"),
                                     TRUE, /* cyclic*/
                                     &edge_color);

  forger_raw_spline_append_points (FORGER_RAW_SPLINE (rectangle),
                                   vertices,
                                   G_N_ELEMENTS (vertices));

  forger_entity_set (rectangle,
                     FORGER_COMPONENT_MATERIAL,
                     FORGER_ENTITY_COMPONENT (self->spline_material));
  forger_container_append_child (self->test_entities_container, rectangle);

  FORGER_EXIT;
}

static void
demo_window_create_quad_action (GSimpleAction *action,
                                GVariant      *parameter,
                                gpointer       user_data)
{
	DemoWindow *self = user_data;
  ForgerEntity *quad;
  const gfloat side_length = 200.0f;
  const gfloat origin_x = -100.0f;
  const gfloat origin_y = -100.0f;
  ForgerVertex vertices [] = {
      FORGER_VERTEX(origin_x - side_length / 2.0f, origin_y - side_length / 2.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f),
      FORGER_VERTEX(origin_x + side_length / 2.0f, origin_y - side_length / 2.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f),
      FORGER_VERTEX(origin_x + side_length / 2.0f, origin_y + side_length / 2.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f),
      FORGER_VERTEX(origin_x - side_length / 2.0f, origin_y + side_length / 2.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f)
  };
  guint index_buffer [] = { 0, 1, 2, 2, 3, 0 };

  FORGER_ENTRY;
	g_assert (DEMO_IS_WINDOW (self));

  forger_canvas_make_current (self->canvas);
  quad = forger_raw_mesh_new (_("Test Quad"));
  forger_raw_mesh_push_n_vertices (FORGER_RAW_MESH (quad), 4, vertices);
  forger_raw_mesh_push_n_index (FORGER_RAW_MESH (quad), 6, index_buffer);
  forger_entity_set (quad,
                     FORGER_COMPONENT_MATERIAL,
                     FORGER_ENTITY_COMPONENT (self->material));
  forger_container_append_child (self->test_entities_container, quad);

  FORGER_EXIT;
}

static void
demo_window_about_action (GSimpleAction *action,
                          GVariant      *parameter,
                          gpointer       user_data)
{
	static const char *authors[] = {"Salim Monder", NULL};
	DemoWindow *self = user_data;

	g_assert (DEMO_IS_WINDOW (self));

	gtk_show_about_dialog (GTK_WINDOW (self),
	                       "program-name", "LibForger Demo",
	                       "logo-icon-name", "io.sam.libforger",
	                       "authors", authors,
	                       "version", PACKAGE_VERSION,
	                       "copyright", "© 2023 Salim Monder",
	                       NULL);
}

static void
demo_window_quit_action (GSimpleAction *action,
                         GVariant      *parameter,
                         gpointer       user_data)
{
	DemoWindow *self = user_data;
  GtkApplication *app;

	g_assert (DEMO_IS_WINDOW (self));

  app = gtk_window_get_application (GTK_WINDOW (self));
	g_application_quit (G_APPLICATION (app));
}


static const GActionEntry app_actions[] = {
	{ "quit",             demo_window_quit_action },
	{ "about",            demo_window_about_action },
  { "create-quad",      demo_window_create_quad_action },
  { "create-rect",      demo_window_create_rectangle_action },
  { "create-quad-mesh", demo_window_create_quad_mesh_action },
};

void
_demo_window_init_actions (DemoWindow * self)
{
  g_assert (DEMO_IS_WINDOW (self));

	g_action_map_add_action_entries (G_ACTION_MAP (self),
	                                 app_actions,
	                                 G_N_ELEMENTS (app_actions),
	                                 self);
}
