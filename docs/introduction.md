Title: Introduction
Slug: Introduction

# Introduction

Libforger is a graphics library that uses OpenGL as its backend, and
is designed for simple-use node-based 3D/2D rendering.

A typical usage of libforger is starting from creating a new valid
[class@ForgerContext] for a new or an existing [class@ForgerScene]
Libforger offers a variety of entity types like [class@ForgerRawMesh],
[class@ForgerMesh] or the a spline types like [class@ForgerRawSpline]
