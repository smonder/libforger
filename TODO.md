# TODO

 * Review API, there are a lot of things I'd like to change.
 * An environment material for ForgerScene.
 * Simple material types: SimpleGlossy, etc.
 * Handle complex Spline types.
 * Handle Lights.
 * Serialize/De-serialize ForgerScene.
 * Use ASSIMP to import external file formats into ForgerScene.
