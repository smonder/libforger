#include <libforger.h>

static void
test_shader_basic (void)
{
  g_message ("testing shaders");
}

int
main (int   argc,
      char *argv[])
{
  g_test_init (&argc, &argv, NULL);
  g_test_add_func ("/libforger/Shader", test_shader_basic);
  return g_test_run ();
}
