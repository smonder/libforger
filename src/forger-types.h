/*
 * forger-types.h
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#if !defined(FORGER_INSIDE) && !defined(LIBFORGER_COMPILATION)
#error "Only <libforger.h> can be included directly."
#endif

#include <glib-object.h>

#include "forger-version-macros.h"

G_BEGIN_DECLS

#define FORGER_IS_COMPONENT_TYPE(x_) ((x_)<N_ENTITY_COMPONENT_TYPES)
#define FORGER_IS_TEXTURE_SLOT(x_) ((x_)<N_TEXTURE_SLOTS && (x_)>FORGER_TEXTURE_INVALID)

typedef struct _ForgerBoundingBox         ForgerBoundingBox;
typedef struct _ForgerCamera              ForgerCamera;
typedef struct _ForgerCanvas              ForgerCanvas;
typedef struct _ForgerContainer           ForgerContainer;
typedef struct _ForgerContext             ForgerContext;
typedef struct _ForgerEdgesRenderNode     ForgerEdgesRenderNode;
typedef struct _ForgerEngine              ForgerEngine;
typedef struct _ForgerEntity              ForgerEntity;
typedef struct _ForgerEntityComponent     ForgerEntityComponent;
typedef struct _ForgerGizmo               ForgerGizmo;
typedef struct _ForgerHelperRenderNode    ForgerHelperRenderNode;
typedef struct _ForgerMap                 ForgerMap;
typedef struct _ForgerMapImage            ForgerMapImage;
typedef struct _ForgerMaterial            ForgerMaterial;
typedef struct _ForgerMaterialDiffuse     ForgerMaterialDiffuse;
typedef struct _ForgerMaterialSpline      ForgerMaterialSpline;
typedef struct _ForgerMesh                ForgerMesh;
typedef struct _ForgerMeshEdge            ForgerMeshEdge;
typedef struct _ForgerMeshFace            ForgerMeshFace;
typedef struct _ForgerMeshRenderNode      ForgerMeshRenderNode;
typedef struct _ForgerMixRenderNode       ForgerMixRenderNode;
typedef struct _ForgerMousePicker         ForgerMousePicker;
typedef struct _ForgerOperation           ForgerOperation;
typedef struct _ForgerRawMesh             ForgerRawMesh;
typedef struct _ForgerRawMeshRenderNode   ForgerRawMeshRenderNode;
typedef struct _ForgerRawSpline           ForgerRawSpline;
typedef struct _ForgerRenderNode          ForgerRenderNode;
typedef struct _ForgerScene               ForgerScene;
typedef struct _ForgerShader              ForgerShader;
typedef struct _ForgerShaderHelper        ForgerShaderHelper;
typedef struct _ForgerShaderModule        ForgerShaderModule;
typedef struct _ForgerSplineRenderNode    ForgerSplineRenderNode;
typedef struct _ForgerTriangle            ForgerTriangle;
typedef struct _ForgerVertex              ForgerVertex;
typedef struct _ForgerWireRenderNode      ForgerWireRenderNode;


/**
 * ForgerGizmoAxis:
 * @FORGER_AXIS_X: the x axis.
 * @FORGER_AXIS_Y: the y axis.
 * @FORGER_AXIS_Z: the z axis.
 *
 * The axis of the gizmo.
 */
typedef enum
{
  FORGER_AXIS_X,
  FORGER_AXIS_Y,
  FORGER_AXIS_Z,
} ForgerGizmoAxis;

/**
 * ForgerComponentType:
 * @FORGER_COMPONENT_RENDER_NODE: a #ForgerRenderNode component which makes the
 * entity render-able.
 * @FORGER_COMPONENT_GIZMO: a #ForgerGizmo component which makes the entity
 * translatable.
 * @FORGER_COMPONENT_BOUNDING_BOX: a #graphene_box_t component which makes the
 * entity select-able and collide-able
 *
 * The type category of the componenet.
 */
typedef enum
{
  FORGER_COMPONENT_RENDER_NODE,
  FORGER_COMPONENT_GIZMO,
  FORGER_COMPONENT_MATERIAL,
  FORGER_COMPONENT_BOUNDING_BOX,

  FORGER_COMPONENT_EDGES_RENDER_NODE,
  FORGER_COMPONENT_EDGES_MATERIAL,

  N_ENTITY_COMPONENT_TYPES
} ForgerComponentType;

/**
 * ForgerMeasuringSystem:
 * @FORGER_UNITS_GENERIC: use a generic measuring system.
 * @FORGER_UNITS_METRIC: use the metric measuring system.
 * @FORGER_UNITS_IMPERIAL: use the imperial measuring system.
 *
 * The measuring units system used in the scene.
 */
typedef enum
{
  FORGER_UNITS_GENERIC,
  FORGER_UNITS_METRIC,
  FORGER_UNITS_IMPERIAL,
} ForgerMeasuringSystem;

/**
 * ForgerShaderModuleType:
 * @FORGER_SHADER_MODULE_GEOMETRY: A GL_GEOMETRY_SHADER module.
 * @FORGER_SHADER_MODULE_PIXEL: A GL_FRAGMENT_SHADER module,
 * @FORGER_SHADER_MODULE_VERTEX: A GL_VERTEX_SHADER module.
 *
 * The type of the shader module.
 */
typedef enum
{
  FORGER_SHADER_MODULE_GEOMETRY,
  FORGER_SHADER_MODULE_PIXEL,
  FORGER_SHADER_MODULE_VERTEX,
} ForgerShaderModuleType;

/**
 * ForgerTextureSlot:
 * @FORGER_TEXTURE_INVALID: an invalid state and should not be used.
 * @FORGER_TEXTURE_DIFFUSE:
 * @FORGER_TEXTURE_SPECULAR: the texture is going to be used to control the specular.
 * @FORGER_TEXTURE_NORMAL: the texture is going to be used as a normal map
 *
 * The slot that the texture will be bound to.
 */
typedef enum
{
  FORGER_TEXTURE_INVALID,
  FORGER_TEXTURE_DIFFUSE,
  FORGER_TEXTURE_SPECULAR,
  FORGER_TEXTURE_NORMAL,

  N_TEXTURE_SLOTS
} ForgerTextureSlot;


typedef enum
{
  FORGER_ENTITY_VISIT_BREAK    = 0,
  FORGER_ENTITY_VISIT_CONTINUE = 0x1,
  FORGER_ENTITY_VISIT_CHILDREN = 0x3,
} ForgerEntityVisit;

typedef enum
{
  FORGER_ENTITY_FLAGS_NONE       = 0,
  FORGER_ENTITY_FLAGS_DESCENDANT = 1 << 0,
  FORGER_ENTITY_FLAGS_ADDED      = 1 << 1,
  FORGER_ENTITY_FLAGS_CHANGED    = 1 << 2,
  FORGER_ENTITY_FLAGS_REMOVED    = 1 << 3,
} ForgerEntityFlags;

/**
 * ForgerSceneTraverseFunc:
 * @entity: a #ForgerEntity
 * @user_data: closure data provided to forger_container_traverse()
 *
 * This function prototype is used to traverse a scene tree of #ForgerEntity.
 *
 * Returns: #ForgerEntityVisit, %FORGER_ENTITY_VISIT_BREAK to stop traversal.
 */
typedef ForgerEntityVisit (* ForgerSceneTraverseFunc) (ForgerEntity * entity,
                                                       gpointer       user_data);

/**
 * ForgerEntityCompare:
 * @entity: a #ForgerEntity that iterate over children
 * @child: a #ForgerEntity to be inserted
 *
 * This callback function is a convenience wrapper around GCompareFunc
 *
 * Returns: int
 */
typedef int (* ForgerEntityCompare) (ForgerEntity * entity,
                                     ForgerEntity * child);



G_END_DECLS
