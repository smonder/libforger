/*
 * forger-material-diffuse.c
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
#define G_LOG_DOMAIN "forger-material-diffuse"

#include "config.h"
#include "forger-debug.h"

#include <glib/gi18n.h>

#include "forger-material-diffuse.h"
#include "forger-shader-private.h"

struct _ForgerMaterialDiffuse
{
  ForgerMaterial parent_instance;

  GdkRGBA color;
  guint use_color : 1;
};

G_DEFINE_FINAL_TYPE (ForgerMaterialDiffuse, forger_material_diffuse, FORGER_TYPE_MATERIAL)

enum {
  PROP_0,
  PROP_COLOR,
  PROP_COLOR_NAME,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];


/*
 * -----< FORGER_MATERIAL CLASS IMPLEMENTATION >----- *
 */
static void
forger_material_diffuse_set_uniforms (ForgerMaterial *material,
                                      ForgerShader   *shader)
{
  ForgerMaterialDiffuse *self = FORGER_MATERIAL_DIFFUSE (material);

  g_assert (FORGER_IS_MATERIAL_DIFFUSE (self));
  g_assert (FORGER_IS_SHADER (shader));

  forger_shader_set_uniform_color (shader, "u_DiffuseColor", &self->color);
  forger_shader_set_uniform_int (shader, "u_UseMap", !self->use_color);
}


/*
 * -----< G_OBJECT CLASS IMPLEMENTATION >----- *
 */
static void
forger_material_diffuse_get_property (GObject    *object,
                                      guint       prop_id,
                                      GValue     *value,
                                      GParamSpec *pspec)
{
  ForgerMaterialDiffuse *self = FORGER_MATERIAL_DIFFUSE (object);

  switch (prop_id)
    {
    case PROP_COLOR:
      g_value_set_boxed (value, forger_material_diffuse_get_color (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
forger_material_diffuse_set_property (GObject      *object,
                                      guint         prop_id,
                                      const GValue *value,
                                      GParamSpec   *pspec)
{
  ForgerMaterialDiffuse *self = FORGER_MATERIAL_DIFFUSE (object);

  switch (prop_id)
    {
    case PROP_COLOR:
      forger_material_diffuse_set_color (self, g_value_get_boxed (value));
      break;

    case PROP_COLOR_NAME:
      forger_material_diffuse_set_color_name (self, g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
forger_material_diffuse_class_init (ForgerMaterialDiffuseClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  ForgerMaterialClass *material_class = FORGER_MATERIAL_CLASS (klass);

  object_class->get_property = forger_material_diffuse_get_property;
  object_class->set_property = forger_material_diffuse_set_property;

  material_class->set_uniforms = forger_material_diffuse_set_uniforms;

  /**
   * ForgerMaterialDiffuse:color:
   *
   * The "color" property is the #GdkRGBA color that represents the color
   * value that is going to be shown instead of map if the user didn't
   * provide any texture map.
   *
   * Since: 0.1
   */
  properties [PROP_COLOR] =
    g_param_spec_boxed ("color", NULL, NULL,
                        GDK_TYPE_RGBA,
                        (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  /**
   * ForgerMaterialDiffuse:color-name:
   *
   * The "color-name" property is another way to set "ForgerMaterialDiffuse:color"
   * property, this time using a string.
   * The string should be a valid string that can be handled by #GdkRGBA.
   * Refer to the #GdkRGBA manual to see available options.
   *
   * Since: 0.1
   */
  properties [PROP_COLOR_NAME] =
    g_param_spec_string ("color-name", NULL, NULL,
                         NULL,
                         (G_PARAM_WRITABLE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
forger_material_diffuse_init (ForgerMaterialDiffuse *self)
{
  ForgerShader *diffuse_shader;
  ForgerShaderModule *vs_module;
  ForgerShaderModule *ps_module;
  GError *error = NULL;

  diffuse_shader = forger_shader_new (_("Diffuse Shader"));
  vs_module = forger_shader_module_new_from_resource (FORGER_SHADER_MODULE_VERTEX,
                                                      "/io/sam/libforger/diffuse-vertex.glsl", &error);
  ps_module = forger_shader_module_new_from_resource (FORGER_SHADER_MODULE_PIXEL,
                                                      "/io/sam/libforger/diffuse-pixel.glsl", &error);

  if (!vs_module || !ps_module)
    g_error ("error initializing Diffuse shader: %s", error->message);

  forger_shader_add_module (diffuse_shader, vs_module);
  forger_shader_add_module (diffuse_shader, ps_module);

  if (!forger_shader_link_modules (diffuse_shader, &error))
    g_error ("error linking diffuse shader: %s", error->message);

  _forger_material_set_shader (FORGER_MATERIAL (self), diffuse_shader);

  self->use_color = !!TRUE;
}


/*
 * -----< CONSTRUCTORS >----- *
 */
/**
 * forger_material_diffuse_new:
 * @name: a #gchar: a unique name identifier for the material.
 *
 * Creates a new #ForgerMaterialDiffuse
 *
 * Returns: (transfer full): a newly created #ForgerMaterial
 */
ForgerMaterial *
forger_material_diffuse_new (const gchar * name)
{
  return g_object_new (FORGER_TYPE_MATERIAL_DIFFUSE,
                       "name", name,
                       NULL);
}


/*
 * -----< PROPERTIES >----- *
 */
/**
 * forger_material_diffuse_get_map:
 * @self: a #ForgerMaterialDiffuse
 *
 * Gets the diffuse map of @self
 *
 * Returns: (transfer full) (nullable): a #ForgerMap or %NULL
 */
ForgerMap *
forger_material_diffuse_get_map (ForgerMaterialDiffuse *self)
{
  g_return_val_if_fail (FORGER_IS_MATERIAL_DIFFUSE (self), NULL);
  g_critical ("Not implemented!");
  return NULL;
}

/**
 * forger_material_diffuse_set_map:
 * @self: a #ForgerMaterialDiffuse
 *
 * Sets the diffuse map of @self
 *
 */
void
forger_material_diffuse_set_map (ForgerMaterialDiffuse *self,
                                 ForgerMap             *map)
{
  FORGER_ENTRY;
  g_return_if_fail (FORGER_IS_MATERIAL_DIFFUSE (self));
  g_return_if_fail (FORGER_IS_MAP (map));

  forger_material_add_map (FORGER_MATERIAL (self),
                           map,
                           FORGER_TEXTURE_DIFFUSE);
  self->use_color = !!FALSE;

  FORGER_EXIT;
}

/**
 * forger_material_diffuse_get_color:
 * @self: a #ForgerMaterialDiffuse
 *
 * gets the "color" property of @self
 *
 * Returns: (transfer full): a #GdkRGBA
 */
GdkRGBA *
forger_material_diffuse_get_color (ForgerMaterialDiffuse *self)
{
  g_return_val_if_fail (FORGER_IS_MATERIAL_DIFFUSE (self), NULL);
  return &self->color;
}

/**
 * forger_material_diffuse_set_color:
 * @self: a #ForgerMaterialDiffuse
 * @color: a #GdkRGBA
 *
 * sets the "color" property of @self
 *
 */
void
forger_material_diffuse_set_color (ForgerMaterialDiffuse *self,
                                   const GdkRGBA         *color)
{
  g_return_if_fail (FORGER_IS_MATERIAL_DIFFUSE (self));
  g_return_if_fail (color != NULL);

  if (!gdk_rgba_equal (&self->color, color))
    {
      self->color.red = color->red;
      self->color.green = color->green;
      self->color.blue = color->blue;
      self->color.alpha = color->alpha;

      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_COLOR]);
    }
}

/**
 * forger_material_diffuse_set_color_name:
 * @self: a #ForgerMaterialDiffuse
 * @color: a #gchar: a valid color string.
 *
 * sets the "color" property of @self using the name of the color.
 * The provided color name "color", should be a valid string
 * that can be paresed using gdk_rgba_parse().
 *
 * FROM GDKRGBA MANUAL:
 * The string can be either one of:
 * - A standard name (Taken from the CSS specification).
 * - A hexadecimal value in the form “#rgb”, “#rrggbb”, “#rrrgggbbb” or ”#rrrrggggbbbb”
 * - A hexadecimal value in the form “#rgba”, “#rrggbbaa”, or ”#rrrrggggbbbbaaaa”
 * - A RGB color in the form “rgb(r,g,b)” (In this case the color will have full opacity)
 * - A RGBA color in the form “rgba(r,g,b,a)”
 * - A HSL color in the form “hsl(hue, saturation, lightness)”
 * - A HSLA color in the form “hsla(hue, saturation, lightness, alpha)”
 *
 * Where “r”, “g”, “b” and “a” are respectively the red, green, blue and alpha color values.
 * In the last two cases, “r”, “g”, and “b” are either integers in the range 0
 * to 255 or percentage values in the range 0% to 100%, and a is a floating point
 * value in the range 0 to 1.
 *
 */
void
forger_material_diffuse_set_color_name (ForgerMaterialDiffuse *self,
                                        const gchar           *color)
{
  GdkRGBA new_color;

  FORGER_ENTRY;
  g_return_if_fail (FORGER_IS_MATERIAL_DIFFUSE (self));
  g_return_if_fail (color != NULL);

  if (gdk_rgba_parse (&new_color, color))
    forger_material_diffuse_set_color (self, &new_color);
  else
    g_critical ("Failed to parse the diffuse color from the provided string");

  FORGER_EXIT;
}
