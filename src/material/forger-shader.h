/*
 * forger-shader.h
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#if !defined(FORGER_INSIDE) && !defined(LIBFORGER_COMPILATION)
#error "Only <libforger.h> can be included directly."
#endif

#include <gio/gio.h>
#include <gdk/gdk.h>
#include <graphene.h>

#include "forger-shader-module.h"
#include "forger-version-macros.h"

G_BEGIN_DECLS

#define FORGER_TYPE_SHADER (forger_shader_get_type())

FORGER_AVAILABLE_IN_ALL
G_DECLARE_DERIVABLE_TYPE (ForgerShader, forger_shader, FORGER, SHADER, GObject)

struct _ForgerShaderClass
{
  GObjectClass parent_class;
};


FORGER_AVAILABLE_IN_ALL
ForgerShader * forger_shader_new                   (const gchar *        name) G_GNUC_WARN_UNUSED_RESULT;

FORGER_AVAILABLE_IN_ALL
gchar *        forger_shader_get_name              (ForgerShader *       self);
FORGER_AVAILABLE_IN_ALL
void           forger_shader_set_name              (ForgerShader *       self,
                                                    const gchar *        name);

FORGER_AVAILABLE_IN_ALL
void           forger_shader_add_module            (ForgerShader *       self,
                                                    ForgerShaderModule * mod);
FORGER_AVAILABLE_IN_ALL
gboolean       forger_shader_link_modules          (ForgerShader *       self,
                                                    GError **            error);

FORGER_AVAILABLE_IN_ALL
void           forger_shader_bind                  (ForgerShader *       self);
FORGER_AVAILABLE_IN_ALL
void           forger_shader_unbind                (ForgerShader *       self);

FORGER_AVAILABLE_IN_ALL
void           forger_shader_set_uniform_int       (ForgerShader *       self,
                                                    const gchar *        name,
                                                    const int            value);
FORGER_AVAILABLE_IN_ALL
void           forger_shader_set_uniform_point     (ForgerShader *       self,
                                                    const gchar *        name,
                                                    graphene_point3d_t * value);
FORGER_AVAILABLE_IN_ALL
void           forger_shader_set_uniform_color     (ForgerShader *       self,
                                                    const gchar *        name,
                                                    GdkRGBA *            value);
FORGER_AVAILABLE_IN_ALL
void           forger_shader_set_uniform_matrix    (ForgerShader *       self,
                                                    const gchar *        name,
                                                    const graphene_matrix_t *   value);

G_END_DECLS
