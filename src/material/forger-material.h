/*
 * forger-material.h
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#if !defined(FORGER_INSIDE) && !defined(LIBFORGER_COMPILATION)
#error "Only <libforger.h> can be included directly."
#endif

#include "entity/forger-entity-component.h"

#include "forger-map.h"
#include "forger-shader.h"
#include "forger-version-macros.h"

G_BEGIN_DECLS

#define FORGER_TYPE_MATERIAL (forger_material_get_type())

FORGER_AVAILABLE_IN_ALL
G_DECLARE_DERIVABLE_TYPE (ForgerMaterial, forger_material, FORGER, MATERIAL, ForgerEntityComponent)

struct _ForgerMaterialClass
{
  ForgerEntityComponentClass parent_class;

  /* A method to be called from the prepare function to set all the uniforms
   * of the material
   */
  void   (* set_uniforms)       (ForgerMaterial * material,
                                 ForgerShader *   shader);
};

FORGER_AVAILABLE_IN_ALL
gchar *        forger_material_get_name          (ForgerMaterial *   self);
FORGER_AVAILABLE_IN_ALL
void           forger_material_set_name          (ForgerMaterial *   self,
                                                  const gchar *      setting);

FORGER_AVAILABLE_IN_ALL
ForgerShader * forger_material_ref_shader        (ForgerMaterial *   self);

FORGER_AVAILABLE_IN_ALL
GPtrArray *    forger_material_get_maps          (ForgerMaterial *   self);
FORGER_AVAILABLE_IN_ALL
void           forger_material_add_map           (ForgerMaterial *   self,
                                                  ForgerMap *        map,
                                                  ForgerTextureSlot  slot);

FORGER_AVAILABLE_IN_ALL
GPtrArray *    forger_material_get_lights        (ForgerMaterial *   self);

G_END_DECLS
