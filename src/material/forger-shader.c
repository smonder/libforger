/*
 * forger-shader.c
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "forger-shader"

#include "config.h"
#include "forger-debug.h"

#include <epoxy/gl.h>

#include "forger-material-error.h"
#include "forger-shader.h"
#include "forger-shader-private.h"

typedef struct
{
  /* The name property is just a way for the ui to view this shader */
  gchar * name;

  /* We need a unique ID to be generated and used by OpenGL API. */
  guint ID_;

  /* We need to store the uniforms in a HashTable so that the setting of
   * uniforms is going to be much easier.
   */
  GHashTable * uniforms;

  /* Modules */
  GPtrArray * modules;
  gboolean shader_linked;
} ForgerShaderPrivate;

G_DEFINE_TYPE_WITH_PRIVATE (ForgerShader, forger_shader, G_TYPE_OBJECT)

enum {
  PROP_0,
  PROP_NAME,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

/*
 * G_OBJECT CLASS IMPLEMENTATION
 */
static void
forger_shader_finalize (GObject *object)
{
  ForgerShader *self = (ForgerShader *)object;
  ForgerShaderPrivate *priv = forger_shader_get_instance_private (self);

  g_clear_pointer (&priv->name, g_free);
  g_clear_pointer (&priv->uniforms, g_hash_table_destroy);
  g_ptr_array_free (priv->modules, FALSE);

  glDeleteProgram (priv->ID_);

  G_OBJECT_CLASS (forger_shader_parent_class)->finalize (object);
}

static void
forger_shader_get_property (GObject    *object,
                            guint       prop_id,
                            GValue     *value,
                            GParamSpec *pspec)
{
  ForgerShader *self = FORGER_SHADER (object);

  switch (prop_id)
    {
    case PROP_NAME:
      g_value_set_string (value, forger_shader_get_name (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
forger_shader_set_property (GObject      *object,
                            guint         prop_id,
                            const GValue *value,
                            GParamSpec   *pspec)
{
  ForgerShader *self = FORGER_SHADER (object);

  switch (prop_id)
    {
    case PROP_NAME:
      forger_shader_set_name (self, g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
forger_shader_class_init (ForgerShaderClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = forger_shader_finalize;
  object_class->get_property = forger_shader_get_property;
  object_class->set_property = forger_shader_set_property;

  /**
   * ForgerShader:name:
   *
   * The "name" property is the name given to the shader.
   *
   * Since: 0.1
   */
  properties [PROP_NAME] =
    g_param_spec_string ("name", NULL, NULL,
                         "dummy",
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
forger_shader_init (ForgerShader *self)
{
  ForgerShaderPrivate *priv = forger_shader_get_instance_private (self);

  priv->uniforms = g_hash_table_new (g_str_hash, g_str_equal);
  priv->modules = g_ptr_array_new ();
  priv->shader_linked = FALSE;
}


/*
 * -----< CONSTRUCTORS >----- *
 */
/**
 * forger_shader_new:
 * @name: a #gchar: the name of the new shader
 *
 * Creates a new #ForgerShader with the name @name.
 *
 * Returns: (transfer full): a newly created #ForgerShader
 */
ForgerShader *
forger_shader_new (const gchar *name)
{
  return g_object_new (FORGER_TYPE_SHADER,
                       "name", name,
                       NULL);
}


/*
 * -----< PROPERTIES >----- *
 */
gchar *
forger_shader_get_name (ForgerShader *self)
{
  ForgerShaderPrivate *priv = forger_shader_get_instance_private (self);
  g_return_val_if_fail (FORGER_IS_SHADER (self), NULL);
  return priv->name;
}

void
forger_shader_set_name (ForgerShader *self,
                        const gchar  *name)
{
  ForgerShaderPrivate *priv = forger_shader_get_instance_private (self);

  FORGER_ENTRY;
  g_return_if_fail (FORGER_IS_SHADER (self));
  g_return_if_fail (name);

  if (g_set_str (&priv->name, name))
    g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_NAME]);

  FORGER_EXIT;
}


/*
 * -----< SHADER MODULES AND LINKING >----- *
 */
/**
 * forger_shader_add_module:
 * @self: a #ForgerShader
 * @mod: a #ForgerShaderModule
 *
 * Adds %mod to %self.
 *
 */
void
forger_shader_add_module (ForgerShader       *self,
                          ForgerShaderModule *mod)
{
  ForgerShaderPrivate *priv = forger_shader_get_instance_private (self);

  FORGER_ENTRY;
  g_return_if_fail (FORGER_IS_SHADER (self));
  g_return_if_fail (FORGER_IS_SHADER_MODULE (mod));

  if (priv->shader_linked)
    {
      g_error ("Can not add modules after shader is linked");
      FORGER_EXIT;
    }

  g_ptr_array_add (priv->modules, mod);
  FORGER_EXIT;
}

/**
 * forger_shader_link_modules:
 * @self: a #ForgerShader
 * @error: a #GError.
 *
 * Link all the GL Shader modules.
 *
 * Returns: %TRUE if the linking succeeded, %FALSE otherwise and %error is set.
 */
gboolean
forger_shader_link_modules (ForgerShader  *self,
                            GError       **error)
{
  ForgerShaderPrivate *priv = forger_shader_get_instance_private (self);
  gint status;

  FORGER_ENTRY;

  g_return_val_if_fail (FORGER_IS_SHADER (self), FALSE);
  g_return_val_if_fail (error != NULL || *error != NULL, FALSE);

  if (priv->shader_linked)
    {
      g_set_error (error,
                   FORGER_MATERIAL_ERROR,
                   FORGER_SHADER_LINKING_FAILED,
                   "Shader is already linked, you can not re-link it");
      FORGER_RETURN (FALSE);
    }

  priv->ID_ = glCreateProgram ();

  for (guint i = 0; i < priv->modules->len; i++)
    {
      ForgerShaderModule * module = g_ptr_array_index (priv->modules, i);
      glAttachShader (priv->ID_,
                      _forger_shader_module_get_id (module));
    }

  glLinkProgram (priv->ID_);

  glGetProgramiv (priv->ID_, GL_LINK_STATUS, &status);
  if (status == GL_FALSE)
    {
      g_autofree gchar *buffer;
      gint log_len;

      glGetProgramiv (priv->ID_, GL_INFO_LOG_LENGTH, &log_len);

      buffer = g_malloc (log_len + 1);
      glGetProgramInfoLog (priv->ID_, log_len, NULL, buffer);

      g_clear_error (error);
      g_set_error (error,
                   FORGER_MATERIAL_ERROR,
                   FORGER_SHADER_LINKING_FAILED,
                   "Shader program linking failure:\n%s", buffer);

      glDeleteProgram (priv->ID_);
      priv->ID_ = 0;

      FORGER_RETURN (FALSE);
    }

  priv->shader_linked = TRUE;
  FORGER_RETURN (TRUE);
}


/*
 * -----< USING THE SHADER METHODS >----- *
 */
/**
 * forger_shader_bind:
 * @self: a #ForgerShader.
 *
 * Selects the shader to be used in the render process.
 *
 * Since: 0.1
 */
void
forger_shader_bind (ForgerShader *self)
{
  ForgerShaderPrivate *priv = forger_shader_get_instance_private (self);

  g_return_if_fail (!self || FORGER_IS_SHADER (self));

  if (self && priv->ID_ != 0)
    glUseProgram (priv->ID_);
}

/**
 * forger_shader_unbind:
 * @self: a #ForgerShader.
 *
 * De-selects the shader.
 *
 * Since: 0.1
 */
void
forger_shader_unbind (ForgerShader * self)
{
  glUseProgram (0);
}


/*
 * -----< SETTING UNIFORMS METHODS >----- *
 */
static gint
get_uniform (ForgerShader *self,
             const gchar  *name)
{
  ForgerShaderPrivate *priv = forger_shader_get_instance_private (self);
  gint loc;

  g_assert (FORGER_IS_SHADER (self));
  g_assert (name);
  g_assert (priv->ID_ != 0);

  if (g_hash_table_contains (priv->uniforms, name))
    return GPOINTER_TO_INT (g_hash_table_lookup (priv->uniforms, name));

  loc = glGetUniformLocation (priv->ID_, name);
  if (loc < 0)
    {
      g_critical ("Failed to set the uniform: Shader doesn't contain uniform \"%s\".", name);
      return -1;
    }

  g_hash_table_insert (priv->uniforms,
                       g_strdup (name),
                       GINT_TO_POINTER (loc));
  return loc;
}

/**
 * forger_shader_set_uniform_int:
 * @self: a #ForgerShader.
 * @name: a #gchar.
 * @value: a #gint.
 *
 * Sets the value of uniform @name as @value.
 * This function does nothing if the uniform doesn't exist in @self.
 *
 * Since: 0.1
 */
void
forger_shader_set_uniform_int (ForgerShader *self,
                               const gchar  *name,
                               const gint    value)
{
  g_return_if_fail (!self || FORGER_IS_SHADER (self));
  g_return_if_fail (name);

  if (!self)
    return;

  glUniform1i (get_uniform (self, name), value);
}

/**
 * forger_shader_set_uniform_point:
 * @self: a #ForgerShader.
 * @name: a #gchar.
 * @value: a #graphene_point3d_t.
 *
 * Sets the value of uniform @name as @value.
 * This function does nothing if the uniform doesn't exist in @self.
 *
 * Since: 0.1
 */
void
forger_shader_set_uniform_point (ForgerShader       *self,
                                 const gchar        *name,
                                 graphene_point3d_t *value)
{
  g_return_if_fail (!self || FORGER_IS_SHADER (self));
  g_return_if_fail (name || value);

  if (!self)
    return;

  glUniform3f (get_uniform (self, name),
               value->x, value->y, value->z);
}

/**
 * forger_shader_set_uniform_color:
 * @self: a #ForgerShader.
 * @name: a #gchar.
 * @value: a #GdkRGBA.
 *
 * Sets the value of uniform @name as @value.
 * This function does nothing if the uniform doesn't exist in @self.
 *
 * Since: 0.1
 */
void
forger_shader_set_uniform_color (ForgerShader *self,
                                 const gchar  *name,
                                 GdkRGBA      *value)
{
  g_return_if_fail (!self || FORGER_IS_SHADER (self));
  g_return_if_fail (name || value);

  if (!self)
    return;

  glUniform4f (get_uniform (self, name),
               value->red, value->green, value->blue, value->alpha);
}

/**
 * forger_shader_set_uniform_matrix:
 * @self: a #ForgerShader.
 * @name: a #gchar.
 * @value: a #graphene_matrxi_t.
 *
 * Sets the value of uniform @name as @value.
 * This function does nothing if the uniform doesn't exist in @self.
 *
 * Since: 0.1
 */
void
forger_shader_set_uniform_matrix (ForgerShader            *self,
                                  const gchar             *name,
                                  const graphene_matrix_t *value)
{
  gfloat matrix[16];

  g_return_if_fail (!self || FORGER_IS_SHADER (self));
  g_return_if_fail (name || value);

  if (!self)
    return;

  graphene_matrix_to_float (value, matrix);
  glUniformMatrix4fv (get_uniform (self, name),
                      1, GL_FALSE, matrix);
}

