/*
 * forger-material.c
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
#define G_LOG_DOMAIN "forger-material"

#include "config.h"
#include "forger-debug.h"

#include <glib/gi18n.h>

#include "forger-material.h"
#include "forger-material-error.h"
#include "forger-shader-private.h"

typedef struct
{
  /* A name is what we see in material editor and what we use to select
   * materials by, therefore it should be unique.
   */
  gchar * name;

  /* Each material consists of three components: */
  /* 1. A shader object that is the link between our library and the GPU */
  ForgerShader * shader;

  /* 2. An array of maps to act as data provided to shaders in form of
   * bytes buffers.
   */
  GPtrArray * maps;

  /* 3. Some numeric factors and uniforms that are general in each material */
  gfloat dummy;

  /* 4. An array of lights that are provided by the engine.
   * For now and since we haven't handled lights (yet) we will treat this
   * as only placeholder.
   */
  GPtrArray * lights;
} ForgerMaterialPrivate;

G_DEFINE_ABSTRACT_TYPE_WITH_PRIVATE (ForgerMaterial, forger_material, FORGER_TYPE_ENTITY_COMPONENT)

enum {
  PROP_0,
  PROP_NAME,
  PROP_SHADER,
  PROP_MAPS,
  PROP_LIGHTS,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

/*
 * -----< FORGER_ENTITY_COMPONENT CLASS IMPLEMENTATION >----- *
 */
static const gchar *
forger_material_get_display_name (ForgerEntityComponent *component)
{
  return _("Material");
}

static const gchar *
forger_material_get_icon_name (ForgerEntityComponent *component)
{
  return "colorimeter-colorhug-symbolic";
}

static const gchar *
forger_material_get_description (ForgerEntityComponent *component)
{
  return _("Material");
}

static void
on_prepare_bind_maps_cb (ForgerMap    *map,
                         ForgerShader *shader)
{
  if (map != NULL)
    {
      forger_map_bind (map, forger_map_get_slot (map));
      forger_shader_set_uniform_int (shader, "u_MapSlot",
                                     forger_map_get_slot (map));
    }
}

static void
forger_material_real_prepare (ForgerEntityComponent *component,
                              graphene_matrix_t     *model_matrix,
                              graphene_matrix_t     *view_projection_matrix)
{
  ForgerMaterial *self = (ForgerMaterial *)component;
  ForgerMaterialPrivate *priv = forger_material_get_instance_private (self);

  g_assert (FORGER_IS_MATERIAL (self));
  g_assert (model_matrix != NULL);
  g_assert (view_projection_matrix != NULL);

  if (!priv->shader)
    return;

  forger_shader_bind (priv->shader);

  /* We first need to setup scene and component uniform matrices */
  forger_shader_set_uniform_matrix (priv->shader,
                                    "u_Model",
                                    model_matrix);
  forger_shader_set_uniform_matrix (priv->shader,
                                    "u_ViewProjection",
                                    view_projection_matrix);

  /* Then the maps if there is any */
  g_ptr_array_foreach (priv->maps, (GFunc)on_prepare_bind_maps_cb, priv->shader);

  /* Then the lights if there is any */
  /* TODO: we need to setup the lights here, just traverse the lights array
   * and setup the light components: 1.Position 2.Color 3.Intensity etc. */

  /* call the set uniforms virtual method */
  if (FORGER_MATERIAL_GET_CLASS (self)->set_uniforms)
    FORGER_MATERIAL_GET_CLASS (self)->set_uniforms (self, priv->shader);
}


/*
 * -----< G_OBJECT CLASS IMPLEMENTATION >----- *
 */
static void
forger_material_finalize (GObject *object)
{
  ForgerMaterial *self = (ForgerMaterial *)object;
  ForgerMaterialPrivate *priv = forger_material_get_instance_private (self);

  g_clear_object (&priv->shader);
  g_ptr_array_free (priv->maps, TRUE);
  g_ptr_array_free (priv->lights, FALSE);

  G_OBJECT_CLASS (forger_material_parent_class)->finalize (object);
}

static void
forger_material_get_property (GObject    *object,
                              guint       prop_id,
                              GValue     *value,
                              GParamSpec *pspec)
{
  ForgerMaterial *self = FORGER_MATERIAL (object);

  switch (prop_id)
    {
    case PROP_NAME:
      g_value_set_string (value, forger_material_get_name (self));
      break;

    case PROP_SHADER:
      g_value_take_object (value, forger_material_ref_shader (self));
      break;

    case PROP_MAPS:
      g_value_set_boxed (value, forger_material_get_maps (self));
      break;

    case PROP_LIGHTS:
      g_value_set_boxed (value, forger_material_get_lights (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
forger_material_set_property (GObject      *object,
                              guint         prop_id,
                              const GValue *value,
                              GParamSpec   *pspec)
{
  ForgerMaterial *self = FORGER_MATERIAL (object);

  switch (prop_id)
    {
    case PROP_NAME:
      forger_material_set_name (self, g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
forger_material_class_init (ForgerMaterialClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  ForgerEntityComponentClass *fecc_class = FORGER_ENTITY_COMPONENT_CLASS (klass);

  object_class->finalize = forger_material_finalize;
  object_class->get_property = forger_material_get_property;
  object_class->set_property = forger_material_set_property;

  fecc_class->get_display_name = forger_material_get_display_name;
  fecc_class->get_icon_name = forger_material_get_icon_name;
  fecc_class->get_description = forger_material_get_description;
  fecc_class->prepare = forger_material_real_prepare;

  /**
   * ForgerMaterial:name:
   *
   * The "name" property is the name that will be viewed in the material
   * editor or used by the system to refer to this material.
   *
   * Since: 0.1
   */
  properties [PROP_NAME] =
    g_param_spec_string ("name", NULL, NULL,
                         _("Dummy"),
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  /**
   * ForgerMaterial:shader:
   *
   * The "shader" property is the #ForgerShader program that %self
   * is built around.
   *
   * Since: 0.1
   */
  properties [PROP_SHADER] =
    g_param_spec_object ("shader", NULL, NULL,
                         FORGER_TYPE_SHADER,
                         (G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));

  /**
   * ForgerMaterial:maps:
   *
   * The "maps" property is the array of #ForgerMaps that are
   * provided to the fragment shader as textures.
   *
   * Since: 0.1
   */
  properties [PROP_MAPS] =
    g_param_spec_boxed ("maps", NULL, NULL,
                        G_TYPE_PTR_ARRAY,
                        (G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));

  /**
   * ForgerMaterial:lights:
   *
   * The "lights" property is the array of lights that are provided
   * to the fragment shader for lighting calculations.
   *
   * Since: 0.1
   */
  properties [PROP_LIGHTS] =
    g_param_spec_boxed ("lights", NULL, NULL,
                        G_TYPE_PTR_ARRAY,
                        (G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
forger_material_init (ForgerMaterial *self)
{
  ForgerMaterialPrivate *priv = forger_material_get_instance_private (self);

  priv->maps = g_ptr_array_new ();
  priv->lights = g_ptr_array_new ();
}


/*
 * -----< PROPERTIES >----- *
 */
gchar *
forger_material_get_name (ForgerMaterial *self)
{
  ForgerMaterialPrivate *priv = forger_material_get_instance_private (self);
  g_return_val_if_fail (FORGER_IS_MATERIAL (self), NULL);
  return priv->name;
}

void
forger_material_set_name (ForgerMaterial *self,
                          const gchar    *setting)
{
  ForgerMaterialPrivate *priv = forger_material_get_instance_private (self);
  g_return_if_fail (FORGER_IS_MATERIAL (self));
  g_return_if_fail (setting != NULL);

  if (g_set_str (&priv->name, setting))
    g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_NAME]);
}

/**
 * forger_material_ref_shader:
 *
 * Get the shader program and increase its reference count by 1.
 *
 * Returns: (transfer full): a #ForgerShader
 */
ForgerShader *
forger_material_ref_shader (ForgerMaterial *self)
{
  ForgerMaterialPrivate *priv = forger_material_get_instance_private (self);
  g_return_val_if_fail (FORGER_IS_MATERIAL (self), NULL);
  return g_object_ref (priv->shader);
}

/**
 * forger_material_get_maps:
 * @self: a #ForgerMaterial
 *
 * Gets the array of #ForgerMaps used by @self.
 *
 * Returns: (transfer full) (element-type Forger.Map): a #GPtrArray
 *   containing #ForgerMap elements.
 */
GPtrArray *
forger_material_get_maps (ForgerMaterial *self)
{
  ForgerMaterialPrivate *priv = forger_material_get_instance_private (self);
  g_return_val_if_fail (FORGER_IS_MATERIAL (self), NULL);
  return priv->maps;
}

/**
 * forger_material_get_lights:
 * @self: a #ForgerMaterial
 *
 * Gets the array of affecting lights.
 * The affecting lights are the lights provided by the engine
 * that will affect how @self will look and behave.
 *
 * Returns: (transfer full) (element-type Forger.Light): a #GPtrArray
 *   containing #ForgerLight elements.
 */
GPtrArray *
forger_material_get_lights (ForgerMaterial *self)
{
  ForgerMaterialPrivate *priv = forger_material_get_instance_private (self);
  g_return_val_if_fail (FORGER_IS_MATERIAL (self), NULL);
  return priv->maps;
}


/*
 * -----< PRIVATE API >----- *
 */
void
_forger_material_set_shader (ForgerMaterial *self,
                             ForgerShader   *shader)
{
  ForgerMaterialPrivate *priv = forger_material_get_instance_private (self);

  g_return_if_fail (FORGER_IS_MATERIAL (self));
  g_return_if_fail (FORGER_IS_SHADER (shader));

  if (g_set_object (&priv->shader, shader))
    g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_SHADER]);
}


/*
 * -----< MAPS HANDLING >----- *
 */
void
forger_material_add_map (ForgerMaterial    *self,
                         ForgerMap         *map,
                         ForgerTextureSlot  slot)
{
  ForgerMaterialPrivate *priv = forger_material_get_instance_private (self);

  FORGER_ENTRY;
  g_return_if_fail (FORGER_IS_MATERIAL (self));
  g_return_if_fail (FORGER_IS_MAP (map));
  g_return_if_fail (FORGER_IS_TEXTURE_SLOT (slot));

  if (!priv->shader)
    FORGER_EXIT;

  forger_map_set_buffer (map, NULL);
  forger_map_bind (map, slot);
  g_ptr_array_add (priv->maps, map);

  FORGER_EXIT;
}
