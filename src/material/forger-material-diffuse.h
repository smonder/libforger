/*
 * forger-material-diffuse.h
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#if !defined(FORGER_INSIDE) && !defined(LIBFORGER_COMPILATION)
#error "Only <libforger.h> can be included directly."
#endif

#include <gdk/gdk.h>

#include "forger-material.h"
#include "forger-version-macros.h"

G_BEGIN_DECLS

#define FORGER_TYPE_MATERIAL_DIFFUSE (forger_material_diffuse_get_type())

FORGER_AVAILABLE_IN_ALL
G_DECLARE_FINAL_TYPE (ForgerMaterialDiffuse, forger_material_diffuse, FORGER, MATERIAL_DIFFUSE, ForgerMaterial)

FORGER_AVAILABLE_IN_ALL
ForgerMaterial * forger_material_diffuse_new            (const gchar *           name);

FORGER_AVAILABLE_IN_ALL
ForgerMap *      forger_material_diffuse_get_map        (ForgerMaterialDiffuse * self);
FORGER_AVAILABLE_IN_ALL
void             forger_material_diffuse_set_map        (ForgerMaterialDiffuse * self,
                                                         ForgerMap *             map);

FORGER_AVAILABLE_IN_ALL
GdkRGBA *        forger_material_diffuse_get_color      (ForgerMaterialDiffuse * self);
FORGER_AVAILABLE_IN_ALL
void             forger_material_diffuse_set_color      (ForgerMaterialDiffuse * self,
                                                         const GdkRGBA *         color);
FORGER_AVAILABLE_IN_ALL
void             forger_material_diffuse_set_color_name (ForgerMaterialDiffuse * self,
                                                         const gchar *           color);

G_END_DECLS
