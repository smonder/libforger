/*
 * forger-map-image.h
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#if !defined(FORGER_INSIDE) && !defined(LIBFORGER_COMPILATION)
#error "Only <libforger.h> can be included directly."
#endif

#include "forger-map.h"
#include "forger-version-macros.h"

G_BEGIN_DECLS

#define FORGER_TYPE_MAP_IMAGE (forger_map_image_get_type())

FORGER_AVAILABLE_IN_ALL
G_DECLARE_FINAL_TYPE (ForgerMapImage, forger_map_image, FORGER, MAP_IMAGE, ForgerMap)

FORGER_AVAILABLE_IN_ALL
ForgerMap *   forger_map_image_new_from_file      (const gchar *    file_path) G_GNUC_WARN_UNUSED_RESULT;
FORGER_AVAILABLE_IN_ALL
ForgerMap *   forger_map_image_new_from_resources (const gchar *    resource_path) G_GNUC_WARN_UNUSED_RESULT;

FORGER_AVAILABLE_IN_ALL
gchar *       forger_map_image_peek_path          (ForgerMapImage * self);

G_END_DECLS
