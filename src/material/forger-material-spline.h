/*
 * forger-material-spline.h
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#if !defined(FORGER_INSIDE) && !defined(LIBFORGER_COMPILATION)
#error "Only <libforger.h> can be included directly."
#endif

#include <gdk/gdk.h>

#include "forger-material.h"
#include "forger-version-macros.h"

G_BEGIN_DECLS

#define FORGER_TYPE_MATERIAL_SPLINE (forger_material_spline_get_type())

FORGER_AVAILABLE_IN_ALL
G_DECLARE_FINAL_TYPE (ForgerMaterialSpline, forger_material_spline, FORGER, MATERIAL_SPLINE, ForgerMaterial)

FORGER_AVAILABLE_IN_ALL
ForgerMaterial * forger_material_spline_new             (const gchar *           name);

FORGER_AVAILABLE_IN_ALL
GdkRGBA *        forger_material_spline_get_color       (ForgerMaterialSpline * self);
FORGER_AVAILABLE_IN_ALL
void             forger_material_spline_set_color       (ForgerMaterialSpline * self,
                                                         const GdkRGBA *        color);
FORGER_AVAILABLE_IN_ALL
void             forger_material_spline_set_color_name  (ForgerMaterialSpline * self,
                                                         const gchar *          color);
