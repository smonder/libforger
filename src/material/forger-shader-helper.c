/*
 * forger-shader-helper.c
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
#define G_LOG_DOMAIN "forger-shader-helper"

#include "config.h"
#include "forger-debug.h"

#include "forger-shader-helper.h"

struct _ForgerShaderHelper
{
  ForgerShader parent_instance;
};

G_DEFINE_FINAL_TYPE (ForgerShaderHelper, forger_shader_helper, FORGER_TYPE_SHADER)


/*
 * -----< G_OBJECT CLASS IMPLEMENTATION >----- *
 */
static void
forger_shader_helper_class_init (ForgerShaderHelperClass *klass)
{
}

static void
forger_shader_helper_init (ForgerShaderHelper *self)
{
}


/*
 * -----< CONSTRUCTORS >----- *
 */
/**
 * forger_shader_helper_new:
 * @name: a #gchar: the name of the shader
 * @vertex_path: a #gchar: the resource path of the vertex shader
 * @pixel_path: a #gchar: the resource path of the pixel shader
 *
 * Creates a new #ForgerShaderHelper object
 *
 * Returns: (transfer full) (nullable): a newly created #ForgerShader or %NULL
 */
ForgerShader *
forger_shader_helper_new (const gchar  *name,
                          const gchar  *vertex_path,
                          const gchar  *pixel_path,
                          GError      **error)
{
  g_autoptr(ForgerShader) helper_shader = NULL;
  g_autoptr(ForgerShaderModule) vs_module = NULL;
  g_autoptr(ForgerShaderModule) ps_module = NULL;

  g_return_val_if_fail (name != NULL, NULL);
  g_return_val_if_fail (vertex_path != NULL, NULL);
  g_return_val_if_fail (pixel_path != NULL, NULL);
  g_return_val_if_fail (error != NULL || *error != NULL, NULL);

  helper_shader = g_object_new (FORGER_TYPE_SHADER_HELPER,
                                "name", name,
                                NULL);

  vs_module = forger_shader_module_new_from_resource (FORGER_SHADER_MODULE_VERTEX,
                                                      vertex_path, error);
  ps_module = forger_shader_module_new_from_resource (FORGER_SHADER_MODULE_PIXEL,
                                                      pixel_path, error);

  if (!vs_module || !ps_module)
    return NULL;

  forger_shader_add_module (helper_shader, vs_module);
  forger_shader_add_module (helper_shader, ps_module);

  if (!forger_shader_link_modules (helper_shader, error))
    return NULL;

  return g_steal_pointer (&helper_shader);
}

