/*
 * forger-map.h
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#if !defined(FORGER_INSIDE) && !defined(LIBFORGER_COMPILATION)
#error "Only <libforger.h> can be included directly."
#endif

#include <gdk-pixbuf/gdk-pixbuf.h>
#include "forger-types.h"
#include "forger-version-macros.h"

G_BEGIN_DECLS

#define FORGER_TYPE_MAP (forger_map_get_type())

FORGER_AVAILABLE_IN_ALL
G_DECLARE_DERIVABLE_TYPE (ForgerMap, forger_map, FORGER, MAP, GObject)

struct _ForgerMapClass
{
  GObjectClass parent_class;

  GdkPixbuf * (* create_buffer) (ForgerMap * map,
                                 GError **   error);
};


FORGER_AVAILABLE_IN_ALL
gboolean          forger_map_set_buffer    (ForgerMap *       self,
                                            GError **         error);

FORGER_AVAILABLE_IN_ALL
GdkPixbuf *       forger_map_ref_buffer    (ForgerMap *       self) G_GNUC_WARN_UNUSED_RESULT;
FORGER_AVAILABLE_IN_ALL
gint              forger_map_get_width     (ForgerMap *       self);
FORGER_AVAILABLE_IN_ALL
gint              forger_map_get_height    (ForgerMap *       self);
FORGER_AVAILABLE_IN_ALL
ForgerTextureSlot forger_map_get_slot      (ForgerMap *       self);

FORGER_AVAILABLE_IN_ALL
void              forger_map_bind          (ForgerMap *       self,
                                            ForgerTextureSlot slot);
FORGER_AVAILABLE_IN_ALL
void              forger_map_unbind        (ForgerMap *       self);

G_END_DECLS
