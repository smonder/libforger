/*
 * forger-map-image.c
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "forger-map-image"

#include "config.h"
#include "forger-debug.h"

#include "forger-map-image.h"
#include "forger-material-error.h"

struct _ForgerMapImage
{
  ForgerMap parent_instance;

  /* We can store one path string: it is either a resource path or a
   * file path.
   */
  gchar * path;

  /* And a flag that we will set when we provide the path */
  guint is_resource : 1;
};

G_DEFINE_FINAL_TYPE (ForgerMapImage, forger_map_image, FORGER_TYPE_MAP)

enum {
  PROP_0,
  PROP_PATH,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];


/*
 * -----< CONSTRUCT-ONLY PROPERTIES SETTERS >----- *
 */
static void
forger_map_image_set_path (ForgerMapImage *self,
                           const gchar    *path)
{
  g_return_if_fail (FORGER_IS_MAP_IMAGE (self));
  g_return_if_fail (path != NULL);

  if (g_set_str (&self->path, path))
    g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_PATH]);
}


/*
 * -----< FORGER_MAP CLASS IMPLEMENTATION >----- *
 */
static GdkPixbuf *
forger_map_image_real_create_buffer (ForgerMap  *map,
                                     GError    **error)
{
  ForgerMapImage *self = (ForgerMapImage *)map;
  g_autoptr(GdkPixbuf) buffer = NULL;

  g_assert (FORGER_IS_MAP_IMAGE (self));
  g_assert (error == NULL || *error == NULL);

  if (self->path == NULL)
    {
      g_set_error (error,
                   FORGER_MATERIAL_ERROR,
                   FORGER_MAP_NO_FILE_PROVIDED,
                   "Error creating map buffer. no file was provided");

      return NULL;
    }

  if (self->is_resource)
    buffer = gdk_pixbuf_new_from_resource (self->path, error);
  else
    buffer = gdk_pixbuf_new_from_file (self->path, error);

  return g_steal_pointer (&buffer);
}


/*
 * -----< G_OBJECT CLASS IMPLEMENTATION >----- *
 */
static void
forger_map_image_finalize (GObject *object)
{
  ForgerMapImage *self = (ForgerMapImage *)object;

  g_clear_pointer (&self->path, g_free);

  G_OBJECT_CLASS (forger_map_image_parent_class)->finalize (object);
}

static void
forger_map_image_get_property (GObject    *object,
                               guint       prop_id,
                               GValue     *value,
                               GParamSpec *pspec)
{
  ForgerMapImage *self = FORGER_MAP_IMAGE (object);

  switch (prop_id)
    {
    case PROP_PATH:
      g_value_take_string (value, forger_map_image_peek_path (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
forger_map_image_set_property (GObject      *object,
                               guint         prop_id,
                               const GValue *value,
                               GParamSpec   *pspec)
{
  ForgerMapImage *self = FORGER_MAP_IMAGE (object);

  switch (prop_id)
    {
    case PROP_PATH:
      forger_map_image_set_path (self, g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
forger_map_image_class_init (ForgerMapImageClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  ForgerMapClass *map_class = FORGER_MAP_CLASS (klass);

  object_class->finalize = forger_map_image_finalize;
  object_class->get_property = forger_map_image_get_property;
  object_class->set_property = forger_map_image_set_property;

  map_class->create_buffer = forger_map_image_real_create_buffer;

  /**
   * ForgerMapImage:path:
   *
   * The "path" property is the path to the image file either from GResource
   * or File.
   *
   * Since: 0.1
   */
  properties [PROP_PATH] =
    g_param_spec_string ("path", NULL, NULL,
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY |
                          G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
forger_map_image_init (ForgerMapImage *self)
{
}


/*
 * -----< CONSTRUCTORS >----- *
 */
/**
 * forger_map_image_new_from_file:
 * @file_path: a #gchar: a valid file path
 *
 * Create a new #ForgerMapImage from file
 *
 * Returns: (transfer full) (nullable): a newly created #ForgerMap or %NULL.
 */
ForgerMap *
forger_map_image_new_from_file (const gchar *file_path)
{
  g_autoptr(ForgerMapImage) self = NULL;

  g_return_val_if_fail (file_path != NULL, NULL);

  self = g_object_new (FORGER_TYPE_MAP_IMAGE,
                       "path", file_path, NULL);

  if (self != NULL)
    self->is_resource = !!FALSE;

  return FORGER_MAP (g_steal_pointer (&self));
}

/**
 * forger_map_image_new_from_resource:
 * @resource_path: a #gchar: a valid GResource path
 *
 * Create a new #ForgerMapImage from resource
 *
 * Returns: (transfer full) (nullable): a newly created #ForgerMap or %NULL.
 */
ForgerMap *
forger_map_image_new_from_resources (const gchar *resource_path)
{
  g_autoptr(ForgerMapImage) self = NULL;

  g_return_val_if_fail (resource_path != NULL, NULL);

  self = g_object_new (FORGER_TYPE_MAP_IMAGE,
                       "path", resource_path, NULL);

  if (self != NULL)
    self->is_resource = !!TRUE;

  return FORGER_MAP (g_steal_pointer (&self));
}


/*
 * -----< PROPERTIES >----- *
 */
gchar *
forger_map_image_peek_path (ForgerMapImage *self)
{
  g_return_val_if_fail (FORGER_IS_MAP_IMAGE (self), NULL);
  return g_strdup (self->path);
}



