/*
 * forger-map.c
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "forger-map"

#include "config.h"
#include "forger-debug.h"

#include <epoxy/gl.h>

#include "forger-map.h"
#include "forger-enums.h"
#include "forger-material-error.h"

typedef struct
{
  /* We need a unique ID to be generated and used by OpenGL API. */
  guint ID_;

  /* We need to keep a copy of the buffer of the image. */
  GdkPixbuf * buffer;

  /* A texture slot to sample the texture from */
  ForgerTextureSlot slot;
} ForgerMapPrivate;

G_DEFINE_ABSTRACT_TYPE_WITH_PRIVATE (ForgerMap, forger_map, G_TYPE_OBJECT)

enum {
  PROP_0,
  PROP_BUFFER,
  PROP_WIDTH,
  PROP_HEIGHT,
  PROP_SLOT,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];


/*
 * G_OBJECT CLASS IMPLEMENTATION
 */
static void
forger_map_finalize (GObject *object)
{
  ForgerMap *self = (ForgerMap *)object;
  ForgerMapPrivate *priv = forger_map_get_instance_private (self);

  g_clear_pointer (&priv->buffer, g_object_unref);
  glDeleteTextures (1, &priv->ID_);

  G_OBJECT_CLASS (forger_map_parent_class)->finalize (object);
}

static void
forger_map_get_property (GObject    *object,
                         guint       prop_id,
                         GValue     *value,
                         GParamSpec *pspec)
{
  ForgerMap *self = FORGER_MAP (object);

  switch (prop_id)
    {
    case PROP_BUFFER:
      g_value_take_object (value, forger_map_ref_buffer (self));
      break;

    case PROP_WIDTH:
      g_value_set_int (value, forger_map_get_width (self));
      break;

    case PROP_HEIGHT:
      g_value_set_int (value, forger_map_get_height (self));
      break;

    case PROP_SLOT:
      g_value_set_enum (value, forger_map_get_slot (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
forger_map_class_init (ForgerMapClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = forger_map_finalize;
  object_class->get_property = forger_map_get_property;

  /**
   * ForgerMap:buffer:
   *
   * The "buffer" property is the underlying #GdkPixbuf object that holds the
   * pixel information buffer of %self.
   *
   * Since: 0.1
   */
  properties [PROP_BUFFER] =
    g_param_spec_object ("buffer", NULL, NULL,
                         GDK_TYPE_PIXBUF,
                         (G_PARAM_READABLE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  /**
   * ForgerMap:width:
   *
   * The "width" property.
   *
   * Since: 0.1
   */
  properties [PROP_WIDTH] =
    g_param_spec_int ("width", NULL, NULL,
                      - G_MAXINT, G_MAXINT, 0,
                      (G_PARAM_READABLE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  /**
   * ForgerMap:height:
   *
   * The "height" property.
   *
   * Since: 0.1
   */
  properties [PROP_HEIGHT] =
    g_param_spec_int ("height", NULL, NULL,
                      - G_MAXINT, G_MAXINT, 0,
                      (G_PARAM_READABLE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  /**
   * ForgerMap:slot:
   *
   * The "slot" property is the GL texture slot that %self is bound to.
   * this is used primarily by the #ForgerMaterial to determine what this
   * map is used for.
   *
   * Since: 0.1
   */
  properties [PROP_SLOT] =
    g_param_spec_enum ("slot", NULL, NULL,
                       FORGER_TYPE_TEXTURE_SLOT, FORGER_TEXTURE_INVALID,
                       (G_PARAM_READABLE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
forger_map_init (ForgerMap *self)
{
  ForgerMapPrivate *priv = forger_map_get_instance_private (self);

  priv->ID_ = 0;
  priv->slot = FORGER_TEXTURE_INVALID;
}


/*
 * -----< VIRTUAL METHODS >----- *
 */
/**
 * forger_map_set_buffer:
 * @self: a #ForgerMap.
 * @error: a #GError or %NULL: The return location for a recoverable error.
 *
 * This function is responsible for calling the create buffer implementation
 * form sub-classes and assigning the result to the buffer member.
 *
 * The GLTextures then is going to be created.
 *
 * Returns: %TRUE if the buffer is created and assigned successfully, %FALSE otherwise.
 */
gboolean
forger_map_set_buffer (ForgerMap  *self,
                       GError    **error)
{
  ForgerMapPrivate *priv = forger_map_get_instance_private (self);

  g_return_val_if_fail (FORGER_IS_MAP (self), FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

  if (priv->ID_ != 0)
    {
      g_set_error (error,
                   FORGER_MATERIAL_ERROR,
                   FORGER_MAP_ALREADY_PREPARED,
                   "The map is already prepared. no thing to do");
      return FALSE;
    }

  if (FORGER_MAP_GET_CLASS (self)->create_buffer)
    priv->buffer = FORGER_MAP_GET_CLASS (self)->create_buffer (self, error);

  if (priv->buffer == NULL)
    return FALSE;

  glGenTextures (1, &priv->ID_);
  glBindTexture (GL_TEXTURE_2D, priv->ID_);
  /* glTextureParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR); */
  /* glTextureParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); */
  /* glTextureParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE); */
  /* glTextureParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE); */
  glTexImage2D (GL_TEXTURE_2D, 0, GL_RGBA8,
                gdk_pixbuf_get_width (priv->buffer),
                gdk_pixbuf_get_height (priv->buffer),
                0, GL_RGBA, GL_UNSIGNED_BYTE,
                gdk_pixbuf_get_pixels (gdk_pixbuf_flip (priv->buffer, FALSE)));
  glGenerateMipmap (GL_TEXTURE_2D);
  glBindTexture (GL_TEXTURE_2D, 0);

  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_BUFFER]);
  return TRUE;
}


/*
 * -----< PROPERTIES >----- *
 */
/**
 * forger_map_ref_buffer:
 * @self: a #ForgerMap.
 *
 * Get the underlying #GdkPixbuf and increase its reference count
 * by 1.
 *
 * Returns: (transfer full) (nullable): a #GdkPixbuf or %NULL
 */
GdkPixbuf *
forger_map_ref_buffer (ForgerMap * self)
{
  ForgerMapPrivate *priv = forger_map_get_instance_private (self);
  g_return_val_if_fail (FORGER_IS_MAP (self), NULL);
  return g_object_ref (priv->buffer);
}

gint
forger_map_get_width (ForgerMap * self)
{
  ForgerMapPrivate *priv = forger_map_get_instance_private (self);
  g_return_val_if_fail (FORGER_IS_MAP (self), 0);

  return gdk_pixbuf_get_width (priv->buffer);
}

gint
forger_map_get_height (ForgerMap * self)
{
  ForgerMapPrivate *priv = forger_map_get_instance_private (self);
  g_return_val_if_fail (FORGER_IS_MAP (self), 0);

  return gdk_pixbuf_get_height (priv->buffer);
}

/**
 * forger_map_get_slot:
 *
 * Get the "slot" property value
 *
 * Returns: a #ForgerTextureSlot: the texture slot that %self is bound to.
 */
ForgerTextureSlot
forger_map_get_slot (ForgerMap *self)
{
  ForgerMapPrivate *priv = forger_map_get_instance_private (self);
  g_return_val_if_fail (FORGER_IS_MAP (self), FORGER_TEXTURE_INVALID);
  return priv->slot;
}


/*
 * -----< USING THE MAP >----- *
 */
/**
 * forger_map_bind:
 * @self: a #ForgerMap.
 * @slot: a #ForgerTextureSlot: the use type of the map.
 *
 * Bind the map to the active shader at @slot use case.
 *
 */
void
forger_map_bind (ForgerMap         *self,
                 ForgerTextureSlot  slot)
{
  ForgerMapPrivate *priv = forger_map_get_instance_private (self);

  g_return_if_fail (FORGER_IS_MAP (self));
  g_return_if_fail (slot != FORGER_TEXTURE_INVALID);

  if (priv->ID_ == 0)
    return;

  priv->slot = slot;
  glActiveTexture (GL_TEXTURE0 + slot);
  glBindTexture (GL_TEXTURE_2D, priv->ID_);

  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_SLOT]);
}

/**
 * forger_map_unbind:
 * @self: a #ForgerMap.
 *
 * Unbind the map from the shader.
 *
 */
void
forger_map_unbind (ForgerMap *self)
{
  ForgerMapPrivate *priv = forger_map_get_instance_private (self);

  g_return_if_fail (FORGER_IS_MAP (self));

  if (priv->ID_ == 0 ||
      priv->slot == FORGER_TEXTURE_INVALID)
    return;

  glActiveTexture (GL_TEXTURE0 + priv->slot);
  glBindTexture (GL_TEXTURE_2D, 0);
}


