/*
 * forger-shader-module.c
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "forger-shader-module"

#include "config.h"
#include "forger-debug.h"

#include <epoxy/gl.h>

#include "forger-material-error.h"
#include "forger-shader-module.h"
#include "forger-shader-private.h"
#include "forger-enums.h"

struct _ForgerShaderModule
{
  GObject parent_instance;

  gint ID_;
  ForgerShaderModuleType type_;
};

G_DEFINE_FINAL_TYPE (ForgerShaderModule, forger_shader_module, G_TYPE_OBJECT)

static guint
module_type_to_gl_enum (ForgerShaderModuleType t)
{
  switch (t)
  {
  case FORGER_SHADER_MODULE_VERTEX: return GL_VERTEX_SHADER;
  case FORGER_SHADER_MODULE_PIXEL: return GL_FRAGMENT_SHADER;
  case FORGER_SHADER_MODULE_GEOMETRY: return GL_GEOMETRY_SHADER;
  default: g_assert_not_reached ();
  }

  return 0;
}

/*
 * G_OBJECT CLASS IMPLEMENTATION
 */
static void
forger_shader_module_finalize (GObject *object)
{
  ForgerShaderModule *self = (ForgerShaderModule *)object;

  glDeleteShader (self->ID_);

  G_OBJECT_CLASS (forger_shader_module_parent_class)->finalize (object);
}

static void
forger_shader_module_class_init (ForgerShaderModuleClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = forger_shader_module_finalize;
}

static void
forger_shader_module_init (ForgerShaderModule *self)
{
}


/*
 * -----< CONSTRUCTORS >----- *
 */
static guint
create_module (ForgerShaderModuleType   type,
               const gchar             *src,
               GError                 **err)
{
  guint shader;
  int status;

  FORGER_ENTRY;

  shader = glCreateShader (module_type_to_gl_enum (type));
  glShaderSource (shader, 1, &src, 0);
  glCompileShader (shader);

  glGetShaderiv (shader, GL_COMPILE_STATUS, &status);
  if (status == GL_FALSE)
    {
      int log_len;
      char *buffer;

      glGetShaderiv (shader, GL_INFO_LOG_LENGTH, &log_len);

      buffer = g_malloc (log_len + 1);
      glGetShaderInfoLog (shader, log_len, NULL, buffer);



      g_set_error (err,
                   FORGER_MATERIAL_ERROR,
                   FORGER_SHADER_MODULE_COMPILATION_FAILED,
                   "Compile failure in %s shader module:\n%s",
                   g_enum_to_string (FORGER_TYPE_SHADER_MODULE_TYPE, type),
                   buffer);

      g_free (buffer);
      glDeleteShader (shader);
      FORGER_RETURN (0);
    }

  FORGER_RETURN (shader);
}

/**
 * forger_shader_module_new_from_file:
 *
 * Creates a #ForgerShaderModule from a file.
 *
 * Returns: (transfer full) (nullable): a #ForgerShaderModule.
 */
ForgerShaderModule *
forger_shader_module_new_from_file (ForgerShaderModuleType   type,
                                    const gchar             *file_path,
                                    GError                 **error)
{
  g_autoptr(ForgerShaderModule) self = NULL;
  g_autoptr(GFile) shader_file = NULL;
  gchar * shader_source_code;
  gsize source_length = 0;
  guint compile_result = 0;

  FORGER_ENTRY;
  g_return_val_if_fail (file_path != NULL, NULL);
  g_return_val_if_fail (error != NULL || *error != NULL, NULL);

  shader_file = g_file_new_for_path (file_path);

  if (!g_file_load_contents (shader_file, NULL, &shader_source_code, &source_length, NULL, error))
    FORGER_RETURN (NULL);

  compile_result = create_module (type, shader_source_code, error);
  if (compile_result == 0)
    FORGER_RETURN (NULL);

  self = g_object_new (FORGER_TYPE_SHADER_MODULE, NULL);
  self->type_ = type;
  self->ID_ = compile_result;

  FORGER_RETURN ( g_steal_pointer (&self) );
}

/**
 * forger_shader_module_new_from_resource:
 *
 * Creates a #ForgerShaderModule from a GResource.
 *
 * Returns: (transfer full) (nullable): a #ForgerShaderModule.
 */
ForgerShaderModule *
forger_shader_module_new_from_resource (ForgerShaderModuleType   type,
                                        const gchar             *res_path,
                                        GError                 **error)
{
  g_autoptr (ForgerShaderModule) self = NULL;
  GBytes * shader_module_source;
  guint compile_result;
  GError *err;

  FORGER_ENTRY;
  g_return_val_if_fail (res_path != NULL, NULL);
  g_return_val_if_fail (error != NULL || *error != NULL, NULL);

  shader_module_source = g_resources_lookup_data (res_path, G_RESOURCE_LOOKUP_FLAGS_NONE, &err);
  if (!shader_module_source)
    {
      g_set_error (error,
                   FORGER_MATERIAL_ERROR,
                   FORGER_SHADER_MODULE_GET_SOURCE_FAILED,
                   "Creating shader module from resources has failed: %s", err->message);
      g_clear_error (&err);
      FORGER_RETURN (NULL);
    }

  compile_result = create_module (type, g_bytes_get_data (shader_module_source, NULL), error);
  if (compile_result == 0)
    FORGER_RETURN (NULL);

  self = g_object_new (FORGER_TYPE_SHADER_MODULE, NULL);
  self->type_ = type;
  self->ID_ = compile_result;

  FORGER_RETURN ( g_steal_pointer (&self) );
}


/*
 * -----< PRIVATE API >----- *
 */
guint
_forger_shader_module_get_id (ForgerShaderModule * self)
{
  g_return_val_if_fail (FORGER_IS_SHADER_MODULE (self), 0);
  return self->ID_;
}
