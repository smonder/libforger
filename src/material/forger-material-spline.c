/*
 * forger-material-spline.c
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
#define G_LOG_DOMAIN "forger-material-spline"

#include "config.h"
#include "forger-debug.h"

#include <glib/gi18n.h>

#include "forger-material-spline.h"
#include "forger-shader-private.h"

struct _ForgerMaterialSpline
{
  ForgerMaterial parent_instance;

  GdkRGBA color;
};

G_DEFINE_FINAL_TYPE (ForgerMaterialSpline, forger_material_spline, FORGER_TYPE_MATERIAL)

enum {
  PROP_0,
  PROP_COLOR,
  PROP_COLOR_NAME,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];


/*
 * -----< FORGER_MATERIAL CLASS IMPLEMENTATION >----- *
 */
static void
forger_material_spline_set_uniforms (ForgerMaterial *material,
                                      ForgerShader   *shader)
{
  ForgerMaterialSpline *self = FORGER_MATERIAL_SPLINE (material);

  g_assert (FORGER_IS_MATERIAL_SPLINE (self));
  g_assert (FORGER_IS_SHADER (shader));

  forger_shader_set_uniform_color (shader, "u_Color", &self->color);
}


/*
 * -----< G_OBJECT CLASS IMPLEMENTATION >----- *
 */
static void
forger_material_spline_get_property (GObject    *object,
                                     guint       prop_id,
                                     GValue     *value,
                                     GParamSpec *pspec)
{
  ForgerMaterialSpline *self = FORGER_MATERIAL_SPLINE (object);

  switch (prop_id)
    {
    case PROP_COLOR:
      g_value_set_boxed (value, forger_material_spline_get_color (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
forger_material_spline_set_property (GObject      *object,
                                     guint         prop_id,
                                     const GValue *value,
                                     GParamSpec   *pspec)
{
  ForgerMaterialSpline *self = FORGER_MATERIAL_SPLINE (object);

  switch (prop_id)
    {
    case PROP_COLOR:
      forger_material_spline_set_color (self, g_value_get_boxed (value));
      break;

    case PROP_COLOR_NAME:
      forger_material_spline_set_color_name (self, g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
forger_material_spline_class_init (ForgerMaterialSplineClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  ForgerMaterialClass *material_class = FORGER_MATERIAL_CLASS (klass);

  object_class->get_property = forger_material_spline_get_property;
  object_class->set_property = forger_material_spline_set_property;

  material_class->set_uniforms = forger_material_spline_set_uniforms;

  /**
   * ForgerMaterialSpline:color:
   *
   * The "color" property is the #GdkRGBA color that represents the color
   * value that is going to be rendered.
   *
   * Since: 0.1
   */
  properties [PROP_COLOR] =
    g_param_spec_boxed ("color", NULL, NULL,
                        GDK_TYPE_RGBA,
                        (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  /**
   * ForgerMaterialSpline:color-name:
   *
   * The "color-name" property is another way to set "ForgerMaterialSpline:color"
   * property, this time using a string.
   * The string should be a valid string that can be handled by #GdkRGBA.
   * Refer to the #GdkRGBA manual to see available options.
   *
   * Since: 0.1
   */
  properties [PROP_COLOR_NAME] =
    g_param_spec_string ("color-name", NULL, NULL,
                         NULL,
                         (G_PARAM_WRITABLE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
forger_material_spline_init (ForgerMaterialSpline *self)
{
  ForgerShader *spline_shader;
  ForgerShaderModule *vs_module;
  ForgerShaderModule *ps_module;
  GError *error = NULL;

  spline_shader = forger_shader_new (_("Spline Shader"));
  vs_module = forger_shader_module_new_from_resource (FORGER_SHADER_MODULE_VERTEX,
                                                      "/io/sam/libforger/spline-vertex.glsl", &error);
  ps_module = forger_shader_module_new_from_resource (FORGER_SHADER_MODULE_PIXEL,
                                                      "/io/sam/libforger/spline-pixel.glsl", &error);

  if (!vs_module || !ps_module)
    g_error ("error initializing Spline Shader: %s", error->message);

  forger_shader_add_module (spline_shader, vs_module);
  forger_shader_add_module (spline_shader, ps_module);

  if (!forger_shader_link_modules (spline_shader, &error))
    g_error ("error linking Spline Shader: %s", error->message);

  _forger_material_set_shader (FORGER_MATERIAL (self), spline_shader);
}


/*
 * -----< CONSTRUCTORS >----- *
 */
/**
 * forger_material_spline_new:
 * @name: a #gchar: a unique name identifier for the material.
 *
 * Creates a new #ForgerMaterialSpline
 *
 * Returns: (transfer full): a newly created #ForgerMaterial
 */
ForgerMaterial *
forger_material_spline_new (const gchar * name)
{
  return g_object_new (FORGER_TYPE_MATERIAL_SPLINE,
                       "name", name,
                       NULL);
}


/*
 * -----< PROPERTIES >----- *
 */
/**
 * forger_material_spline_get_color:
 * @self: a #ForgerMaterialSpline
 *
 * gets the "color" property of @self
 *
 * Returns: (transfer full): a #GdkRGBA
 */
GdkRGBA *
forger_material_spline_get_color (ForgerMaterialSpline *self)
{
  g_return_val_if_fail (FORGER_IS_MATERIAL_SPLINE (self), NULL);
  return &self->color;
}

/**
 * forger_material_spline_set_color:
 * @self: a #ForgerMaterialSpline
 * @color: a #GdkRGBA
 *
 * sets the "color" property of @self
 *
 */
void
forger_material_spline_set_color (ForgerMaterialSpline *self,
                                  const GdkRGBA        *color)
{
  GdkRGBA new_color;

  g_return_if_fail (FORGER_IS_MATERIAL_SPLINE (self));

  if (gdk_rgba_equal (&self->color, color))
    return;

  if (!color)
    {
      new_color.red = 0.0f;
      new_color.green = 0.0f;
      new_color.blue = 0.0f;
      new_color.alpha = 1.0f;
    }
  else
    {
      new_color.red = color->red;
      new_color.green = color->green;
      new_color.blue = color->blue;
      new_color.alpha = color->alpha;
    }

  self->color.red = new_color.red;
  self->color.green = new_color.green;
  self->color.blue = new_color.blue;
  self->color.alpha = new_color.alpha;

  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_COLOR]);
}

/**
 * forger_material_spline_set_color_name:
 * @self: a #ForgerMaterialSpline
 * @color: a #gchar: a valid color string.
 *
 * sets the "color" property of @self using the name of the color.
 * The provided color name "color", should be a valid string
 * that can be paresed using gdk_rgba_parse().
 *
 * FROM GDKRGBA MANUAL:
 * The string can be either one of:
 * - A standard name (Taken from the CSS specification).
 * - A hexadecimal value in the form “#rgb”, “#rrggbb”, “#rrrgggbbb” or ”#rrrrggggbbbb”
 * - A hexadecimal value in the form “#rgba”, “#rrggbbaa”, or ”#rrrrggggbbbbaaaa”
 * - A RGB color in the form “rgb(r,g,b)” (In this case the color will have full opacity)
 * - A RGBA color in the form “rgba(r,g,b,a)”
 * - A HSL color in the form “hsl(hue, saturation, lightness)”
 * - A HSLA color in the form “hsla(hue, saturation, lightness, alpha)”
 *
 * Where “r”, “g”, “b” and “a” are respectively the red, green, blue and alpha color values.
 * In the last two cases, “r”, “g”, and “b” are either integers in the range 0
 * to 255 or percentage values in the range 0% to 100%, and a is a floating point
 * value in the range 0 to 1.
 *
 */
void
forger_material_spline_set_color_name (ForgerMaterialSpline *self,
                                       const gchar          *color)
{
  GdkRGBA new_color;

  FORGER_ENTRY;
  g_return_if_fail (FORGER_IS_MATERIAL_SPLINE (self));
  g_return_if_fail (color != NULL);

  if (gdk_rgba_parse (&new_color, color))
    forger_material_spline_set_color (self, &new_color);
  else
    g_critical ("Failed to parse the color from the provided string");

  FORGER_EXIT;
}

