/*
 * forger-material-error.h
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#if !defined(FORGER_INSIDE) && !defined(LIBFORGER_COMPILATION)
#error "Only <libforger.h> can be included directly."
#endif

#include <glib.h>
#include "forger-version-macros.h"

G_BEGIN_DECLS

#define FORGER_MATERIAL_ERROR (forger_material_error_quark())

typedef enum
{
  FORGER_SHADER_MODULE_GET_SOURCE_FAILED = 1,
  FORGER_SHADER_MODULE_COMPILATION_FAILED,
  FORGER_SHADER_LINKING_FAILED,

  FORGER_MAP_NO_FILE_PROVIDED,
  FORGER_MAP_ALREADY_PREPARED,
} ForgerMaterialError;

FORGER_AVAILABLE_IN_ALL
GQuark         forger_material_error_quark           (void);

G_END_DECLS
