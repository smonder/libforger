/*
 * forger-gizmo.h
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#if !defined(FORGER_INSIDE) && !defined(LIBFORGER_COMPILATION)
#error "Only <libforger.h> can be included directly."
#endif

#include <graphene-gobject.h>
#include "forger-entity-component.h"

#include "forger-types.h"
#include "forger-version-macros.h"

G_BEGIN_DECLS

#define FORGER_TYPE_GIZMO (forger_gizmo_get_type())

FORGER_AVAILABLE_IN_ALL
G_DECLARE_FINAL_TYPE (ForgerGizmo, forger_gizmo, FORGER, GIZMO, ForgerEntityComponent)

FORGER_AVAILABLE_IN_ALL
ForgerGizmo *       forger_gizmo_new          (void) G_GNUC_WARN_UNUSED_RESULT;

FORGER_AVAILABLE_IN_ALL
void                forger_gizmo_set_location (ForgerGizmo *       self,
                                               graphene_point3d_t *location);
FORGER_AVAILABLE_IN_ALL
void                forger_gizmo_set_rotation (ForgerGizmo *       self,
                                               graphene_point3d_t *rotation);
FORGER_AVAILABLE_IN_ALL
void                forger_gizmo_set_scale    (ForgerGizmo *       self,
                                               graphene_point3d_t *scale);
FORGER_AVAILABLE_IN_ALL
void                forger_gizmo_set_scale_f  (ForgerGizmo *       self,
                                               const gfloat        factor);

FORGER_AVAILABLE_IN_ALL
graphene_point3d_t *forger_gizmo_get_location (ForgerGizmo *   self);
FORGER_AVAILABLE_IN_ALL
graphene_point3d_t *forger_gizmo_get_rotation (ForgerGizmo *   self);
FORGER_AVAILABLE_IN_ALL
graphene_point3d_t *forger_gizmo_get_scale    (ForgerGizmo *   self);

FORGER_AVAILABLE_IN_ALL
void                forger_gizmo_translate    (ForgerGizmo *   self,
                                               ForgerGizmoAxis axis,
                                               const gfloat    amount);
FORGER_AVAILABLE_IN_ALL
void                forger_gizmo_rotate       (ForgerGizmo *   self,
                                               ForgerGizmoAxis around_axis,
                                               const gfloat    amount);
FORGER_AVAILABLE_IN_ALL
void                forger_gizmo_scale        (ForgerGizmo *   self,
                                               ForgerGizmoAxis axis,
                                               const gfloat    amount);
FORGER_AVAILABLE_IN_ALL
void                forger_gizmo_scale_uni    (ForgerGizmo *   self,
                                               const gfloat    amount);

FORGER_AVAILABLE_IN_ALL
graphene_matrix_t * forger_gizmo_get_matrix   (ForgerGizmo *   self);

G_END_DECLS
