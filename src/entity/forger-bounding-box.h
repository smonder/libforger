/*
 * forger-bounding-box.h
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <graphene-gobject.h>

#include "forger-entity-component.h"
#include "forger-version-macros.h"

G_BEGIN_DECLS

#define FORGER_TYPE_BOUNDING_BOX (forger_bounding_box_get_type())

FORGER_AVAILABLE_IN_ALL
G_DECLARE_FINAL_TYPE (ForgerBoundingBox, forger_bounding_box, FORGER, BOUNDING_BOX, ForgerEntityComponent)

FORGER_AVAILABLE_IN_ALL
ForgerEntityComponent * forger_bounding_box_new                (void) G_GNUC_WARN_UNUSED_RESULT;

FORGER_AVAILABLE_IN_ALL
graphene_box_t *        forger_bounding_box_get_box            (ForgerBoundingBox * self);

FORGER_AVAILABLE_IN_ALL
void                    forger_bounding_box_update_from_buffer (ForgerBoundingBox * self,
                                                                GArray *            positions_buffer);
FORGER_AVAILABLE_IN_ALL
gboolean                forger_bounding_box_ray_intersects     (ForgerBoundingBox * self,
                                                                graphene_matrix_t * model_matrix,
                                                                graphene_ray_t *    ray);

G_END_DECLS
