/*
 * forger-mesh-components.h
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#if !defined(FORGER_INSIDE) && !defined(LIBFORGER_COMPILATION)
#error "Only <libforger.h> can be included directly."
#endif

#include <graphene.h>
#include "forger-types.h"
#include "forger-version-macros.h"

G_BEGIN_DECLS

typedef struct _ForgerMeshEdge ForgerMeshEdge;
struct _ForgerMeshEdge
{
  guint a_index;
  guint b_index;
};


/*
 * -----< MESH FACE COMPONENT >----- *
 */
#define FORGER_TYPE_MESH_FACE (forger_mesh_face_get_type ())

typedef struct _ForgerTriangle ForgerTriangle;
struct _ForgerTriangle
{
  guint a_index;
  guint b_index;
  guint c_index;
};

FORGER_AVAILABLE_IN_ALL
GType               forger_mesh_face_get_type         (void) G_GNUC_CONST;
FORGER_AVAILABLE_IN_ALL
ForgerMeshFace *    forger_mesh_face_new              (void);
FORGER_AVAILABLE_IN_ALL
ForgerMeshFace *    forger_mesh_face_copy             (ForgerMeshFace * self);
FORGER_AVAILABLE_IN_ALL
void                forger_mesh_face_free             (ForgerMeshFace * self);

FORGER_AVAILABLE_IN_ALL
ForgerMeshFace *    forger_mesh_face_next             (ForgerMeshFace * self);
FORGER_AVAILABLE_IN_ALL
void                forger_mesh_face_set_next         (ForgerMeshFace * self,
                                                       ForgerMeshFace * next);

FORGER_AVAILABLE_IN_ALL
void                forger_mesh_face_push_triangle    (ForgerMeshFace * self,
                                                       ForgerTriangle * triangle);
FORGER_AVAILABLE_IN_ALL
void                forger_mesh_face_pop_triangle     (ForgerMeshFace * self);
FORGER_AVAILABLE_IN_ALL
void                forger_mesh_face_foreach_triangle (ForgerMeshFace * self,
                                                       GFunc            func_,
                                                       gpointer         user_data);

FORGER_AVAILABLE_IN_ALL
void                forger_mesh_face_push_edge        (ForgerMeshFace * self,
                                                       ForgerMeshEdge * edge);
FORGER_AVAILABLE_IN_ALL
void                forger_mesh_face_pop_edge         (ForgerMeshFace * self);
FORGER_AVAILABLE_IN_ALL
void                forger_mesh_face_foreach_edge     (ForgerMeshFace * self,
                                                       GFunc            func_,
                                                       gpointer         user_data);

FORGER_AVAILABLE_IN_ALL
guint               forger_mesh_face_get_start_index  (ForgerMeshFace * self);
FORGER_AVAILABLE_IN_ALL
void                forger_mesh_face_set_start_index  (ForgerMeshFace * self,
                                                       const guint      index_);

G_DEFINE_AUTOPTR_CLEANUP_FUNC (ForgerMeshFace, forger_mesh_face_free)

G_END_DECLS
