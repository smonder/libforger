/*
 * forger-raw-mesh.c
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "forger-raw-mesh"

#include "config.h"
#include "forger-debug.h"

#include "forger-raw-mesh.h"
#include "forger-bounding-box.h"
#include "forger-gizmo.h"
#include "renderer/forger-raw-mesh-render-node.h"

/**
 * SECTION: ForgerRawMesh
 * @short_description: A simple 3D solid in that can be rendered in FORGER.
 * @title: ForgerRawMesh.
 *
 * This is the most simple 3D solid type that are going to be
 * rendered in FORGER.
 *
 * Since: 0.1
 */

struct _ForgerRawMesh
{
  ForgerEntity parent_instance;

  /* Vertices data of the entity */
  GArray * positions;
  GArray * normals;
  GArray * uvmaps;
  GArray * index_buffer;
};

G_DEFINE_FINAL_TYPE (ForgerRawMesh, forger_raw_mesh, FORGER_TYPE_ENTITY)

enum {
  PROP_0,
  N_PROPS
};

G_GNUC_UNUSED static GParamSpec *properties [N_PROPS];


/*
 * -----< FORGER_ENTITY CLASS IMPLEMENTATION >----- *
 */
static const gchar *
forger_raw_mesh_real_get_icon_name (ForgerEntity * entity)
{
  return "package-x-generic-symbolic";
}

static void
update_buffers (ForgerRawMesh    *self,
                ForgerRenderNode *node)
{
  forger_render_node_update_positions (node,
                                       self->positions->data,
                                       self->positions->len);
  forger_render_node_update_normals (node,
                                     self->normals->data,
                                     self->normals->len);

  forger_render_node_update_uvmap (node,
                                   self->uvmaps->data,
                                   self->uvmaps->len);

  forger_render_node_update_indices (node,
                                     self->index_buffer->data,
                                     self->index_buffer->len);
}

static void
forger_raw_mesh_real_update_buffers (ForgerEntity * entity)
{
  ForgerRawMesh *self = (ForgerRawMesh *)entity;
  ForgerRenderNode *node;

  g_assert (FORGER_IS_RAW_MESH (self));

  node = FORGER_RENDER_NODE (forger_entity_get (entity, FORGER_COMPONENT_RENDER_NODE));
  update_buffers (self, node);
}

static gboolean
forger_raw_mesh_real_ray_intersects (ForgerEntity   *entity,
                                     graphene_ray_t *ray)
{
  ForgerRawMesh *self = (ForgerRawMesh *)entity;
  ForgerGizmo *gizmo;
  ForgerBoundingBox *bbox;

  g_assert (FORGER_IS_RAW_MESH (self));

  gizmo = FORGER_GIZMO (forger_entity_get (entity, FORGER_COMPONENT_GIZMO));
  bbox = FORGER_BOUNDING_BOX (forger_entity_get (entity, FORGER_COMPONENT_BOUNDING_BOX));

  forger_bounding_box_update_from_buffer (bbox, self->positions);
  return forger_bounding_box_ray_intersects (bbox, forger_gizmo_get_matrix (gizmo), ray);
}


/*
 * -----< G_OBJECT CLASS IMPLEMENTATION >----- *
 */
static void
forger_raw_mesh_finalize (GObject *object)
{
  ForgerRawMesh *self = (ForgerRawMesh *)object;

  g_array_free (self->positions, FALSE);
  g_array_free (self->normals, FALSE);
  g_array_free (self->uvmaps, FALSE);
  g_array_free (self->index_buffer, FALSE);

  G_OBJECT_CLASS (forger_raw_mesh_parent_class)->finalize (object);
}

static void
forger_raw_mesh_get_property (GObject    *object,
                              guint       prop_id,
                              GValue     *value,
                              GParamSpec *pspec)
{
  G_GNUC_UNUSED ForgerRawMesh *self = FORGER_RAW_MESH (object);

  switch (prop_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
forger_raw_mesh_set_property (GObject      *object,
                              guint         prop_id,
                              const GValue *value,
                              GParamSpec   *pspec)
{
  G_GNUC_UNUSED ForgerRawMesh *self = FORGER_RAW_MESH (object);

  switch (prop_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
forger_raw_mesh_class_init (ForgerRawMeshClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  ForgerEntityClass *entity_class = FORGER_ENTITY_CLASS (klass);

  object_class->finalize = forger_raw_mesh_finalize;
  object_class->get_property = forger_raw_mesh_get_property;
  object_class->set_property = forger_raw_mesh_set_property;

  entity_class->get_icon_name = forger_raw_mesh_real_get_icon_name;
  entity_class->update_buffers = forger_raw_mesh_real_update_buffers;
  entity_class->ray_intersects = forger_raw_mesh_real_ray_intersects;
}

static void
forger_raw_mesh_init (ForgerRawMesh *self)
{
  forger_entity_set (FORGER_ENTITY (self),
                     FORGER_COMPONENT_RENDER_NODE,
                     FORGER_ENTITY_COMPONENT (forger_raw_mesh_render_node_new (1000)));

  forger_entity_set (FORGER_ENTITY (self),
                     FORGER_COMPONENT_GIZMO,
                     FORGER_ENTITY_COMPONENT (forger_gizmo_new ()));

  forger_entity_set (FORGER_ENTITY (self),
                     FORGER_COMPONENT_BOUNDING_BOX,
                     forger_bounding_box_new ());

  /* Prepare the buffers */
  self->positions = g_array_new (FALSE, FALSE, sizeof(graphene_point3d_t));
  self->normals = g_array_new (FALSE, FALSE, sizeof(graphene_point3d_t));
  self->uvmaps = g_array_new (FALSE, FALSE, sizeof(graphene_point_t));
  self->index_buffer = g_array_new (FALSE, FALSE, sizeof(guint));
}


/*
 * -----< CONSTRUCTORS >----- *
 */
/**
 * forger_raw_mesh_new:
 * @name: a #gchar: the name identifier of the raw mesh.
 *
 * Create a new #ForgerRawMesh object.
 *
 * Returns: (transfer full): a newly created #ForgerEntity
 */
ForgerEntity *
forger_raw_mesh_new (const gchar *name)
{
  return g_object_new (FORGER_TYPE_RAW_MESH,
                       "name", name, NULL);
}


/*
 * -----< PROPERTIES >-----*
 */
guint
forger_raw_mesh_get_n_vertices (ForgerRawMesh *self)
{
  g_return_val_if_fail (FORGER_IS_RAW_MESH (self), 0);
  return self->positions->len;
}


/*
 * ---< PUSH / PULL / POP API >--- *
 */
void
forger_raw_mesh_push_vertex (ForgerRawMesh *self,
                             ForgerVertex  *vert)
{
  FORGER_ENTRY;
  g_return_if_fail (FORGER_IS_RAW_MESH (self));
  g_return_if_fail (vert);

  g_array_append_val (self->positions, vert->position);
  g_array_append_val (self->normals, vert->normal);
  g_array_append_val (self->uvmaps, vert->uvmap);

  FORGER_EXIT;
}

void
forger_raw_mesh_push_n_vertices (ForgerRawMesh *self,
                                 const guint    n_vertex,
                                 gpointer       vertices)
{
  GArray * verts_array;

  FORGER_ENTRY;
  g_return_if_fail (FORGER_IS_RAW_MESH (self));

  verts_array = g_array_new_take (vertices, n_vertex, FALSE, sizeof(ForgerVertex));
  for (guint i = 0; i < verts_array->len; i++)
    {
      ForgerVertex vert = g_array_index (verts_array, ForgerVertex, i);

      g_array_append_val (self->positions, vert.position);
      g_array_append_val (self->normals, vert.normal);
      g_array_append_val (self->uvmaps, vert.uvmap);
    }

  forger_bounding_box_update_from_buffer (FORGER_BOUNDING_BOX (forger_entity_get (FORGER_ENTITY (self), FORGER_COMPONENT_BOUNDING_BOX)),
                                          self->positions);
  FORGER_EXIT;
}

void
forger_raw_mesh_remove_vertex (ForgerRawMesh *self,
                               const guint    index_)
{
  FORGER_ENTRY;
  g_return_if_fail (FORGER_IS_RAW_MESH (self));

  g_array_remove_index (self->positions, index_);
  g_array_remove_index (self->normals, index_);
  g_array_remove_index (self->uvmaps, index_);

  FORGER_TODO ("Update the corresponding indices in the index buffer according to index_");
  FORGER_EXIT;
}

void
forger_raw_mesh_push_index (ForgerRawMesh *self,
                            const guint    index_)
{

  FORGER_ENTRY;
  g_return_if_fail (FORGER_IS_RAW_MESH (self));
  g_array_append_val (self->index_buffer, index_);
  FORGER_EXIT;
}

void
forger_raw_mesh_push_n_index (ForgerRawMesh *self,
                              const guint    n_indices,
                              guint         *indices)
{
  FORGER_ENTRY;
  g_return_if_fail (FORGER_IS_RAW_MESH (self));
  g_array_append_vals (self->index_buffer, indices, n_indices);
  FORGER_EXIT;
}

void
forger_raw_mesh_remove_index (ForgerRawMesh *self,
                              const guint    loc)
{
  FORGER_ENTRY;
  g_return_if_fail (FORGER_IS_RAW_MESH (self));
  g_array_remove_index (self->index_buffer, loc);
  FORGER_EXIT;
}

