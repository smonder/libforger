/*
 * forger-mesh.h
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#if !defined(FORGER_INSIDE) && !defined(LIBFORGER_COMPILATION)
#error "Only <libforger.h> can be included directly."
#endif

#include "forger-entity.h"
#include "forger-mesh-components.h"

#include "forger-types.h"
#include "forger-version-macros.h"

G_BEGIN_DECLS

#define FORGER_TYPE_MESH (forger_mesh_get_type())

FORGER_AVAILABLE_IN_ALL
G_DECLARE_DERIVABLE_TYPE (ForgerMesh, forger_mesh, FORGER, MESH, ForgerEntity)

struct _ForgerMeshClass
{
  ForgerEntityClass parent_class;
};

FORGER_AVAILABLE_IN_ALL
ForgerEntity * forger_mesh_new                       (void) G_GNUC_WARN_UNUSED_RESULT;

FORGER_AVAILABLE_IN_ALL
void           forger_mesh_append_face               (ForgerMesh *     self,
                                                      ForgerMeshFace * face,
                                                      GArray *         vertices);
FORGER_AVAILABLE_IN_ALL
void           forger_mesh_append_face_series        (ForgerMesh *     self,
                                                      ForgerMeshFace * face,
                                                      GArray *         vertices);

G_END_DECLS
