/* forger-camera.c
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "forger-camera"

#include "config.h"
#include "forger-debug.h"

#include <epoxy/gl.h>
#include <glib/gi18n.h>

#include "forger-camera.h"
#include "forger-gizmo.h"


#define CAMERA_ICON_NAME "camera-web-symbolic"

struct _ForgerCamera
{
  ForgerEntity parent_instance;

  gchar * description;

  /* We should make the camera also controls the viewport. So
   * that we can easily calculate the projection.
   */
  GdkRectangle viewport;
  gfloat fov;
  gfloat near_clip;
  gfloat far_clip;

  /* Since the camera is going to control the projection matrix. */
  graphene_matrix_t projection_matrix;

  /* all the transformations combined into one matrix */
  graphene_matrix_t vp_matrix;

  /* A flag to set when the projection is orthographic.
   * for example when we want to work in a 2D space.
   */
  guint is_orthographic : 1;
};


G_DEFINE_FINAL_TYPE (ForgerCamera, forger_camera, FORGER_TYPE_ENTITY)

enum {
  PROP_0,
  PROP_DESCRIPTION,
  PROP_VIEWPORT,
  PROP_IS_ORTHO,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];


/*
 * -----< FORGER_ENTITY CLASS IMPLEMENTATION >----- *
 */
static const gchar *
forger_camera_entity_real_get_icon_name (ForgerEntity * entity)
{
  return CAMERA_ICON_NAME;
}


/*
 * -----< G_OBJECT CLASS IMPLEMENTATION >----- *
 */
static void
forger_camera_finalize (GObject *object)
{
  ForgerCamera *self = FORGER_CAMERA (object);

  g_clear_pointer (&self->description, g_free);

  G_OBJECT_CLASS (forger_camera_parent_class)->finalize (object);
}

static void
forger_camera_get_property (GObject    *object,
                            guint       prop_id,
                            GValue     *value,
                            GParamSpec *pspec)
{
  ForgerCamera *self = FORGER_CAMERA (object);

  switch (prop_id)
    {
    case PROP_DESCRIPTION:
      g_value_set_string (value, forger_camera_get_description (self));
      break;

    case PROP_VIEWPORT:
      g_value_set_boxed (value, forger_camera_get_viewport (self));
      break;

    case PROP_IS_ORTHO:
      g_value_set_boolean (value, forger_camera_is_orthographic (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
forger_camera_set_property (GObject      *object,
                            guint         prop_id,
                            const GValue *value,
                            GParamSpec   *pspec)
{
  ForgerCamera *self = FORGER_CAMERA (object);

  switch (prop_id)
    {
    case PROP_DESCRIPTION:
      forger_camera_set_description (self, g_value_get_string (value));
      break;

    case PROP_VIEWPORT:
      forger_camera_set_viewport (self, g_value_get_boxed (value));
      break;

    case PROP_IS_ORTHO:
      forger_camera_set_orthographic (self, g_value_get_boolean (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
forger_camera_class_init (ForgerCameraClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  ForgerEntityClass *entity_class = FORGER_ENTITY_CLASS (klass);

  object_class->finalize = forger_camera_finalize;
  object_class->get_property = forger_camera_get_property;
  object_class->set_property = forger_camera_set_property;

  entity_class->get_icon_name = forger_camera_entity_real_get_icon_name;

  properties [PROP_DESCRIPTION] =
    g_param_spec_string ("description",
                         "Description",
                         "A short description about the camera.",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_CONSTRUCT | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  /**
   * ForgerCamera:viewport:
   *
   * The "viewport" property is the viewport area of the canvas
   * where to calculate projection matrix and view transformations.
   *
   * Since: 0.1
   */
  properties [PROP_VIEWPORT] =
    g_param_spec_boxed ("viewport",
                        "Viewport",
                        "The viewport area of the canvas.",
                        GDK_TYPE_RECTANGLE,
                        (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  /**
   * ForgerCamera:is-orthographic:
   *
   * The "is-orthographic" property denotes if the projection type of
   * %self is orthographic or perspective.
   *
   * Since: 0.1
   */
  properties [PROP_IS_ORTHO] =
    g_param_spec_boolean ("is-orthographic",
                          "Is Orthographic",
                          "If the projection is orthographic.",
                          TRUE,
                          (G_PARAM_READWRITE | G_PARAM_CONSTRUCT | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
forger_camera_init (ForgerCamera *self)
{
  self->viewport = (GdkRectangle) { 0, 0, 800, 600 };

  self->fov = 45.0f;
  self->near_clip = 0.1f;
  self->far_clip = 1000.0f;


  forger_entity_set (FORGER_ENTITY (self),
                     FORGER_COMPONENT_GIZMO,
                     FORGER_ENTITY_COMPONENT (forger_gizmo_new ()));
}


/*
 * -----< CONSTRUTORS >----- *
 */
/**
 * forger_camera_new:
 * @name: a #gchar: the name of the camera.
 * @description: a #gchar: a short desc to show in GUI controllers.
 * @fov: a #gfloat: the field of view angle in degrees.
 * @near_clip: a #gfloat: the location of Near Clip Plan.
 * @far_clip: a #gfloat: the location of Far Clip Plan.
 *
 * Creates a new #ForgerCamera.
 *
 * Returns: (transfer full): a newly created #ForgerCamera.
 *
 * Since: 0.1
 */
ForgerCamera *
forger_camera_new (const gchar  *name,
                   const gchar  *description,
                   const gfloat  fov,
                   const gfloat  near_clip,
                   const gfloat  far_clip)
{
  g_autoptr (ForgerCamera) camera = NULL;

  g_return_val_if_fail (name, NULL);

  camera = g_object_new (FORGER_TYPE_CAMERA,
                         "name", name,
                         "description", description,
                         NULL);
  camera->fov = fov;
  camera->near_clip = near_clip;
  camera->far_clip = far_clip;

  return g_steal_pointer (&camera);
}

/**
 * forger_camera_new_default:
 * @name: a #gchar: the name of the camera.
 * @description: a #gchar: a short desc to show in GUI controllers.
 *
 * Creates a new #ForgerCamera using the default values for
 * #ForgerCamera.fov, #ForgerCamera.near_clip, #ForgerCamera.far_clip.
 *
 * Returns: (transfer full): a newly created #ForgerCamera.
 *
 * Since: 0.1
 */
ForgerCamera *
forger_camera_new_default (const gchar *name,
                           const gchar *description)
{
  return g_object_new (FORGER_TYPE_CAMERA,
                       "name", name,
                       "description", description,
                       NULL);
}


/*
 * -----< CAMERA PROPERTIES >----- *
 */
/**
 * forger_camera_get_decription:
 * @self: a #ForgerCamera.
 *
 * Gets the description property of @self.
 *
 * Returns: (transfer full): a #gchar.
 *
 * Since: 0.1
 */
gchar *
forger_camera_get_description (ForgerCamera *self)
{
  g_return_val_if_fail (FORGER_IS_CAMERA (self), NULL);

  return self->description;
}

/**
 * forger_camera_set_description:
 * @self: a #ForgerCamera.
 * @desc: a #gchar.
 *
 * Sets the name property of @self.
 *
 * Since: 0.1
 */
void
forger_camera_set_description (ForgerCamera  *self,
                             const gchar *desc)
{
  g_return_if_fail (FORGER_IS_CAMERA (self));

  if (g_strcmp0 (self->description, desc) != 0)
    {
      g_free (self->description);
      self->description = g_strdup (desc);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_DESCRIPTION]);
    }
}

/**
 * forger_camera_get_vpmatrix:
 * @self: a #ForgerCamera.
 *
 * Gets the view_projection matrix of @self.
 *
 * Returns: (transfer full): a #graphene_matrix_t.
 *
 * Since: 0.1
 */
graphene_matrix_t *
forger_camera_get_vpmatrix (ForgerCamera *self)
{
  ForgerGizmo *gizmo;
  g_return_val_if_fail (FORGER_IS_CAMERA (self), NULL);

  gizmo = FORGER_GIZMO (forger_entity_get (FORGER_ENTITY (self),
                                           FORGER_COMPONENT_GIZMO));

  /* first we should calculate the vpmatrix */
  graphene_matrix_multiply (&self->projection_matrix,
                            forger_gizmo_get_matrix (gizmo),
                            &self->vp_matrix);

  return &self->vp_matrix;
}

graphene_matrix_t *
forger_camera_get_vmatrix (ForgerCamera *self)
{
  ForgerGizmo *gizmo;
  g_return_val_if_fail (FORGER_IS_CAMERA (self), NULL);

  gizmo = FORGER_GIZMO (forger_entity_get (FORGER_ENTITY (self),
                                           FORGER_COMPONENT_GIZMO));
  return forger_gizmo_get_matrix (gizmo);
}

graphene_matrix_t *
forger_camera_get_pmatrix (ForgerCamera *self)
{
  g_return_val_if_fail (FORGER_IS_CAMERA (self), NULL);

  return &self->projection_matrix;
}

GdkRectangle *
forger_camera_get_viewport (ForgerCamera *self)
{
  g_return_val_if_fail (FORGER_IS_CAMERA (self), NULL);

  return &self->viewport;
}

void
forger_camera_set_viewport (ForgerCamera   *self,
                          GdkRectangle *viewport)
{
  g_return_if_fail (FORGER_IS_CAMERA (self));
  g_return_if_fail (viewport);

  /* first we set the viewport */
  self->viewport.x = viewport->x;
  self->viewport.y = viewport->y;
  self->viewport.width = viewport->width;
  self->viewport.height = viewport->height;

  /* the we calculate projection */
  if (self->is_orthographic)
    graphene_matrix_init_ortho (&self->projection_matrix,
                                - (gfloat)self->viewport.width / 2, (gfloat)self->viewport.width / 2,
                                - (gfloat)self->viewport.height / 2, (gfloat)self->viewport.height / 2,
                                self->near_clip, self->far_clip);
  else
    graphene_matrix_init_perspective (&self->projection_matrix,
                                      self->fov,
                                      (gfloat)self->viewport.width / self->viewport.height,
                                      self->near_clip, self->far_clip);

  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_VIEWPORT]);
}

gboolean
forger_camera_is_orthographic (ForgerCamera *self)
{
  g_return_val_if_fail (FORGER_IS_CAMERA (self), FALSE);

  return self->is_orthographic;
}

void
forger_camera_set_orthographic (ForgerCamera *self,
                              gboolean    is_ortho)
{
  g_return_if_fail (FORGER_IS_CAMERA (self));

  is_ortho = !!is_ortho;

  if (self->is_orthographic != is_ortho)
    {
      self->is_orthographic = is_ortho;
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_IS_ORTHO]);
    }
}


/*
 * -----< METHODS >----- *
 */
/**
 * forger_camera_handle_update:
 * @self: a #ForgerCamera
 *
 * we call the get function to update the view matrix each frame so that our
 * mouse picker is going to be synchronized with the changes.
 *
 */
void
forger_camera_handle_update (ForgerCamera *self,
                             guint         time_step)
{
  ForgerGizmo *gizmo;

  g_assert (FORGER_IS_CAMERA (self));

  gizmo = FORGER_GIZMO (forger_entity_get (FORGER_ENTITY (self),
                                           FORGER_COMPONENT_GIZMO));

  forger_gizmo_get_matrix (gizmo);
}

static gfloat
get_zoom_speed (gfloat scale)
{
	gfloat distance;
  gfloat speed;

  distance = scale * 0.2f;
  distance = MAX (distance, 0.0f);
	speed = distance * distance;

  speed = MIN (speed, 100.0f);

	return speed;
}

/**
 * forger_camera_handle_zoom_event:
 * @self: a #ForgerCamera
 * @scroll: a #GtkEventControllerScroll
 *
 * We call this method to handle the zoom event in the canvas.
 *
 * Returns: %TRUE if the zoom event is handled. %FALSE otherwise.
 */
gboolean
forger_camera_handle_zoom_event (ForgerCamera             *self,
                                 GtkEventControllerScroll *scroll,
                                 const gdouble             offset_x,
                                 const gdouble             offset_y)
{
  ForgerGizmo *gizmo;
  graphene_point3d_t *scale;

  g_return_val_if_fail (FORGER_IS_CAMERA (self), FALSE);
  g_return_val_if_fail (GTK_IS_EVENT_CONTROLLER_SCROLL (scroll), FALSE);

  gizmo = FORGER_GIZMO (forger_entity_get (FORGER_ENTITY (self),
                                           FORGER_COMPONENT_GIZMO));
  scale = forger_gizmo_get_scale (gizmo);

  forger_gizmo_scale_uni (gizmo, - offset_y * get_zoom_speed (scale->x));
	if (scale->x < 1.0f)
    forger_gizmo_set_scale_f (gizmo, 1.0f);

  return TRUE;
}

static gfloat
get_pan_speed (gint viewport_dimension)
{
  gfloat dim;

  dim = MIN (viewport_dimension / 1000.0f, 2.4f);

  return 0.0366f * (dim * dim) - 0.1778f * dim + 0.3021f;
}

/**
 * forger_camera_handle_pan_event:
 * @self: a #ForgerCamera
 * @drag_gesture: a #GtkGestureDrag
 *
 * We call this method to handle the camera pan event of the canvas
 *
 */
void
forger_camera_handle_pan_event (ForgerCamera   *self,
                                GtkGestureDrag *drag_gesture,
                                const gdouble   offset_x,
                                const gdouble   offset_y)
{
  ForgerGizmo *gizmo;

  g_return_if_fail (FORGER_IS_CAMERA (self));
  g_return_if_fail (GTK_IS_GESTURE_DRAG (drag_gesture));

  gizmo = FORGER_GIZMO (forger_entity_get (FORGER_ENTITY (self),
                                           FORGER_COMPONENT_GIZMO));
  forger_gizmo_translate (gizmo, FORGER_AXIS_X,
                          offset_x * 0.001f * get_pan_speed (self->viewport.width));
  forger_gizmo_translate (gizmo, FORGER_AXIS_Y,
                          - offset_y * 0.001f * get_pan_speed (self->viewport.height));
}
