/*
 * forger-vertex.h
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#if !defined(FORGER_INSIDE) && !defined(LIBFORGER_COMPILATION)
#error "Only <libforger.h> can be included directly."
#endif

#include <glib-object.h>
#include <graphene.h>

#include "forger-version-macros.h"

G_BEGIN_DECLS

#define FORGER_TYPE_VERTEX (forger_vertex_get_type ())

/**
 * FORGER_VERTEX:
 * px, py, pz: are the 3 position components.
 * nx, ny, nz: are the 3 normal components.
 * mx, my: are the 2 UVMap components.
 *
 * a helper macro to create a vertex on the stack.
 */
#define FORGER_VERTEX(px,py,pz,nx,ny,nz,mx,my) \
    { .position = { .x = (px), .y = (py), .z = (pz) }, \
      .normal =  { .x = (nx), .y = (ny), .z = (nz) }, \
      .uvmap = { .x = (mx), .y= (my) } }


typedef struct _ForgerVertex ForgerVertex;

struct _ForgerVertex
{
  graphene_point3d_t position;
  graphene_point3d_t normal;
  graphene_point_t   uvmap;
};

FORGER_AVAILABLE_IN_ALL
GType             forger_vertex_get_type (void) G_GNUC_CONST;
FORGER_AVAILABLE_IN_ALL
ForgerVertex *    forger_vertex_new      (void);
FORGER_AVAILABLE_IN_ALL
ForgerVertex *    forger_vertex_copy     (ForgerVertex * self);
FORGER_AVAILABLE_IN_ALL
void              forger_vertex_free     (ForgerVertex * self);

G_DEFINE_AUTOPTR_CLEANUP_FUNC (ForgerVertex, forger_vertex_free)


G_END_DECLS
