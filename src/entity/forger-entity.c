/*
 * forger-entity.c
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "forger-entity"

#include "config.h"
#include "forger-debug.h"

#include "forger-entity-private.h"


/**
 * SECTION: ForgerEntity
 * @short_description: The base type that is going to be dealt with in Front-end FORGER.
 * @title: ForgerEntity.
 *
 * This is the base type that all the other classes that are going to be
 * used in FORGER should inherit from.
 *
 * Since: 0.1
 */


typedef struct
{
  /* -----< PRIVATE API >----- */
  /* Here is the private API of the entity.
   * Note that Every item here SHOULD NOT be associated with a GPorperty.
   */

  /* The parenting if the entity is contained in another entity */
  ForgerEntity * parent;
  GList link;

  /* The head of the render nodes inside this entity */
  ForgerEntityComponent * component [N_ENTITY_COMPONENT_TYPES];

  /* -----< PROPERTIES >----- */
  gchar * name;
  graphene_point3d_t origin_point;

  /* GUI flags */
  guint sensitive : 1;
  guint visible : 1;
} ForgerEntityPrivate;

G_DEFINE_ABSTRACT_TYPE_WITH_PRIVATE (ForgerEntity, forger_entity, G_TYPE_OBJECT)

enum {
  PROP_0,
  PROP_NAME,
  PROP_PARENT,
  PROP_ORIGIN,
  PROP_SENSITIVE,
  PROP_VISIBLE,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];


/*
 * -----< G_OBJECT CLASS IMPLEMENTATION >----- *
 */
static void
forger_entity_finalize (GObject *object)
{
  ForgerEntity *self = (ForgerEntity *)object;
  ForgerEntityPrivate *priv = forger_entity_get_instance_private (self);

  for (guint i = 0; i < N_ENTITY_COMPONENT_TYPES; i++)
    if (priv->component[i])
      g_clear_object (&priv->component[i]);

  G_OBJECT_CLASS (forger_entity_parent_class)->finalize (object);
}

static void
forger_entity_get_property (GObject    *object,
                            guint       prop_id,
                            GValue     *value,
                            GParamSpec *pspec)
{
  ForgerEntity *self = FORGER_ENTITY (object);

  switch (prop_id)
    {
    case PROP_NAME:
      g_value_take_string (value, forger_entity_dup_name (self));
      break;

    case PROP_PARENT:
      g_value_set_object (value, forger_entity_get_parent (self));
      break;

    case PROP_ORIGIN:
      g_value_set_boxed (value, forger_entity_get_origin (self));
      break;

    case PROP_SENSITIVE:
      g_value_set_boolean (value, forger_entity_is_sensitive (self));
      break;

    case PROP_VISIBLE:
      g_value_set_boolean (value, forger_entity_is_visible (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
forger_entity_set_property (GObject      *object,
                            guint         prop_id,
                            const GValue *value,
                            GParamSpec   *pspec)
{
  ForgerEntity *self = FORGER_ENTITY (object);

  switch (prop_id)
    {
    case PROP_NAME:
      forger_entity_set_name (self, g_value_get_string (value));
      break;

    case PROP_ORIGIN:
      forger_entity_set_origin (self, g_value_get_boxed (value));
      break;

    case PROP_SENSITIVE:
      forger_entity_set_sensitive (self, g_value_get_boolean (value));
      break;

    case PROP_VISIBLE:
      forger_entity_set_visible (self, g_value_get_boolean (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
forger_entity_class_init (ForgerEntityClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = forger_entity_finalize;
  object_class->get_property = forger_entity_get_property;
  object_class->set_property = forger_entity_set_property;

  /**
   * ForgerEntity:name:
   *
   * The "name" property is the given name of the entity
   * that is going to be shown on the Entities Page.
   *
   * Since: 0.1
   */
  properties [PROP_NAME] =
    g_param_spec_string ("name",
                         "Name",
                         "The name of the entity.",
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_CONSTRUCT | G_PARAM_STATIC_STRINGS));

  /**
   * ForgerEntity:parent:
   *
   * The "parent" property is the #ForgerEntity that %self is a child
   * of in a larger #ForgerScene.
   *
   * This property is set by parent itself. thus, here we can only read it.
   *
   */
  properties [PROP_PARENT] =
    g_param_spec_object ("parent", NULL, NULL,
                         FORGER_TYPE_ENTITY,
                         (G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));

  /**
   * ForgerEntity:origin:
   *
   * The "origin" property is the center point of the entity that the
   * transformation will revolve around.
   */
  properties [PROP_ORIGIN] =
    g_param_spec_boxed ("origin", NULL, NULL,
                        GRAPHENE_TYPE_POINT3D,
                        (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  /**
   * ForgerEntity:sensitive:
   *
   * The "sensitive" property controls if %self is immune to changes
   * and getting selected.
   *
   * Since: 0.1
   */
  properties [PROP_SENSITIVE] =
    g_param_spec_boolean ("sensitive",
                          "Sensitive",
                          "If the entity is sensitive to modification.",
                          TRUE,
                          (G_PARAM_READWRITE | G_PARAM_CONSTRUCT | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  /**
   * ForgerEntity:visible:
   *
   * The "visible" property controls if %self is visible on screen.
   *
   * Since: 0.1
   */
  properties [PROP_VISIBLE] =
    g_param_spec_boolean ("visible",
                          "Visible",
                          "If the entity is visible on the screen.",
                          TRUE,
                          (G_PARAM_READWRITE | G_PARAM_CONSTRUCT | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
forger_entity_init (ForgerEntity *self)
{
  ForgerEntityPrivate *priv = forger_entity_get_instance_private (self);

  priv->link.data = self;
  priv->parent = NULL;
  priv->origin_point = GRAPHENE_POINT3D_INIT_ZERO;
}


/*
 * -----< VIRTUAL METHODS >----- *
 */
/**
 * forger_entity_get_icon_name:
 *
 * Gets the name of the icon that is going to be displayed in GUI widgets
 * near the name of the entity.
 * This is so helpful when we need to represent the entity as a GUI widget.
 *
 * Returns: (transfer full): A static string represents the name of the icon.
 */
const gchar *
forger_entity_get_icon_name (ForgerEntity *self)
{
  if (FORGER_ENTITY_GET_CLASS (self)->get_icon_name != NULL)
    return FORGER_ENTITY_GET_CLASS (self)->get_icon_name (self);

  return NULL;
}

/**
 * forger_entity_update_buffers:
 *
 * This method responsible for preparing the entity to be rendered in the canvas.
 *
 */
void
forger_entity_update_buffers (ForgerEntity * self)
{
  if (FORGER_ENTITY_GET_CLASS (self)->update_buffers != NULL)
    FORGER_ENTITY_GET_CLASS (self)->update_buffers (self);
}

gboolean
forger_entity_ray_intersects (ForgerEntity   *self,
                              graphene_ray_t *ray)
{
  g_return_val_if_fail (FORGER_IS_ENTITY (self), FALSE);

  if (FORGER_ENTITY_GET_CLASS (self)->ray_intersects != NULL)
    return FORGER_ENTITY_GET_CLASS (self)->ray_intersects (self, ray);

  return FALSE;
}

/**
 * forger_entity_get_closest_point:
 * @self: a #ForgerEntity.
 * @ray_hit: a #graphene_point3d_t: the hit position of the ray.
 * @ray_sensitivity: a #float: the radius of the circle around the hit position of the ray.
 *
 * Searches for the closest point to where the ray hit.
 * regarding sensitivity.
 *
 * Returns: (transfer full): the index of the closest point or -1 if there is none.
 */
gint
forger_entity_get_closeset_point (ForgerEntity       *self,
                                  graphene_point3d_t *ray_hit,
                                  const float         ray_sensitivity)
{
  g_return_val_if_fail (FORGER_IS_ENTITY (self), -1);

  if (FORGER_ENTITY_GET_CLASS (self)->get_closeset_point != NULL)
    return FORGER_ENTITY_GET_CLASS (self)->get_closeset_point (self, ray_hit, ray_sensitivity);

  return -1;
}


/*
 * -----< PROPERTIES >----- *
 */
gchar *
forger_entity_dup_name (ForgerEntity *self)
{
  ForgerEntityPrivate *priv = forger_entity_get_instance_private (self);
  g_return_val_if_fail (FORGER_IS_ENTITY (self), NULL);
  return g_strdup (priv->name);
}

void
forger_entity_set_name (ForgerEntity *self,
                        const gchar  *name)
{
  ForgerEntityPrivate *priv = forger_entity_get_instance_private (self);
  const gchar *new_name = ((name == NULL)? "dummy" : name);

  g_return_if_fail (FORGER_IS_ENTITY (self));

  if (g_set_str (&priv->name, new_name))
    g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_NAME]);
}

/**
 * forger_entity_get_parent:
 *
 * Get the value of the "parent" property.
 *
 * Returns: (transfer full) (nullable): a #ForgerEntity or %NULL if @self
 * is the root.
 */
ForgerEntity *
forger_entity_get_parent (ForgerEntity *self)
{
  ForgerEntityPrivate *priv = forger_entity_get_instance_private (self);
  g_return_val_if_fail (FORGER_IS_ENTITY (self), NULL);
  return priv->parent;
}

/**
 * forger_entity_get_origin:
 * @self: a #ForgerEntity
 *
 * Gets the "origin" property value.
 *
 * Returns: (transfer full): a #graphene_point3d_t pointer
 */
graphene_point3d_t *
forger_entity_get_origin (ForgerEntity *self)
{
  ForgerEntityPrivate *priv = forger_entity_get_instance_private (self);
  g_return_val_if_fail (FORGER_IS_ENTITY (self), NULL);
  return &priv->origin_point;
}

/**
 * forger_entity_set_origin:
 * @self: a #ForgerEntity
 * @setting: a #graphene_poiny3d_t
 *
 * Sets the "origin" property value.
 *
 */
void
forger_entity_set_origin (ForgerEntity       *self,
                          graphene_point3d_t *setting)
{
  ForgerEntityPrivate *priv = forger_entity_get_instance_private (self);

  g_return_if_fail (FORGER_IS_ENTITY (self));
  g_return_if_fail (setting);

  graphene_point3d_init_from_point (&priv->origin_point, setting);
  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_ORIGIN]);
}

gboolean
forger_entity_is_sensitive (ForgerEntity *self)
{
  ForgerEntityPrivate *priv = forger_entity_get_instance_private (self);
  g_return_val_if_fail (FORGER_IS_ENTITY (self), FALSE);
  return priv->sensitive;
}

void
forger_entity_set_sensitive (ForgerEntity *self,
                             gboolean      sensitive)
{
  ForgerEntityPrivate *priv = forger_entity_get_instance_private (self);

  g_return_if_fail (FORGER_IS_ENTITY (self));

  sensitive = !!sensitive;

  if (priv->sensitive != sensitive)
    {
      priv->sensitive = sensitive;
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_SENSITIVE]);
    }
}

gboolean
forger_entity_is_visible (ForgerEntity *self)
{
  ForgerEntityPrivate *priv = forger_entity_get_instance_private (self);
  g_return_val_if_fail (FORGER_IS_ENTITY (self), FALSE);
  return priv->visible;
}

void
forger_entity_set_visible (ForgerEntity *self,
                           gboolean      visible)
{
  ForgerEntityPrivate *priv = forger_entity_get_instance_private (self);

  g_return_if_fail (FORGER_IS_ENTITY (self));

  visible = !!visible;

  if (priv->visible != visible)
    {
      priv->visible = visible;
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_VISIBLE]);
    }
}


/*
 * -----< PRIVATE API >----- *
 */
GList *
_forger_entity_get_link (ForgerEntity *self)
{
  ForgerEntityPrivate *priv = forger_entity_get_instance_private (self);
  g_return_val_if_fail (FORGER_IS_ENTITY (self), NULL);
  return &priv->link;
}

void
_forger_entity_set_parent (ForgerEntity *self,
                           ForgerEntity *parent)
{
  ForgerEntityPrivate *priv = forger_entity_get_instance_private (self);

  g_return_if_fail (FORGER_IS_ENTITY (self));
  g_return_if_fail (!parent || FORGER_IS_ENTITY (parent));

  if (priv->parent == parent)
    return;

  if (g_set_object (&priv->parent, parent))
    g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_PARENT]);
}


/*
 * -----< COMPONENT API >----- *
 */
/**
 * forger_entity_get:
 * @self: a #ForgerEntity
 * @type_: a #ForgerComponentType
 *
 * Get the component of @self whose type denoted by @type_
 *
 * Returns: (transfer full) (nullable): a #ForgerEntityComponent or %NULL
 */
ForgerEntityComponent *
forger_entity_get (ForgerEntity        *self,
                   ForgerComponentType  type_)
{
  ForgerEntityPrivate *priv = forger_entity_get_instance_private (self);

  g_return_val_if_fail (FORGER_IS_ENTITY (self), NULL);
  g_return_val_if_fail (FORGER_IS_COMPONENT_TYPE (type_), NULL);

  return priv->component[type_];
}

/**
 * forger_entity_set:
 * @self: a #ForgerEntity
 * @type_: a #ForgerComponentType
 * @component: a #ForgerEntityComponent
 *
 * Set the component of @self whose type denoted by @type_
 *
 */
void
forger_entity_set (ForgerEntity          *self,
                   ForgerComponentType    type_,
                   ForgerEntityComponent *component)
{
  ForgerEntityPrivate *priv = forger_entity_get_instance_private (self);

  g_return_if_fail (FORGER_IS_ENTITY (self));
  g_return_if_fail (FORGER_IS_COMPONENT_TYPE (type_));
  g_return_if_fail (FORGER_IS_ENTITY_COMPONENT (component));

  g_set_object (&priv->component[type_], component);
}

