/* forger-entity-component.h
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#pragma once


#if !defined(FORGER_INSIDE) && !defined(LIBFORGER_COMPILATION)
#error "Only <libforger.h> can be included directly."
#endif

#include <gio/gio.h>
#include <graphene.h>

#include "forger-version-macros.h"

G_BEGIN_DECLS

#define FORGER_TYPE_ENTITY_COMPONENT (forger_entity_component_get_type())

FORGER_AVAILABLE_IN_ALL
G_DECLARE_DERIVABLE_TYPE (ForgerEntityComponent, forger_entity_component, FORGER, ENTITY_COMPONENT, GObject)

struct _ForgerEntityComponentClass
{
  GObjectClass parent_class;

  const gchar * (* get_display_name) (ForgerEntityComponent * component);
  const gchar * (* get_icon_name)    (ForgerEntityComponent * component);
  const gchar * (* get_description)  (ForgerEntityComponent * component);

  /* The prepare function is called before the draw call to prepare
   * the component, thus the entity from the component side, to be rendered.
   */
  void          (* prepare)          (ForgerEntityComponent * component,
                                      graphene_matrix_t *     model_matrix,
                                      graphene_matrix_t *     view_projection_matrix);

  /* The draw call for each buffer in the entity: this is handled by
   * children according to the type of the entity.
   */
  void          (* draw)             (ForgerEntityComponent * component);
};

FORGER_AVAILABLE_IN_ALL
const gchar * forger_entity_component_get_display_name (ForgerEntityComponent * self);
FORGER_AVAILABLE_IN_ALL
const gchar * forger_entity_component_get_icon_name    (ForgerEntityComponent * self);
FORGER_AVAILABLE_IN_ALL
const gchar * forger_entity_component_get_description  (ForgerEntityComponent * self);

FORGER_AVAILABLE_IN_ALL
void          forger_entity_component_prepare          (ForgerEntityComponent * self,
                                                        graphene_matrix_t *     model_matrix,
                                                        graphene_matrix_t *     view_projection_matrix);
FORGER_AVAILABLE_IN_ALL
void          forger_entity_component_draw             (ForgerEntityComponent * self);

G_END_DECLS

