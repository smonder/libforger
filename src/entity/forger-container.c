/*
 * forger-container.c
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "forger-container"

#include "config.h"
#include "forger-debug.h"

#include "forger-container.h"
#include "forger-entity-private.h"
#include "forger-utils-private.h"

typedef struct
{
  GQueue children;
  gchar * expanded_icon_name;
} ForgerContainerPrivate;


static void list_model_iface_init (GListModelInterface *iface);

G_DEFINE_TYPE_WITH_CODE (ForgerContainer, forger_container, FORGER_TYPE_ENTITY,
                         G_ADD_PRIVATE (ForgerContainer)
                         G_IMPLEMENT_INTERFACE (G_TYPE_LIST_MODEL, list_model_iface_init))


enum {
  PROP_0,
  PROP_EXPANDED_ICON_NAME,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];


/*
 * -----< G_LIST_MODEL INTERFACE IMPLEMENTATION >----- *
 */
static guint
list_model_get_n_items (GListModel *model)
{
  ForgerContainer *self = (ForgerContainer *)model;
  ForgerContainerPrivate *priv = forger_container_get_instance_private (self);
  g_assert (FORGER_IS_CONTAINER (self));
  return priv->children.length;
}

static GType
list_model_get_item_type (GListModel *model)
{
  return FORGER_TYPE_ENTITY;
}

static gpointer
list_model_get_item (GListModel *model,
                     guint       position)
{
  ForgerContainer *self = (ForgerContainer *)model;
  ForgerContainerPrivate *priv = forger_container_get_instance_private (self);

  g_assert (FORGER_IS_CONTAINER (self));

  if (position >= priv->children.length)
    return NULL;

  if (position == 0)
    return g_object_ref (g_queue_peek_head (&priv->children));

  if (position == priv->children.length-1)
    return g_object_ref (g_queue_peek_tail (&priv->children));

  return g_object_ref (g_queue_peek_nth (&priv->children, position));
}

static void
list_model_iface_init (GListModelInterface *iface)
{
  iface->get_n_items = list_model_get_n_items;
  iface->get_item_type = list_model_get_item_type;
  iface->get_item = list_model_get_item;
}


/*
 * -----< FORGER_ENTITY CLASS IMPLEMENTATION >----- *
 */
static const gchar *
forger_container_real_get_icon_name (ForgerEntity * entity)
{
  return "folder-symbolic";
}


/*
 * -----< G_OBJECT CLASS IMPLEMENTATION >----- *
 */
static void
forger_container_finalize (GObject *object)
{
  ForgerContainer *self = (ForgerContainer *)object;
  ForgerContainerPrivate *priv = forger_container_get_instance_private (self);

  g_queue_free (&priv->children);

  G_OBJECT_CLASS (forger_container_parent_class)->finalize (object);
}

static void
forger_container_get_property (GObject    *object,
                               guint       prop_id,
                               GValue     *value,
                               GParamSpec *pspec)
{
  ForgerContainer *self = FORGER_CONTAINER (object);

  switch (prop_id)
    {
    case PROP_EXPANDED_ICON_NAME:
      g_value_set_string (value, forger_container_get_expanded_icon_name (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
forger_container_set_property (GObject      *object,
                               guint         prop_id,
                               const GValue *value,
                               GParamSpec   *pspec)
{
  ForgerContainer *self = FORGER_CONTAINER (object);

  switch (prop_id)
    {
    case PROP_EXPANDED_ICON_NAME:
      forger_container_set_expanded_icon_name (self, g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
forger_container_class_init (ForgerContainerClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  ForgerEntityClass *entity_class = FORGER_ENTITY_CLASS (klass);

  object_class->finalize = forger_container_finalize;
  object_class->get_property = forger_container_get_property;
  object_class->set_property = forger_container_set_property;

  entity_class->get_icon_name = forger_container_real_get_icon_name;

  properties [PROP_EXPANDED_ICON_NAME] =
    g_param_spec_string ("expanded-icon-name", NULL, NULL,
                         NULL,
                         (G_PARAM_READWRITE | G_PARAM_CONSTRUCT |
                          G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
forger_container_init (ForgerContainer *self)
{
}


/*
 * ---< CONSTRUCTORS >--- *
 */
/**
 * forger_container_new:
 *
 * Creates a new #ForgerContainer object.
 *
 * Returns: (transfer full): a newly created #ForgerEntity.
 */
ForgerEntity *
forger_container_new (const gchar * name)
{
  return g_object_new (FORGER_TYPE_CONTAINER,
                       "name", name,
                       NULL);
}


/*
 * -----< PROPERTIES >----- *
 */
const gchar *
forger_container_get_expanded_icon_name (ForgerContainer *self)
{
  ForgerContainerPrivate *priv = forger_container_get_instance_private (self);
  g_return_val_if_fail (FORGER_IS_CONTAINER (self), NULL);
  return priv->expanded_icon_name;
}

void
forger_container_set_expanded_icon_name (ForgerContainer *self,
                                         const gchar     *setting)
{
  ForgerContainerPrivate *priv = forger_container_get_instance_private (self);
  const gchar * icon_name = setting;

  g_return_if_fail (FORGER_IS_CONTAINER (self));

  if (!icon_name)
    icon_name = "folder-open-symbolic";

  if (g_set_str (&priv->expanded_icon_name, icon_name))
    g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_EXPANDED_ICON_NAME]);
}


/*
 * -----< METHODS >----- *
 */
void
forger_container_append_child (ForgerContainer *self,
                               ForgerEntity    *child)
{
  ForgerContainerPrivate *priv = forger_container_get_instance_private (self);
  guint child_position;

  g_return_if_fail (FORGER_IS_CONTAINER (self));
  g_return_if_fail (FORGER_IS_ENTITY (child));

  g_queue_push_tail_link (&priv->children,
                          _forger_entity_get_link (child));
  child_position = g_queue_link_index (&priv->children,
                                       _forger_entity_get_link (child));
  g_return_if_fail (child_position > -1);
  _forger_entity_set_parent (child, FORGER_ENTITY (self));

  g_list_model_items_changed (G_LIST_MODEL (self), child_position, 0, 1);
}

void
forger_container_prepend_child (ForgerContainer *self,
                                ForgerEntity    *child)
{
  ForgerContainerPrivate *priv = forger_container_get_instance_private (self);
  guint child_position;

  g_return_if_fail (FORGER_IS_CONTAINER (self));
  g_return_if_fail (FORGER_IS_ENTITY (child));

  g_queue_push_head_link (&priv->children,
                          _forger_entity_get_link (child));
  child_position = g_queue_link_index (&priv->children,
                                       _forger_entity_get_link (child));
  g_return_if_fail (child_position > -1);
  _forger_entity_set_parent (child, FORGER_ENTITY (self));

  g_list_model_items_changed (G_LIST_MODEL (self), child_position, 0, 1);
}

void
forger_container_insert_child (ForgerContainer *self,
                               const gint       index_,
                               ForgerEntity    *child)
{
  ForgerContainerPrivate *priv = forger_container_get_instance_private (self);
  guint child_position;

  g_return_if_fail (FORGER_IS_CONTAINER (self));
  g_return_if_fail (FORGER_IS_ENTITY (child));

  g_queue_push_nth_link (&priv->children, index_,
                         _forger_entity_get_link (child));
  child_position = g_queue_link_index (&priv->children,
                                       _forger_entity_get_link (child));
  g_return_if_fail (child_position > -1);
  _forger_entity_set_parent (child, FORGER_ENTITY (self));

  g_list_model_items_changed (G_LIST_MODEL (self), child_position, 0, 1);
}

void
forger_container_remove_child (ForgerContainer *self,
                               ForgerEntity    *child)
{
  ForgerContainerPrivate *priv = forger_container_get_instance_private (self);
  guint child_position;

  g_return_if_fail (FORGER_IS_CONTAINER (self));
  g_return_if_fail (FORGER_IS_ENTITY (child));
  g_return_if_fail (forger_container_contains (self, child));

  child_position = g_queue_link_index (&priv->children,
                                       _forger_entity_get_link (child));
  g_return_if_fail (child_position > -1);
  g_queue_unlink (&priv->children, _forger_entity_get_link (child));
  _forger_entity_set_parent (child, NULL);

  g_list_model_items_changed (G_LIST_MODEL (self), child_position, 1, 0);
}

/**
 * forger_container_peek_nth_child:
 * @self: a #ForgerContainer
 * @index_: a #guint: the index of the entity
 *
 * Get the value of the child entity that is denoted by %index_
 *
 * Returns: (transfer full) (nullable): a #ForgerEntity or %NULL.
 */
ForgerEntity *
forger_container_peek_nth_child (ForgerContainer *self,
                                 const guint      index_)
{
  ForgerContainerPrivate *priv = forger_container_get_instance_private (self);
  g_return_val_if_fail (FORGER_IS_CONTAINER (self), FALSE);
  return FORGER_ENTITY (g_queue_peek_nth (&priv->children, index_));
}

gboolean
forger_container_contains (ForgerContainer *self,
                           ForgerEntity    *child)
{
  ForgerContainerPrivate *priv = forger_container_get_instance_private (self);

  g_return_val_if_fail (FORGER_IS_CONTAINER (self), FALSE);
  g_return_val_if_fail (FORGER_IS_ENTITY (child), FALSE);

  return (g_queue_find (&priv->children, child)) != NULL;
}

void
forger_container_move_child (ForgerContainer *self,
                             ForgerEntity    *child,
                             ForgerContainer *dest)
{
  ForgerContainerPrivate *priv = forger_container_get_instance_private (self);

  FORGER_ENTRY;

  g_return_if_fail (FORGER_IS_CONTAINER (self));
  g_return_if_fail (FORGER_IS_ENTITY (child));
  g_return_if_fail (FORGER_IS_CONTAINER (dest));

  if (!forger_container_contains (self, child))
    FORGER_EXIT;

  g_queue_unlink (&priv->children, _forger_entity_get_link (child));
  forger_container_append_child (dest, child);

  FORGER_EXIT;
}

void
forger_container_copy_child (ForgerContainer *self,
                             ForgerEntity    *child,
                             ForgerContainer *dest)
{
  ForgerEntity *new_entity;

  FORGER_ENTRY;

  g_return_if_fail (FORGER_IS_CONTAINER (self));
  g_return_if_fail (FORGER_IS_ENTITY (child));
  g_return_if_fail (FORGER_IS_CONTAINER (dest));

  if (!forger_container_contains (self, child))
    FORGER_EXIT;

  new_entity = FORGER_ENTITY (_forger_g_object_clone (G_OBJECT (child)));
  forger_container_append_child (dest, new_entity);

  FORGER_EXIT;
}


/*
 * -----< FORGER CONTAINER TRAVERSE >----- *
 */
typedef struct
{
  GTraverseType           type;
  GTraverseFlags          flags;
  gint                    depth;
  ForgerSceneTraverseFunc callback;
  gpointer                user_data;
} ForgerTreeTraversal;

static inline gboolean
can_callback_node (ForgerEntity   *entity,
                   GTraverseFlags  flags)
{
  if (FORGER_IS_CONTAINER (entity))
    {
      ForgerContainerPrivate *priv = forger_container_get_instance_private (FORGER_CONTAINER (entity));
      return ((flags & G_TRAVERSE_LEAVES) && priv->children.length == 0) ||
             ((flags & G_TRAVERSE_NON_LEAVES) && priv->children.length > 0);
    }

  return (flags & G_TRAVERSE_LEAVES);
}

static gboolean
do_traversal (ForgerEntity        *entity,
              ForgerTreeTraversal *traversal)
{
  const GList *iter;
  ForgerEntityVisit ret = FORGER_ENTITY_VISIT_BREAK;

  if (traversal->depth < 0)
    return FORGER_ENTITY_VISIT_CONTINUE;

  traversal->depth--;

  if (traversal->type == G_PRE_ORDER &&
      can_callback_node (entity, traversal->flags))
    {
      ret = traversal->callback (entity, traversal->user_data);

      if (forger_entity_get_parent (entity) != NULL &&
          (ret == FORGER_ENTITY_VISIT_CONTINUE || ret == FORGER_ENTITY_VISIT_BREAK))
        goto finish;
    }

  if (!FORGER_IS_CONTAINER (entity))
    {
      ret = FORGER_ENTITY_VISIT_CONTINUE;
      goto finish;
    }

  ForgerContainerPrivate *priv = forger_container_get_instance_private (FORGER_CONTAINER (entity));
  iter = priv->children.head;

  while (iter != NULL)
    {
      ForgerEntity *child = iter->data;

      iter = iter->next;
      ret = do_traversal (child, traversal);
      if (ret == FORGER_ENTITY_VISIT_BREAK)
        goto finish;
    }

  if (traversal->type == G_POST_ORDER && can_callback_node (entity, traversal->flags))
    ret = traversal->callback (entity, traversal->user_data);

finish:
  traversal->depth++;

  return ret;
}

/**
 * forger_container_traverse:
 * @self: a #ForgerContainer
 * @traverse_type: the type of traversal, pre and post supported
 * @traverse_flags: the flags for what nodes to match
 * @max_depth: the max depth for the traversal or -1 for all
 * @traverse_func: (scope call): the callback for each matching node
 * @user_data: user data for @traverse_func
 *
 * Calls @traverse_func for each node that matches the requested
 * type, flags, and depth.
 *
 * Traversal is stopped if @traverse_func returns %TRUE.
 */
void
forger_container_traverse (ForgerContainer         *self,
                           GTraverseType            traverse_type,
                           GTraverseFlags           traverse_flags,
                           gint                     max_depth,
                           ForgerSceneTraverseFunc  traverse_func,
                           gpointer                 user_data)
{
  ForgerTreeTraversal traverse;

  g_return_if_fail (FORGER_IS_CONTAINER (self));
  g_return_if_fail (traverse_type == G_PRE_ORDER ||
                    traverse_type == G_POST_ORDER);
  g_return_if_fail (traverse_func != NULL);

  traverse.type = traverse_type;
  traverse.flags = traverse_flags;
  traverse.depth = max_depth < 0 ? G_MAXINT : max_depth;
  traverse.callback = traverse_func;
  traverse.user_data = user_data;

  do_traversal (FORGER_ENTITY (self), &traverse);
}

