/*
 * forger-mesh-components.c
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
#define G_LOG_DOMAIN "forger-mesh-components"

#include "config.h"
#include "forger-debug.h"

#include "forger-mesh-components.h"


/*
 * -----< MESH FACE COMPONENT >----- *
 */
struct _ForgerMeshFace
{
  ForgerMeshFace *   next_;
  GArray *           triangles;
  GArray *           edges;

  guint              start_index;
};

G_DEFINE_BOXED_TYPE (ForgerMeshFace, forger_mesh_face, forger_mesh_face_copy, forger_mesh_face_free)

/**
 * forger_mesh_face_new:
 *
 * Creates a new dynamically allocated #ForgerMeshFace.
 *
 * Returns: (transfer full): A newly created #ForgerMeshFace
 */
ForgerMeshFace *
forger_mesh_face_new (void)
{
  ForgerMeshFace *self;
  self = g_slice_new0 (ForgerMeshFace);
  self->triangles = g_array_new (FALSE, TRUE, sizeof(ForgerTriangle));
  self->edges = g_array_new (FALSE, TRUE, sizeof(ForgerMeshEdge));
  return self;
}

/**
 * forger_mesh_face_copy:
 * @self: a #ForgerMeshFace
 *
 * Makes a deep copy of a #ForgerMeshFace.
 *
 * Returns: (transfer full): A newly created #ForgerMeshFace with the same
 *   contents as @self
 */
ForgerMeshFace *
forger_mesh_face_copy (ForgerMeshFace *self)
{
  ForgerMeshFace *copy;
  g_return_val_if_fail (self, NULL);
  copy = forger_mesh_face_new ();
  copy->triangles = g_array_new (FALSE, TRUE, sizeof(ForgerTriangle));
  copy->edges = g_array_new (FALSE, TRUE, sizeof(ForgerMeshEdge));
  return copy;
}

/**
 * forger_mesh_face_free:
 * @self: a #ForgerMeshFace
 *
 * Frees a #ForgerMeshFace allocated using forger_mesh_face_new()
 * or forger_mesh_face_copy().
 */
void
forger_mesh_face_free (ForgerMeshFace *self)
{
  g_return_if_fail (self);
  g_array_free (self->edges, TRUE);
  g_array_free (self->triangles, TRUE);
  g_slice_free (ForgerMeshFace, self);
}

/**
 * forger_mesh_face_next:
 * @self: a #ForgerMeshFace
 *
 * Get the #ForgerMeshFace next to @self in the Mesh
 *
 * Returns: (transfer full) (nullable): a #ForgerMeshFace
 */
ForgerMeshFace *
forger_mesh_face_next (ForgerMeshFace *self)
{
  g_return_val_if_fail (self, NULL);
  return self->next_;
}

/**
 * forger_mesh_face_set_next:
 * @self: a #ForgerMeshFace
 * @next: a #ForgerMeshFace
 *
 * Set the #ForgerMeshFace next to @self in the Mesh
 *
 */
void
forger_mesh_face_set_next (ForgerMeshFace *self,
                           ForgerMeshFace *next)
{
  g_return_if_fail (self);
  self->next_ = next;
}

/**
 * forger_mesh_face_push_triangle:
 * @self: a #ForgerMeshFace
 * @triangle: a #ForgerTriangle
 *
 * Pushs @triangle to the top of the triangles stack of @self
 *
 */
void
forger_mesh_face_push_triangle (ForgerMeshFace *self,
                                ForgerTriangle *triangle)
{
  g_return_if_fail (self);
  g_return_if_fail (triangle);
  g_array_append_val (self->triangles, *triangle);
}

/**
 * forger_mesh_face_pop_triangle:
 * @self: a #ForgerMeshFace
 *
 * Removes the triangle from the head of the triangles stack of @self
 *
 */
void
forger_mesh_face_pop_triangle (ForgerMeshFace *self)
{
  g_return_if_fail (self);
  g_array_remove_index (self->triangles, self->triangles->len -1);
}

/**
 * forger_mesh_face_foreach_triangle:
 * @self: a #ForgerMeshFace
 * @func_: (scope call): a #GFunc
 * @user_data: a pointer to data to pass to @func_
 *
 * Calls @func_ on each triangle in the triangles stack of @self
 *
 */
void
forger_mesh_face_foreach_triangle (ForgerMeshFace *self,
                                   GFunc           func_,
                                   gpointer        user_data)
{
  g_return_if_fail (self);

  for (guint i = 0; i < self->triangles->len; i++)
    {
      ForgerTriangle tris = g_array_index (self->triangles, ForgerTriangle, i);
      func_ (&tris, user_data);
    }
}

/**
 * forger_mesh_face_push_edge:
 * @self: a #ForgerMeshFace
 * @edge: a #ForgerMeshEdge
 *
 * Pushs @edge to the top of the edges stack of @self
 *
 */
void
forger_mesh_face_push_edge (ForgerMeshFace *self,
                            ForgerMeshEdge *edge)
{
  g_return_if_fail (self);
  g_return_if_fail (edge);
  g_array_append_val (self->edges, *edge);
}

/**
 * forger_mesh_face_pop_edge:
 * @self: a #ForgerMeshFace
 *
 * Removes the edge from the head of the edges stack of @self
 *
 */
void
forger_mesh_face_pop_edge (ForgerMeshFace *self)
{
  g_return_if_fail (self);
  g_array_remove_index (self->edges, self->edges->len - 1);
}

/**
 * forger_mesh_face_foreach_edge:
 * @self: a #ForgerMeshFace
 * @func_: (scope call): a #GFunc
 * @user_data: a pointer to data to pass to @func_
 *
 * Calls @func_ on each edge in the edges stack of @self
 *
 */
void
forger_mesh_face_foreach_edge (ForgerMeshFace *self,
                               GFunc           func_,
                               gpointer        user_data)
{
  g_return_if_fail (self);
  for (guint i = 0; i < self->edges->len; i++)
    {
      ForgerMeshEdge edge = g_array_index (self->edges, ForgerMeshEdge, i);
      func_ (&edge, user_data);
    }
}

guint
forger_mesh_face_get_start_index (ForgerMeshFace *self)
{
  g_return_val_if_fail (self, 0);
  return self->start_index;
}

void
forger_mesh_face_set_start_index (ForgerMeshFace *self,
                                  const guint     index_)
{
  g_return_if_fail (self);
  self->start_index = index_;
}
