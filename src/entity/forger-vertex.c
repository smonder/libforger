/*
 * forger-vertex.c
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "forger-vertex"

#include "config.h"
#include "forger-debug.h"

#include <epoxy/gl.h>

#include "forger-vertex.h"

G_DEFINE_BOXED_TYPE (ForgerVertex, forger_vertex, forger_vertex_copy, forger_vertex_free)

/**
 * forger_vertex_new:
 *
 * Creates a new #ForgerVertex.
 *
 * Returns: (transfer full): A newly created #ForgerVertex
 */
ForgerVertex *
forger_vertex_new (void)
{
  ForgerVertex *self;

  self = g_slice_new0 (ForgerVertex);

  return self;
}

/**
 * forger_vertex_copy:
 * @self: a #ForgerVertex
 *
 * Makes a deep copy of a #ForgerVertex.
 *
 * Returns: (transfer full): A newly created #ForgerVertex with the same
 *   contents as @self
 */
ForgerVertex *
forger_vertex_copy (ForgerVertex *self)
{
  ForgerVertex *copy;

  g_return_val_if_fail (self, NULL);

  copy = forger_vertex_new ();
  memcpy (&copy->position, &self->position, sizeof(graphene_point3d_t));
  memcpy (&copy->normal, &self->normal, sizeof(graphene_point3d_t));
  memcpy (&copy->uvmap, &self->uvmap, sizeof(graphene_point_t));

  return copy;
}

/**
 * forger_vertex_free:
 * @self: a #ForgerVertex
 *
 * Frees a #ForgerVertex allocated using forger_vertex_new()
 * or forger_vertex_copy().
 */
void
forger_vertex_free (ForgerVertex *self)
{
  g_return_if_fail (self);

  g_slice_free (ForgerVertex, self);
}
