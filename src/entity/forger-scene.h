/*
 * forger-scene.h
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#if !defined(FORGER_INSIDE) && !defined(LIBFORGER_COMPILATION)
#error "Only <libforger.h> can be included directly."
#endif

#include <glib-object.h>

#include "forger-container.h"
#include "forger-types.h"
#include "forger-version-macros.h"

G_BEGIN_DECLS

#define FORGER_TYPE_SCENE (forger_scene_get_type())

FORGER_AVAILABLE_IN_ALL
G_DECLARE_FINAL_TYPE (ForgerScene, forger_scene, FORGER, SCENE, GObject)

FORGER_AVAILABLE_IN_ALL
ForgerScene *          forger_scene_new              (void) G_GNUC_WARN_UNUSED_RESULT;

FORGER_AVAILABLE_IN_ALL
gchar *                forger_scene_get_title        (ForgerScene *          self);
FORGER_AVAILABLE_IN_ALL
void                   forger_scene_set_title        (ForgerScene *          self,
                                                      const gchar *          setting);

FORGER_AVAILABLE_IN_ALL
gchar *                forger_scene_get_location     (ForgerScene *          self);
FORGER_AVAILABLE_IN_ALL
void                   forger_scene_set_location     (ForgerScene *          self,
                                                      const gchar *          setting);

FORGER_AVAILABLE_IN_ALL
gchar *                forger_scene_get_file_path    (ForgerScene *          self);
FORGER_AVAILABLE_IN_ALL
void                   forger_scene_set_file_path    (ForgerScene *          self,
                                                      const gchar *          setting);

FORGER_AVAILABLE_IN_ALL
ForgerMeasuringSystem  forger_scene_get_units        (ForgerScene *          self);
FORGER_AVAILABLE_IN_ALL
void                   forger_scene_set_units        (ForgerScene *          self,
                                                      ForgerMeasuringSystem  setting);

FORGER_AVAILABLE_IN_ALL
ForgerContainer *      forger_scene_get_root         (ForgerScene *          self);

G_END_DECLS
