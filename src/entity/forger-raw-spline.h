/*
 * forger-raw-spline.h
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#if !defined(FORGER_INSIDE) && !defined(LIBFORGER_COMPILATION)
#error "Only <libforger.h> can be included directly."
#endif

#include <gdk/gdk.h>
#include <graphene.h>

#include "forger-entity.h"
#include "forger-version-macros.h"

G_BEGIN_DECLS

#define FORGER_TYPE_RAW_SPLINE (forger_raw_spline_get_type())

FORGER_AVAILABLE_IN_ALL
G_DECLARE_DERIVABLE_TYPE (ForgerRawSpline, forger_raw_spline, FORGER, RAW_SPLINE, ForgerEntity)

struct _ForgerRawSplineClass
{
  ForgerEntityClass parent_class;
};


FORGER_AVAILABLE_IN_ALL
ForgerEntity *       forger_raw_spline_new                    (const gchar *        name,
                                                               gboolean             cyclic,
                                                               GdkRGBA *            color) G_GNUC_WARN_UNUSED_RESULT;

FORGER_AVAILABLE_IN_ALL
guint                forger_raw_spline_get_thickness          (ForgerRawSpline *    self);
FORGER_AVAILABLE_IN_ALL
void                 forger_raw_spline_set_thickness          (ForgerRawSpline *    self,
                                                               const guint          setting);
FORGER_AVAILABLE_IN_ALL
gboolean             forger_raw_spline_is_cyclic              (ForgerRawSpline *    self);
FORGER_AVAILABLE_IN_ALL
void                 forger_raw_spline_set_cyclic             (ForgerRawSpline *    self,
                                                               gboolean             setting);
FORGER_AVAILABLE_IN_ALL
guint                forger_raw_spline_get_active_point       (ForgerRawSpline *   self);
FORGER_AVAILABLE_IN_ALL
void                 forger_raw_spline_select_point           (ForgerRawSpline *    self,
                                                               const guint          point_index);

FORGER_AVAILABLE_IN_ALL
void                 forger_raw_spline_append_point           (ForgerRawSpline *    self,
                                                               graphene_point3d_t * point);
FORGER_AVAILABLE_IN_ALL
void                 forger_raw_spline_append_colored_point   (ForgerRawSpline *    self,
                                                               graphene_point3d_t * point,
                                                               GdkRGBA *            bvcolor);
FORGER_AVAILABLE_IN_ALL
void                 forger_raw_spline_append_points          (ForgerRawSpline *    self,
                                                               gconstpointer        points,
                                                               const guint          count);
FORGER_AVAILABLE_IN_ALL
void                 forger_raw_spline_append_colored_points  (ForgerRawSpline *    self,
                                                               gconstpointer        point_buffer,
                                                               gconstpointer        bvcolor_buffer,
                                                               const guint          count);
FORGER_AVAILABLE_IN_ALL
void                 forger_raw_spline_prepend_point          (ForgerRawSpline *    self,
                                                               graphene_point3d_t * point);
FORGER_AVAILABLE_IN_ALL
void                 forger_raw_spline_prepend_colored_point  (ForgerRawSpline *    self,
                                                               graphene_point3d_t * point,
                                                               GdkRGBA *            bvcolor);
FORGER_AVAILABLE_IN_ALL
void                 forger_raw_spline_prepend_points         (ForgerRawSpline *    self,
                                                               gconstpointer        points,
                                                               const guint          count);
FORGER_AVAILABLE_IN_ALL
void                 forger_raw_spline_prepend_colored_points (ForgerRawSpline *    self,
                                                               gconstpointer        point_buffer,
                                                               gconstpointer        bvcolor_buffer,
                                                               const guint          count);

FORGER_AVAILABLE_IN_ALL
graphene_point3d_t * forger_raw_spline_get_point_at           (ForgerRawSpline *    self,
                                                               const guint          index_);
FORGER_AVAILABLE_IN_ALL
GdkRGBA *            forger_raw_spline_get_pcolor_at          (ForgerRawSpline *    self,
                                                               const guint          index_);

G_END_DECLS
