/*
 * forger-mesh.c
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
#define G_LOG_DOMAIN "forger-mesh"

#include "config.h"
#include "forger-debug.h"

#include "forger-mesh.h"
#include "forger-bounding-box.h"
#include "forger-gizmo.h"
#include "forger-vertex.h"
#include "renderer/forger-mesh-render-node.h"
#include "renderer/forger-edges-render-node.h"

/**
 * SECTION: ForgerMesh
 * @short_description: The base 3D solid in that can be rendered in FORGER.
 * @title: ForgerMesh.
 *
 * This is the base class of all 3D objects in Forger.
 *
 * Since: 0.1
 */

typedef struct
{
  /* Vertices data of the entity */
  GArray * positions;
  GArray * normals;
  GArray * uvmaps;

  /* These index buffers are going to be filled and updated when neccessary */
  GArray * faces_index_buffer;
  GArray * edges_index_buffer;
  guint    needs_update_buffers : 1;

  /* The faces linked list */
  ForgerMeshFace * faces;
} ForgerMeshPrivate;

G_DEFINE_TYPE_WITH_PRIVATE (ForgerMesh, forger_mesh, FORGER_TYPE_ENTITY)

enum {
  PROP_0,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];


/*
 * -----< FORGER_ENTITY CLASS IMPLEMENTATION >----- *
 */
static const gchar *
forger_mesh_real_get_icon_name (ForgerEntity * entity)
{
  return "package-x-generic-symbolic";
}

static void
update_buffers (ForgerMeshPrivate *priv,
                ForgerRenderNode  *node_A,
                ForgerRenderNode  *node_B)
{
  forger_render_node_update_positions (node_A,
                                       priv->positions->data,
                                       priv->positions->len);
  forger_render_node_update_normals (node_A,
                                     priv->normals->data,
                                     priv->normals->len);
  forger_render_node_update_uvmap (node_A,
                                   priv->uvmaps->data,
                                   priv->uvmaps->len);
  forger_render_node_update_indices (node_A,
                                     priv->faces_index_buffer->data,
                                     priv->faces_index_buffer->len);

  forger_render_node_update_positions (node_B,
                                       priv->positions->data,
                                       priv->positions->len);
  forger_render_node_update_indices (node_B,
                                     priv->edges_index_buffer->data,
                                     priv->edges_index_buffer->len);
}

static void
get_tris_indices (ForgerTriangle    *tris,
                  ForgerMeshPrivate *priv)
{
  g_array_append_val (priv->faces_index_buffer, *tris);
}

static void
get_edges_indices (ForgerMeshEdge    *edge,
                   ForgerMeshPrivate *priv)
{
  g_array_append_val (priv->edges_index_buffer, *edge);
}

static void
update_indices (ForgerMeshPrivate *priv)
{
  g_array_remove_range (priv->faces_index_buffer, 0, priv->faces_index_buffer->len);
  g_array_remove_range (priv->edges_index_buffer, 0, priv->edges_index_buffer->len);

  for (ForgerMeshFace *iter = priv->faces;
       iter != NULL;
       iter = forger_mesh_face_next (iter))
    {
      forger_mesh_face_foreach_triangle (iter, (GFunc)get_tris_indices, priv);
      forger_mesh_face_foreach_edge (iter, (GFunc)get_edges_indices, priv);
    }
  priv->needs_update_buffers = !!FALSE;
}

static void
forger_mesh_real_update_buffers (ForgerEntity * entity)
{
  ForgerMesh *self = (ForgerMesh *)entity;
  ForgerMeshPrivate *priv = forger_mesh_get_instance_private (self);

  g_assert (FORGER_IS_MESH (self));

  if (priv->needs_update_buffers)
    update_indices (priv);

  update_buffers (priv,
                  FORGER_RENDER_NODE (forger_entity_get (entity, FORGER_COMPONENT_RENDER_NODE)),
                  FORGER_RENDER_NODE (forger_entity_get (entity, FORGER_COMPONENT_EDGES_RENDER_NODE)));
}

static gboolean
forger_mesh_real_ray_intersects (ForgerEntity   *entity,
                                 graphene_ray_t *ray)
{
  ForgerMesh *self = (ForgerMesh *)entity;
  ForgerMeshPrivate *priv = forger_mesh_get_instance_private (self);
  ForgerGizmo *gizmo;
  ForgerBoundingBox *bbox;

  g_assert (FORGER_IS_MESH (self));

  gizmo = FORGER_GIZMO (forger_entity_get (entity, FORGER_COMPONENT_GIZMO));
  bbox = FORGER_BOUNDING_BOX (forger_entity_get (entity, FORGER_COMPONENT_BOUNDING_BOX));

  forger_bounding_box_update_from_buffer (bbox, priv->positions);
  return forger_bounding_box_ray_intersects (bbox, forger_gizmo_get_matrix (gizmo), ray);
}


/*
 * -----< G_OBJECT CLASS IMPLEMENTATION >----- *
 */
static void
forger_mesh_finalize (GObject *object)
{
  ForgerMesh *self = (ForgerMesh *)object;
  ForgerMeshPrivate *priv = forger_mesh_get_instance_private (self);

  g_array_free (priv->positions, FALSE);
  g_array_free (priv->normals, FALSE);
  g_array_free (priv->uvmaps, FALSE);
  g_array_free (priv->faces_index_buffer, FALSE);
  g_array_free (priv->edges_index_buffer, FALSE);

  FORGER_TODO ("Free the edges and faces queues");

  G_OBJECT_CLASS (forger_mesh_parent_class)->finalize (object);
}

static void
forger_mesh_get_property (GObject    *object,
                          guint       prop_id,
                          GValue     *value,
                          GParamSpec *pspec)
{
  ForgerMesh *self = FORGER_MESH (object);

  switch (prop_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
forger_mesh_set_property (GObject      *object,
                          guint         prop_id,
                          const GValue *value,
                          GParamSpec   *pspec)
{
  ForgerMesh *self = FORGER_MESH (object);

  switch (prop_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
forger_mesh_class_init (ForgerMeshClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  ForgerEntityClass *entity_class = FORGER_ENTITY_CLASS (klass);

  object_class->finalize = forger_mesh_finalize;
  object_class->get_property = forger_mesh_get_property;
  object_class->set_property = forger_mesh_set_property;

  entity_class->get_icon_name = forger_mesh_real_get_icon_name;
  entity_class->update_buffers = forger_mesh_real_update_buffers;
  entity_class->ray_intersects = forger_mesh_real_ray_intersects;
}

static void
forger_mesh_init (ForgerMesh *self)
{
  ForgerMeshPrivate *priv = forger_mesh_get_instance_private (self);

  forger_entity_set (FORGER_ENTITY (self),
                     FORGER_COMPONENT_RENDER_NODE,
                     FORGER_ENTITY_COMPONENT (forger_mesh_render_node_new (1024)));

  forger_entity_set (FORGER_ENTITY (self),
                     FORGER_COMPONENT_GIZMO,
                     FORGER_ENTITY_COMPONENT (forger_gizmo_new ()));

  forger_entity_set (FORGER_ENTITY (self),
                     FORGER_COMPONENT_BOUNDING_BOX,
                     forger_bounding_box_new ());

  forger_entity_set (FORGER_ENTITY (self),
                     FORGER_COMPONENT_EDGES_RENDER_NODE,
                     FORGER_ENTITY_COMPONENT (forger_edges_render_node_new (1024)));

  /* Prepare the buffers */
  priv->positions = g_array_new (FALSE, FALSE, sizeof(graphene_point3d_t));
  priv->normals = g_array_new (FALSE, FALSE, sizeof(graphene_point3d_t));
  priv->uvmaps = g_array_new (FALSE, FALSE, sizeof(graphene_point_t));
  priv->faces_index_buffer = g_array_new (FALSE, FALSE, sizeof(ForgerTriangle));
  priv->edges_index_buffer = g_array_new (FALSE, FALSE, sizeof(ForgerMeshEdge));

  /* Set the index_buffers to be updated */
  priv->needs_update_buffers = !!TRUE;
}


/*
 * -----< CONSTRUCTORS >----- *
 */
/**
 * forger_mesh_new:
 *
 * Create a new #ForgerMesh.
 *
 * Returns: (transfer full): a newly created #ForgerEntity
 */
ForgerEntity *
forger_mesh_new (void)
{
  return g_object_new (FORGER_TYPE_MESH, NULL);
}


/*
 * -----< ADD / REMOVE / MODIFY FACES >---- *
 */
static void
add_face_foreach_triangle_cb (ForgerTriangle *tris,
                              guint          *iter_index)
{
  tris->a_index += *iter_index;
  tris->b_index += *iter_index;
  tris->c_index += *iter_index;
}

static void
add_face_foreach_edge_cb (ForgerMeshEdge *edge,
                          guint          *iter_index)
{
  edge->a_index += *iter_index;
  edge->b_index += *iter_index;
}

/**
 * forger_mesh_append_face:
 * @self: a #ForgerMesh
 * @face: a #ForgerMeshFace
 * @vertices: (element-type Forger.Vertex): a #GArray
 *
 * Append @face to @self.
 * @face indices are local to face and not supposed to continue @self's indices.
 * This method will alter @face's indices to align with @self.
 *
 */
void
forger_mesh_append_face (ForgerMesh     *self,
                         ForgerMeshFace *face,
                         GArray         *vertices)
{
  ForgerMeshPrivate *priv = forger_mesh_get_instance_private (self);
  ForgerMeshFace *last_face;
  guint start_index;

  FORGER_ENTRY;
  g_return_if_fail (FORGER_IS_MESH (self));
  g_return_if_fail (face);
  g_return_if_fail (vertices && vertices->len >= 3);
  g_return_if_fail (forger_mesh_face_next (face) == NULL);

  start_index = priv->positions->len;

  for (guint i = 0; i < vertices->len; i++)
    {
      ForgerVertex vert = g_array_index (vertices, ForgerVertex, i);
      g_array_append_val (priv->positions, vert.position);
      g_array_append_val (priv->normals, vert.normal);
      g_array_append_val (priv->uvmaps, vert.uvmap);
    }

  /* update the indices in the face */
  forger_mesh_face_foreach_triangle (face, (GFunc)add_face_foreach_triangle_cb, &start_index);
  forger_mesh_face_foreach_edge (face, (GFunc)add_face_foreach_edge_cb, &start_index);
  forger_mesh_face_set_start_index (face, start_index);

  /* add the face */
  if (priv->faces != NULL)
    {
      last_face = priv->faces;
      while (forger_mesh_face_next (last_face) != NULL)
        last_face = forger_mesh_face_next (last_face);

      forger_mesh_face_set_next (last_face, face);
    }
  else
    priv->faces = face;

  /* Set the index_buffers to be updated */
  priv->needs_update_buffers = !!TRUE;
  FORGER_EXIT;
}

/**
 * forger_mesh_append_face_series:
 * @self: a #ForgerMesh
 * @face: a #ForgerMeshFace: the head of the sieries
 * @vertices: (element-type Forger.Vertex): a #GArray: the vertex data buffer of all faces
 *
 * Append the face series whose head is @face to @self.
 * The indices of the series should be continuous starting from @face towards
 * the end.
 * This method will alter all the indices of the series from @face on-wards
 * to align with @self.
 *
 */
void
forger_mesh_append_face_series (ForgerMesh     *self,
                                ForgerMeshFace *face,
                                GArray         *vertices)
{
  ForgerMeshPrivate *priv = forger_mesh_get_instance_private (self);
  ForgerMeshFace *last_face;
  guint start_index;

  FORGER_ENTRY;
  g_return_if_fail (FORGER_IS_MESH (self));
  g_return_if_fail (face);
  g_return_if_fail (vertices && vertices->len >= 3);

  start_index = priv->positions->len;

  /* update the buffer data */
  for (guint i = 0; i < vertices->len; i++)
    {
      ForgerVertex vert = g_array_index (vertices, ForgerVertex, i);
      g_array_append_val (priv->positions, vert.position);
      g_array_append_val (priv->normals, vert.normal);
      g_array_append_val (priv->uvmaps, vert.uvmap);
    }

  /* update the indices in the faces */
  for (ForgerMeshFace *iter = face;
       iter != NULL;
       iter = forger_mesh_face_next (iter))
    {
      forger_mesh_face_foreach_triangle (iter, (GFunc)add_face_foreach_triangle_cb, &start_index);
      forger_mesh_face_foreach_edge (iter, (GFunc)add_face_foreach_edge_cb, &start_index);
      forger_mesh_face_set_start_index (iter, start_index);
    }

  /* append the faces */
  if (priv->faces != NULL)
    {
      last_face = priv->faces;
      while (forger_mesh_face_next (last_face) != NULL)
        last_face = forger_mesh_face_next (last_face);
      forger_mesh_face_set_next (last_face, face);
    }
  else
    priv->faces = face;

  /* Set the index_buffers to be updated */
  priv->needs_update_buffers = !!TRUE;
  FORGER_EXIT;
}

