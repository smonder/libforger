/*
 * forger-gizmo.c
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "forger-gizmo"

#include "config.h"
#include "forger-debug.h"

#include <glib/gi18n.h>

#include "forger-gizmo.h"

struct _ForgerGizmo
{
  ForgerEntityComponent parent_instance;

  graphene_matrix_t  matrix;

  graphene_point3d_t location;
  graphene_point3d_t rotation;
  graphene_point3d_t scale;
};

G_DEFINE_FINAL_TYPE (ForgerGizmo, forger_gizmo, FORGER_TYPE_ENTITY_COMPONENT)

enum {
  PROP_0,
  PROP_LOCATION,
  PROP_ROTATION,
  PROP_SCALE,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];


/*
 * -----< FORGER_ENTITY_COMPONENT CLASS IMPLEMENTATION >----- *
 */
static const gchar *
forger_gizmo_get_display_name (ForgerEntityComponent *component)
{
  return _("Transformations");
}

static const gchar *
forger_gizmo_get_icon_name (ForgerEntityComponent *component)
{
  return NULL;
}

static const gchar *
forger_gizmo_get_description  (ForgerEntityComponent *component)
{
  return _("The transformation gizmo");
}


/*
 * -----< G_OBJECT CLASS IMPLEMENTATION >----- *
 */
static void
forger_gizmo_get_property (GObject    *object,
                           guint       prop_id,
                           GValue     *value,
                           GParamSpec *pspec)
{
  ForgerGizmo *self = FORGER_GIZMO (object);

  switch (prop_id)
    {
    case PROP_LOCATION:
      g_value_set_boxed (value, forger_gizmo_get_location (self));
      break;

    case PROP_ROTATION:
      g_value_set_boxed (value, forger_gizmo_get_rotation (self));
      break;

    case PROP_SCALE:
      g_value_set_boxed (value, forger_gizmo_get_scale (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
forger_gizmo_set_property (GObject      *object,
                           guint         prop_id,
                           const GValue *value,
                           GParamSpec   *pspec)
{
  ForgerGizmo *self = FORGER_GIZMO (object);

  switch (prop_id)
    {
    case PROP_LOCATION:
      forger_gizmo_set_location (self, g_value_get_boxed (value));
      break;

    case PROP_ROTATION:
      forger_gizmo_set_rotation (self, g_value_get_boxed (value));
      break;

    case PROP_SCALE:
      forger_gizmo_set_scale (self, g_value_get_boxed (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
forger_gizmo_class_init (ForgerGizmoClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  ForgerEntityComponentClass *fec_class = FORGER_ENTITY_COMPONENT_CLASS (klass);

  object_class->get_property = forger_gizmo_get_property;
  object_class->set_property = forger_gizmo_set_property;

  fec_class->get_display_name = forger_gizmo_get_display_name;
  fec_class->get_icon_name = forger_gizmo_get_icon_name;
  fec_class->get_description = forger_gizmo_get_description;

  /**
   * ForgerGizmo:location:
   *
   * The "location" property is x, y, z location offset.
   *
   * Since: 0.1
   */
  properties [PROP_LOCATION] =
    g_param_spec_boxed ("location", NULL, NULL,
                        GRAPHENE_TYPE_POINT3D,
                        (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  /**
   * ForgerGizmo:rotation:
   *
   * The "rotation" property is the rotation angles about OX, OY, OZ
   * Axis respectively in %Degrees.
   *
   * Since: 0.1
   */
  properties [PROP_ROTATION] =
    g_param_spec_boxed ("rotation", NULL, NULL,
                        GRAPHENE_TYPE_POINT3D,
                        (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  /**
   * ForgerGizmo:scale:
   *
   * The "scale" property is the scale deltas on OX, OY, OZ.
   *
   * Since: 0.1
   */
  properties [PROP_SCALE] =
    g_param_spec_boxed ("scale", NULL, NULL,
                        GRAPHENE_TYPE_POINT3D,
                        (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
forger_gizmo_init (ForgerGizmo *self)
{
  graphene_matrix_init_identity (&self->matrix);

  self->location = GRAPHENE_POINT3D_INIT_ZERO;
  self->rotation = GRAPHENE_POINT3D_INIT_ZERO;
  self->scale = (graphene_point3d_t){ .x = 1.0f , .y = 1.0f, .z = 1.0f };
}


/*
 * -----< CONSTRUCTORS >----- *
 */
/**
 * forger_gizmo_new:
 *
 * Create a new #ForgerGizmo
 *
 * Returns: a newly created #ForgerGizmo
 */
ForgerGizmo *
forger_gizmo_new (void)
{
  return g_object_new (FORGER_TYPE_GIZMO, NULL);
}


/*
 * -----< PROPERTIES >----- *
 */
void
forger_gizmo_set_location (ForgerGizmo        *self,
                           graphene_point3d_t *location)
{
  g_return_if_fail (FORGER_IS_GIZMO (self));
  graphene_point3d_init_from_point (&self->location, location);
  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_LOCATION]);
}

void
forger_gizmo_set_rotation (ForgerGizmo        *self,
                           graphene_point3d_t *rotation)
{
  g_return_if_fail (FORGER_IS_GIZMO (self));
  graphene_point3d_init_from_point (&self->rotation, rotation);
  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_ROTATION]);
}

void
forger_gizmo_set_scale (ForgerGizmo        *self,
                        graphene_point3d_t *scale)
{
  g_return_if_fail (FORGER_IS_GIZMO (self));
  graphene_point3d_init_from_point (&self->scale, scale);
  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_SCALE]);
}

void
forger_gizmo_set_scale_f (ForgerGizmo  *self,
                          const gfloat  factor)
{
  g_return_if_fail (FORGER_IS_GIZMO (self));

  self->scale.x = factor;
  self->scale.y = factor;
  self->scale.z = factor;

  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_SCALE]);
}

graphene_point3d_t *
forger_gizmo_get_location (ForgerGizmo *self)
{
  g_return_val_if_fail (FORGER_IS_GIZMO (self), NULL);
  return &self->location;
}

graphene_point3d_t *
forger_gizmo_get_rotation (ForgerGizmo *self)
{
  g_return_val_if_fail (FORGER_IS_GIZMO (self), NULL);
  return &self->rotation;
}

graphene_point3d_t *
forger_gizmo_get_scale (ForgerGizmo *self)
{
  g_return_val_if_fail (FORGER_IS_GIZMO (self), NULL);
  return &self->scale;
}


/*
 * -----< METHODS >----- *
 */
void
forger_gizmo_translate (ForgerGizmo     *self,
                        ForgerGizmoAxis  axis,
                        const gfloat     amount)
{
  g_return_if_fail (FORGER_IS_GIZMO (self));

  switch (axis)
    {
    case FORGER_AXIS_X:
      self->location.x += amount;
      break;

    case FORGER_AXIS_Y:
      self->location.y += amount;
      break;

    case FORGER_AXIS_Z:
      self->location.z += amount;
      break;

    default: g_assert_not_reached ();
    }
  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_LOCATION]);
}

void
forger_gizmo_rotate (ForgerGizmo     *self,
                     ForgerGizmoAxis  around_axis,
                     const gfloat     amount)
{
  g_return_if_fail (FORGER_IS_GIZMO (self));

  switch (around_axis)
    {
    case FORGER_AXIS_X:
      self->rotation.x += amount;
      break;

    case FORGER_AXIS_Y:
      self->rotation.y += amount;
      break;

    case FORGER_AXIS_Z:
      self->rotation.z += amount;
      break;

    default: g_assert_not_reached ();
    }
  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_ROTATION]);
}

void
forger_gizmo_scale (ForgerGizmo     *self,
                    ForgerGizmoAxis  axis,
                    const gfloat     amount)
{
  g_return_if_fail (FORGER_IS_GIZMO (self));

  switch (axis)
    {
    case FORGER_AXIS_X:
      self->scale.x += amount;
      break;

    case FORGER_AXIS_Y:
      self->scale.y += amount;
      break;

    case FORGER_AXIS_Z:
      self->scale.z += amount;
      break;

    default: g_assert_not_reached ();
    }
  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_SCALE]);
}

void
forger_gizmo_scale_uni (ForgerGizmo  *self,
                        const gfloat  amount)
{
  g_return_if_fail (FORGER_IS_GIZMO (self));

  self->scale.x += amount;
  self->scale.y += amount;
  self->scale.z += amount;

  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_SCALE]);
}

/*
 * -----< UTILITIES >----- *
 */
/**
 * forger_gizmo_get_matrix:
 *
 * Gets the underlaying transformation matrix of @self.
 *
 * Returns: (transfer full): a pointer to a #graphene_matrix_t or %NULL
 */
graphene_matrix_t *
forger_gizmo_get_matrix (ForgerGizmo *self)
{
  graphene_euler_t rotation;
  g_return_val_if_fail (FORGER_IS_GIZMO (self), NULL);

  graphene_euler_init (&rotation,
                       self->rotation.x,
                       self->rotation.y,
                       self->rotation.z);

  graphene_matrix_init_identity (&self->matrix);
  graphene_matrix_scale (&self->matrix, self->scale.x, self->scale.y, self->scale.z);
  graphene_matrix_rotate_euler (&self->matrix, &rotation);
  graphene_matrix_translate (&self->matrix, &self->location);

  return &self->matrix;
}
