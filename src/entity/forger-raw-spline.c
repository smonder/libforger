/*
 * forger-raw-spline.c
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
#define G_LOG_DOMAIN "forger-raw-spline"

#include "config.h"
#include "forger-debug.h"

#include <glib/gi18n.h>
#include <epoxy/gl.h>

#include "forger-raw-spline.h"
#include "forger-bounding-box.h"
#include "forger-gizmo.h"
#include "material/forger-material-spline.h"
#include "renderer/forger-spline-render-node.h"

/**
 * SECTION: ForgerRawSpline
 * @short_description: The base type for rendering 2D shapes in FORGER.
 * @title: ForgerRawSpline.
 *
 * This is the base type that all the other 2D classes that are going to be
 * rendered in FORGER should inherit from.
 *
 * Since: 0.1
 */

typedef struct
{
  GArray * points_positions;
  GArray * points_bvcolors;

  guint   selected_point;
} ForgerRawSplinePrivate;

G_DEFINE_TYPE_WITH_PRIVATE (ForgerRawSpline, forger_raw_spline, FORGER_TYPE_ENTITY)

enum {
  PROP_0,
  PROP_THICKNESS,
  PROP_CYCLIC,
  PROP_ACTIVE_POINT,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];


/*
 * -----< FORGER_ENTITY CLASS IMPLEMENTATION >----- *
 */
static const gchar *
forger_raw_spline_real_get_icon_name (ForgerEntity * entity)
{
  return "folder-publicshare-symbolic";
}

static void
update_buffers (ForgerRawSplinePrivate *priv,
                ForgerRenderNode       *node)
{
  forger_render_node_update_positions (node,
                                       priv->points_positions->data,
                                       priv->points_positions->len);
  forger_render_node_update_normals (node,
                                     priv->points_bvcolors->data,
                                     priv->points_bvcolors->len);
  forger_spline_render_node_set_n_point (FORGER_SPLINE_RENDER_NODE (node),
                                         priv->points_positions->len);
}

static void
forger_raw_spline_real_update_buffers (ForgerEntity * entity)
{
  ForgerRawSpline *self = (ForgerRawSpline *)entity;
  ForgerRawSplinePrivate *priv = forger_raw_spline_get_instance_private (self);
  ForgerRenderNode *node;
  ForgerMaterial *material;

  material = FORGER_MATERIAL (forger_entity_get (entity, FORGER_COMPONENT_MATERIAL));
  if (!FORGER_IS_MATERIAL_SPLINE (material))
    g_error ("Splines can't be rendered with any type of material, please use spline material derivatives");

  node = FORGER_RENDER_NODE (forger_entity_get (entity, FORGER_COMPONENT_RENDER_NODE));
  update_buffers (priv, node);
}

static gboolean
forger_raw_spline_real_ray_intersects (ForgerEntity   *entity,
                                       graphene_ray_t *ray)
{
  ForgerRawSpline *self = (ForgerRawSpline *)entity;
  ForgerRawSplinePrivate *priv = forger_raw_spline_get_instance_private (self);
  ForgerGizmo *gizmo;
  ForgerBoundingBox *bbox;

  g_assert (FORGER_IS_RAW_SPLINE (self));

  gizmo = FORGER_GIZMO (forger_entity_get (entity, FORGER_COMPONENT_GIZMO));
  bbox = FORGER_BOUNDING_BOX (forger_entity_get (entity, FORGER_COMPONENT_BOUNDING_BOX));

  forger_bounding_box_update_from_buffer (bbox, priv->points_positions);
  return forger_bounding_box_ray_intersects (bbox, forger_gizmo_get_matrix (gizmo), ray);
}


/*
 * -----< G_OBJECT CLASS IMPLEMENTATION >----- *
 */
static void
forger_raw_spline_finalize (GObject *object)
{
  ForgerRawSpline *self = (ForgerRawSpline *)object;
  ForgerRawSplinePrivate *priv = forger_raw_spline_get_instance_private (self);

  g_array_free (priv->points_positions, TRUE);
  g_array_free (priv->points_bvcolors, TRUE);

  G_OBJECT_CLASS (forger_raw_spline_parent_class)->finalize (object);
}

static void
forger_raw_spline_get_property (GObject    *object,
                                guint       prop_id,
                                GValue     *value,
                                GParamSpec *pspec)
{
  ForgerRawSpline *self = FORGER_RAW_SPLINE (object);

  switch (prop_id)
    {
    case PROP_CYCLIC:
      g_value_set_boolean (value, forger_raw_spline_is_cyclic (self));
      break;

    case PROP_THICKNESS:
      g_value_set_uint (value, forger_raw_spline_get_thickness (self));
      break;

    case PROP_ACTIVE_POINT:
      g_value_set_uint (value, forger_raw_spline_get_active_point (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
forger_raw_spline_set_property (GObject      *object,
                                guint         prop_id,
                                const GValue *value,
                                GParamSpec   *pspec)
{
  ForgerRawSpline *self = FORGER_RAW_SPLINE (object);

  switch (prop_id)
    {
    case PROP_CYCLIC:
      forger_raw_spline_set_cyclic (self, g_value_get_boolean (value));
      break;

    case PROP_THICKNESS:
      forger_raw_spline_set_thickness (self, g_value_get_uint (value));
      break;

    case PROP_ACTIVE_POINT:
      forger_raw_spline_select_point (self, g_value_get_uint (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
forger_raw_spline_class_init (ForgerRawSplineClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  ForgerEntityClass *entity_class = FORGER_ENTITY_CLASS (klass);

  object_class->finalize = forger_raw_spline_finalize;
  object_class->get_property = forger_raw_spline_get_property;
  object_class->set_property = forger_raw_spline_set_property;

  entity_class->update_buffers = forger_raw_spline_real_update_buffers;
  entity_class->ray_intersects = forger_raw_spline_real_ray_intersects;
  entity_class->get_icon_name = forger_raw_spline_real_get_icon_name;

  /**
   * ForgerRawSpline:cyclic:
   *
   * The "cyclic" property denotes whether the spline is closed or opened
   *
   * Since: 0.1
   */
  properties [PROP_CYCLIC] =
    g_param_spec_boolean ("cyclic", NULL, NULL,
                          FALSE,
                          (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY |
                           G_PARAM_STATIC_STRINGS));

  /**
   * ForgerRawSpline:thickness:
   *
   * The "thickness" property denotes the light width of %self.
   *
   * Since: 0.1
   */
  properties [PROP_THICKNESS] =
    g_param_spec_uint ("thickness", NULL, NULL,
                       1, 7, 1,
                       (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY |
                        G_PARAM_STATIC_STRINGS));

  /**
   * ForgerRawSpline:active-point:
   *
   * The "active-point" property is the index of the selected point at %self
   * when %self is in edit mode.
   *
   * Since: 0.1
   */
  properties [PROP_ACTIVE_POINT] =
    g_param_spec_uint ("active-point", NULL, NULL,
                       0, G_MAXUINT, 0,
                       (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY |
                        G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
forger_raw_spline_init (ForgerRawSpline *self)
{
  ForgerRawSplinePrivate *priv = forger_raw_spline_get_instance_private (self);

  forger_entity_set (FORGER_ENTITY (self),
                     FORGER_COMPONENT_RENDER_NODE,
                     FORGER_ENTITY_COMPONENT (forger_spline_render_node_new (1000)));

  forger_entity_set (FORGER_ENTITY (self),
                     FORGER_COMPONENT_GIZMO,
                     FORGER_ENTITY_COMPONENT (forger_gizmo_new ()));

  forger_entity_set (FORGER_ENTITY (self),
                     FORGER_COMPONENT_BOUNDING_BOX,
                     forger_bounding_box_new ());

  /* Prepare the buffers */
  priv->points_positions = g_array_new (FALSE, FALSE, sizeof(graphene_point3d_t));
  priv->points_bvcolors = g_array_new (FALSE, FALSE, sizeof(GdkRGBA));

  /* Setup some properties */
  priv->selected_point = 0;
}


/*
 * -----< CONSTRUCTORS >----- *
 */
/**
 * forger_raw_spline_new:
 *
 * Create a new #ForgerRawSpline.
 *
 * Returns: (transfer full): a newly created #ForgerRawSpline
 */
ForgerEntity *
forger_raw_spline_new (const gchar *name,
                       gboolean     cyclic,
                       GdkRGBA     *color)
{
  return g_object_new (FORGER_TYPE_RAW_SPLINE,
                       "name", name,
                       "cyclic", cyclic,
                       "color", color,
                       NULL);
}


/*
 * -----< PROPERTIES >----- *
 */
gboolean
forger_raw_spline_is_cyclic (ForgerRawSpline *self)
{
  ForgerRenderNode *node;
  g_return_val_if_fail (FORGER_IS_RAW_SPLINE (self), FALSE);

  node = FORGER_RENDER_NODE (forger_entity_get (FORGER_ENTITY (self),
                                                FORGER_COMPONENT_RENDER_NODE));
  return forger_spline_render_node_is_cyclic (FORGER_SPLINE_RENDER_NODE (node));
}

void
forger_raw_spline_set_cyclic (ForgerRawSpline *self,
                              gboolean         setting)
{
  ForgerRenderNode *node;
  g_return_if_fail (FORGER_IS_RAW_SPLINE (self));

  node = FORGER_RENDER_NODE (forger_entity_get (FORGER_ENTITY (self),
                                                FORGER_COMPONENT_RENDER_NODE));
  forger_spline_render_node_set_cyclic (FORGER_SPLINE_RENDER_NODE (node), setting);
  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_CYCLIC]);
}

guint
forger_raw_spline_get_thickness (ForgerRawSpline *self)
{
  ForgerRenderNode *node;
  g_return_val_if_fail (FORGER_IS_RAW_SPLINE (self), 0);

  node = FORGER_RENDER_NODE (forger_entity_get (FORGER_ENTITY (self),
                                                FORGER_COMPONENT_RENDER_NODE));
  return forger_spline_render_node_get_line_width (FORGER_SPLINE_RENDER_NODE (node));
}

void
forger_raw_spline_set_thickness (ForgerRawSpline *self,
                                 const guint      setting)
{
  ForgerRenderNode *node;
  g_return_if_fail (FORGER_IS_RAW_SPLINE (self));

  node = FORGER_RENDER_NODE (forger_entity_get (FORGER_ENTITY (self),
                                                FORGER_COMPONENT_RENDER_NODE));
  forger_spline_render_node_set_line_width (FORGER_SPLINE_RENDER_NODE (node), setting);
  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_THICKNESS]);
}

guint
forger_raw_spline_get_active_point (ForgerRawSpline *self)
{
  ForgerRawSplinePrivate *priv = forger_raw_spline_get_instance_private (self);
  g_return_val_if_fail (FORGER_IS_RAW_SPLINE (self), 0);
  return priv->selected_point;
}

void
forger_raw_spline_select_point (ForgerRawSpline *self,
                                const guint      point_index)
{
  ForgerRawSplinePrivate *priv = forger_raw_spline_get_instance_private (self);

  FORGER_ENTRY;
  g_return_if_fail (FORGER_IS_RAW_SPLINE (self));

  if (priv->selected_point == point_index)
    FORGER_EXIT;

  priv->selected_point = point_index;
  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_ACTIVE_POINT]);

  FORGER_EXIT;
}


/*
 * -----< RAW SPLINE PUBLIC METHODS >----- *
 */
void
forger_raw_spline_append_point (ForgerRawSpline    *self,
                                graphene_point3d_t *point)
{
  ForgerRawSplinePrivate *priv = forger_raw_spline_get_instance_private (self);
  GdkRGBA color = { 0.0f, 0.0f, 0.0f, 0.0f };

  FORGER_ENTRY;
  g_return_if_fail (FORGER_IS_RAW_SPLINE (self));
  g_return_if_fail (point);

  g_array_append_val (priv->points_positions, *point);
  g_array_append_val (priv->points_bvcolors, *gdk_rgba_copy (&color));

  FORGER_EXIT;
}

void
forger_raw_spline_append_colored_point (ForgerRawSpline    *self,
                                        graphene_point3d_t *point,
                                        GdkRGBA            *bvcolor)
{
  ForgerRawSplinePrivate *priv = forger_raw_spline_get_instance_private (self);

  FORGER_ENTRY;
  g_return_if_fail (FORGER_IS_RAW_SPLINE (self));
  g_return_if_fail (point || bvcolor);

  g_array_append_val (priv->points_positions, *point);
  g_array_append_val (priv->points_bvcolors, *bvcolor);

  FORGER_EXIT;
}

void
forger_raw_spline_append_points (ForgerRawSpline *self,
                                 gconstpointer    points,
                                 const guint      count)
{
  ForgerRawSplinePrivate *priv = forger_raw_spline_get_instance_private (self);
  GdkRGBA color = { 0.0f, 0.0f, 0.0f, 0.0f };

  FORGER_ENTRY;
  g_return_if_fail (FORGER_IS_RAW_SPLINE (self));
  g_return_if_fail (points && count > 0);

  g_array_append_vals (priv->points_positions, points, count);

  for (guint i = 0; i < count; i++)
    g_array_append_val (priv->points_bvcolors, *gdk_rgba_copy (&color));

  FORGER_EXIT;
}

void
forger_raw_spline_append_colored_points (ForgerRawSpline *self,
                                         gconstpointer    point_buffer,
                                         gconstpointer    bvcolor_buffer,
                                         const guint      count)
{
  ForgerRawSplinePrivate *priv = forger_raw_spline_get_instance_private (self);

  FORGER_ENTRY;
  g_return_if_fail (FORGER_IS_RAW_SPLINE (self));
  g_return_if_fail (point_buffer && bvcolor_buffer && count > 0);

  g_array_append_vals (priv->points_positions, point_buffer, count);
  g_array_append_vals (priv->points_bvcolors, bvcolor_buffer, count);

  FORGER_EXIT;
}

void
forger_raw_spline_prepend_point (ForgerRawSpline    *self,
                                 graphene_point3d_t *point)
{
  ForgerRawSplinePrivate *priv = forger_raw_spline_get_instance_private (self);
  GdkRGBA color = { 0.0f, 0.0f, 0.0f, 0.0f };

  FORGER_ENTRY;
  g_return_if_fail (FORGER_IS_RAW_SPLINE (self));
  g_return_if_fail (point);

  g_array_prepend_val (priv->points_positions, *point);
  g_array_prepend_val (priv->points_bvcolors, *gdk_rgba_copy (&color));

  FORGER_EXIT;
}

void
forger_raw_spline_prepend_colored_point (ForgerRawSpline    *self,
                                         graphene_point3d_t *point,
                                         GdkRGBA            *bvcolor)
{
  ForgerRawSplinePrivate *priv = forger_raw_spline_get_instance_private (self);

  FORGER_ENTRY;
  g_return_if_fail (FORGER_IS_RAW_SPLINE (self));
  g_return_if_fail (point || bvcolor);

  g_array_prepend_val (priv->points_positions, *point);
  g_array_prepend_val (priv->points_bvcolors, *bvcolor);

  FORGER_EXIT;
}

void
forger_raw_spline_prepend_points (ForgerRawSpline *self,
                                  gconstpointer    points,
                                  const guint      count)
{
  ForgerRawSplinePrivate *priv = forger_raw_spline_get_instance_private (self);
  GdkRGBA color = { 0.0f, 0.0f, 0.0f, 0.0f };

  FORGER_ENTRY;
  g_return_if_fail (FORGER_IS_RAW_SPLINE (self));
  g_return_if_fail (points && count > 0);

  g_array_prepend_vals (priv->points_positions, points, count);

  for (guint i = 0; i < count; i++)
    g_array_prepend_val (priv->points_bvcolors, *gdk_rgba_copy (&color));

  FORGER_EXIT;
}

void
forger_raw_spline_prepend_colored_points (ForgerRawSpline *self,
                                          gconstpointer    point_buffer,
                                          gconstpointer    bvcolor_buffer,
                                          const guint      count)
{
  ForgerRawSplinePrivate *priv = forger_raw_spline_get_instance_private (self);

  FORGER_ENTRY;
  g_return_if_fail (FORGER_IS_RAW_SPLINE (self));
  g_return_if_fail (point_buffer && bvcolor_buffer && count > 0);

  g_array_prepend_vals (priv->points_positions, point_buffer, count);
  g_array_prepend_vals (priv->points_bvcolors, bvcolor_buffer, count);

  FORGER_EXIT;
}

/**
 * forger_raw_spline_get_point_at:
 * @self: a #ForgerRawSpline
 * @index_: a guint: the index of the point
 *
 * Get the point position componenets at @index_
 *
 * Returns: (transfer full) (nullable): a pointer to #graphene_point3d_t
 */
graphene_point3d_t *
forger_raw_spline_get_point_at (ForgerRawSpline *self,
                                const guint      index_)
{
  ForgerRawSplinePrivate *priv = forger_raw_spline_get_instance_private (self);

  FORGER_ENTRY;
  g_return_val_if_fail (FORGER_IS_RAW_SPLINE (self), NULL);

  FORGER_RETURN (&g_array_index (priv->points_positions, graphene_point3d_t, index_));
}

/**
 * forger_raw_spline_get_pcolor_at:
 * @self: a #ForgerRawSpline
 * @index_: a guint: the index of the point
 *
 * Get the point color componenets at @index_
 *
 * Returns: (transfer full) (nullable): a pointer to #GdkRGBA
 */
GdkRGBA *
forger_raw_spline_get_pcolor_at (ForgerRawSpline *self,
                                 const guint      index_)
{
  ForgerRawSplinePrivate *priv = forger_raw_spline_get_instance_private (self);

  FORGER_ENTRY;
  g_return_val_if_fail (FORGER_IS_RAW_SPLINE (self), NULL);

  FORGER_RETURN (&g_array_index (priv->points_bvcolors, GdkRGBA, index_));
}
