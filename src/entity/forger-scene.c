/*
 * forger-scene.c
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "forger-scene"

#include "config.h"
#include "forger-debug.h"

#include <glib/gi18n.h>

#include "forger-enums.h"
#include "forger-scene.h"

struct _ForgerScene
{
  GObject parent_instance;

  /* The title property is used by the ui elements */
  gchar * title;

  /* Now we are going to represent the location as a text, but I am planning to
   * add a GeoLocation support for more accurate results in simulation or
   * rendering if we are going to add those in the future.
   */
  gchar * location;

  /* We need also a reference for file location so that we provide it to the
   * serializer
   */
  gchar * file_path;

  /* For now we are going to use an enum type for representing the measurements
   * system. however, in the near future this is going to be a complicated
   * system.
   */
  ForgerMeasuringSystem units;

  /* Since the scene is a tree structure which nodes are of type
   * "ForgerEntity" and "ForgerContainer"
   */
  ForgerContainer * root;
};

G_DEFINE_FINAL_TYPE (ForgerScene, forger_scene, G_TYPE_OBJECT)

enum {
  PROP_0,
  PROP_TITLE,
  PROP_LOCATION,
  PROP_FILE_PATH,
  PROP_UNITS,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];


/*
 * -----< SIGNAL CALLBACKS >----- *
 */


/*
 * -----< G_OBJECT CLASS IMPLEMENTATION >----- *
 */
static void
forger_scene_finalize (GObject *object)
{
  ForgerScene *self = (ForgerScene *)object;

  g_clear_pointer (&self->title, g_free);
  g_clear_pointer (&self->location, g_free);
  g_clear_pointer (&self->file_path, g_free);

  g_clear_object (&self->root);

  G_OBJECT_CLASS (forger_scene_parent_class)->finalize (object);
}

static void
forger_scene_get_property (GObject    *object,
                           guint       prop_id,
                           GValue     *value,
                           GParamSpec *pspec)
{
  ForgerScene *self = FORGER_SCENE (object);

  switch (prop_id)
    {
    case PROP_TITLE:
      g_value_set_string (value, forger_scene_get_title (self));
      break;

    case PROP_LOCATION:
      g_value_set_string (value, forger_scene_get_location (self));
      break;

    case PROP_FILE_PATH:
      g_value_set_string (value, forger_scene_get_file_path (self));
      break;

    case PROP_UNITS:
      g_value_set_enum (value, forger_scene_get_units (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
forger_scene_set_property (GObject      *object,
                           guint         prop_id,
                           const GValue *value,
                           GParamSpec   *pspec)
{
  ForgerScene *self = FORGER_SCENE (object);

  switch (prop_id)
    {
    case PROP_TITLE:
      forger_scene_set_title (self, g_value_get_string (value));
      break;

    case PROP_LOCATION:
      forger_scene_set_location (self, g_value_get_string (value));
      break;

    case PROP_FILE_PATH:
      forger_scene_set_file_path (self, g_value_get_string (value));
      break;

    case PROP_UNITS:
      forger_scene_set_units (self, g_value_get_enum (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
forger_scene_class_init (ForgerSceneClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = forger_scene_finalize;
  object_class->get_property = forger_scene_get_property;
  object_class->set_property = forger_scene_set_property;

  properties [PROP_TITLE] =
    g_param_spec_string ("title", NULL, NULL,
                         _("Dummy"),
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties [PROP_LOCATION] =
    g_param_spec_string ("location", NULL, NULL,
                         _("Dummy"),
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties [PROP_FILE_PATH] =
    g_param_spec_string ("file-path", NULL, NULL,
                         _("Path"),
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties [PROP_UNITS] =
    g_param_spec_enum ("units", NULL, NULL,
                       FORGER_TYPE_MEASURING_SYSTEM, FORGER_UNITS_GENERIC,
                       (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
forger_scene_init (ForgerScene *self)
{
  self->root = FORGER_CONTAINER (forger_container_new (_("SCENE ROOT")));
}


/*
 * -----< CONSTRUCTORS >----- *
 */
/**
 * forger_scene_new:
 *
 * Create a new #ForgerScene object.
 *
 * Returns: (transfer full): a newly created #ForgerScene
 */
ForgerScene *
forger_scene_new (void)
{
  return g_object_new (FORGER_TYPE_SCENE, NULL);
}


/*
 * -----< PROPERTIES >----- *
 */
gchar *
forger_scene_get_title (ForgerScene *self)
{
  g_return_val_if_fail (FORGER_IS_SCENE (self), NULL);
  return self->title;
}

void
forger_scene_set_title (ForgerScene *self,
                        const gchar *setting)
{
  g_return_if_fail (FORGER_IS_SCENE (self));

  if (g_set_str (&self->title, setting))
    g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_TITLE]);
}

gchar *
forger_scene_get_location (ForgerScene *self)
{
  g_return_val_if_fail (FORGER_IS_SCENE (self), NULL);
  return self->location;
}

void
forger_scene_set_location (ForgerScene *self,
                           const gchar *setting)
{
  g_return_if_fail (FORGER_IS_SCENE (self));

  if (g_set_str (&self->location, setting))
    g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_LOCATION]);
}

gchar *
forger_scene_get_file_path (ForgerScene *self)
{
  g_return_val_if_fail (FORGER_IS_SCENE (self), NULL);
  return self->file_path;
}

void
forger_scene_set_file_path (ForgerScene *self,
                            const gchar *setting)
{
  g_return_if_fail (FORGER_IS_SCENE (self));

  if (g_set_str (&self->file_path, setting))
    g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_FILE_PATH]);
}

ForgerMeasuringSystem
forger_scene_get_units (ForgerScene *self)
{
  g_return_val_if_fail (FORGER_IS_SCENE (self), FORGER_UNITS_GENERIC);
  return self->units;
}

void
forger_scene_set_units (ForgerScene           *self,
                        ForgerMeasuringSystem  setting)
{
  g_return_if_fail (FORGER_IS_SCENE (self));

  if (self->units != setting)
    {
      self->units = setting;
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_UNITS]);
    }
}

/**
 * forger_scene_get_root:
 * @self: a #ForgerScene.
 *
 * Gets a pointer to the root node aka #ForgerContainer of %self.
 *
 * Returns: (transfer full): a #ForgerContainer object
 */
ForgerContainer *
forger_scene_get_root (ForgerScene *self)
{
  g_return_val_if_fail (FORGER_IS_SCENE (self), NULL);
  return self->root;
}
