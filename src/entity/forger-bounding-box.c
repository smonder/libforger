/*
 * forger-bounding-box.c
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "forger-bounding-box"

#include "config.h"
#include "forger-debug.h"

#include <glib/gi18n.h>

#include "renderer/forger-helper-render-node.h"

#include "forger-bounding-box.h"

struct _ForgerBoundingBox
{
  ForgerEntityComponent parent_instance;

  /* The bounding box which we will use as selection box */
  graphene_box_t     bounding_box;
  ForgerRenderNode * bounding_box_node;
};

G_DEFINE_FINAL_TYPE (ForgerBoundingBox, forger_bounding_box, FORGER_TYPE_ENTITY_COMPONENT)

enum {
  PROP_0,
  PROP_BOX,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];


/*
 * -----< FORGER_ENTITY_COMPONENT CLASS IMPLEMENTATION >----- *
 */
static const gchar *
forger_bounding_box_get_display_name (ForgerEntityComponent *component)
{
  return _("Bounding Box");
}

static const gchar *
forger_bounding_box_get_icon_name (ForgerEntityComponent *component)
{
  return "edit-select-all-symbolic";
}

static const gchar *
forger_bounding_box_get_description (ForgerEntityComponent *component)
{
  return _("The bounding box");
}

static void
forger_bounding_box_draw (ForgerEntityComponent *component)
{
  ForgerBoundingBox *self = FORGER_BOUNDING_BOX (component);

  graphene_point3d_t bbox_vertices[8];
  graphene_vec3_t bbox_vvec[8];

  graphene_box_get_vertices (&self->bounding_box, bbox_vvec);
  for (guint i = 0; i < 8; i++)
    {
      bbox_vertices[i].x = graphene_vec3_get_x (&bbox_vvec[i]);
      bbox_vertices[i].y = graphene_vec3_get_y (&bbox_vvec[i]);
      bbox_vertices[i].z = graphene_vec3_get_z (&bbox_vvec[i]);
    }

  forger_render_node_update_positions (self->bounding_box_node,
                                       bbox_vertices, 8);

  forger_render_node_draw (self->bounding_box_node);
}


/*
 * -----< G_OBJECT CLASS IMPLEMENTATION >----- *
 */
static void
forger_bounding_box_finalize (GObject *object)
{
  ForgerBoundingBox *self = FORGER_BOUNDING_BOX (object);

  g_clear_object (&self->bounding_box_node);

  G_OBJECT_CLASS (forger_bounding_box_parent_class)->finalize (object);
}

static void
forger_bounding_box_get_property (GObject    *object,
                                  guint       prop_id,
                                  GValue     *value,
                                  GParamSpec *pspec)
{
  ForgerBoundingBox *self = FORGER_BOUNDING_BOX (object);

  switch (prop_id)
    {
    case PROP_BOX:
      g_value_set_boxed (value, forger_bounding_box_get_box (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
forger_bounding_box_class_init (ForgerBoundingBoxClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  ForgerEntityComponentClass *fec_class = FORGER_ENTITY_COMPONENT_CLASS (klass);

  object_class->finalize = forger_bounding_box_finalize;
  object_class->get_property = forger_bounding_box_get_property;

  fec_class->get_display_name = forger_bounding_box_get_display_name;
  fec_class->get_icon_name = forger_bounding_box_get_icon_name;
  fec_class->get_description = forger_bounding_box_get_description;
  fec_class->draw = forger_bounding_box_draw;

  /**
   * ForgerBoundingBox:box:
   *
   * The "box" property is the underlying #graphene_box_t object which
   * all the operations of %self is affecting.
   *
   * Since: 0.1
   */
  properties [PROP_BOX] =
    g_param_spec_boxed ("box", NULL, NULL,
                        GRAPHENE_TYPE_BOX,
                        (G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
forger_bounding_box_init (ForgerBoundingBox *self)
{
  guint bounding_box_indices [36] =
    {
      2, 7, 6, 7, 1, 5,
      3, 0, 1, 4, 1, 0,
      6, 5, 4, 2, 4, 0,
      2, 3, 7, 7, 3, 1,
      3, 2, 0, 4, 5, 1,
      6, 7, 5, 2, 6, 4
    };

  self->bounding_box_node = forger_helper_render_node_new (36);
  forger_render_node_update_indices (self->bounding_box_node,
                                     bounding_box_indices,
                                     G_N_ELEMENTS (bounding_box_indices));
}


/*
 * -----< CONSTRUCTORS >----- *
 */
/**
 * forger_bounding_box_new:
 *
 * Create a new #ForgerBoundingBox
 *
 * Returns: (transfer full): a newly created #ForgerEntityComponent
 */
ForgerEntityComponent *
forger_bounding_box_new (void)
{
  return g_object_new (FORGER_TYPE_BOUNDING_BOX, NULL);
}


/*
 * -----< PROPERTIES >----- *
 */
/**
 * forger_bounding_box_get_box:
 * @self: a #ForgerBoundingBox
 *
 * Gets the "box" property
 *
 * Returns: (transfer none): a #graphene_box_t
 */
graphene_box_t *
forger_bounding_box_get_box (ForgerBoundingBox *self)
{
  g_return_val_if_fail (FORGER_IS_BOUNDING_BOX (self), NULL);
  return &self->bounding_box;
}


/*
 * -----< METHODS >----- *
 */
/**
 * forger_bounding_box_update_from_buffer:
 * @self: a #ForgerBoundingBox
 * @positions_buffer: (element-type Forger.Vertex): a #GArray with item types of
 *   #ForgerVertex: the buffer of positions to calculate the box from.
 *
 * Calculate the bounding box from a vertices' positions buffer data.
 *
 */
void
forger_bounding_box_update_from_buffer (ForgerBoundingBox *self,
                                        GArray            *positions_buffer)
{
  FORGER_ENTRY;
  graphene_point3d_t *positions_data;

  g_return_if_fail (FORGER_IS_BOUNDING_BOX (self));
  g_return_if_fail (positions_buffer->len >= 3);

  positions_data = g_try_new (graphene_point3d_t, positions_buffer->len);
  memcpy (positions_data, positions_buffer->data, positions_buffer->len * sizeof (graphene_point3d_t));

  graphene_box_init_from_points (&self->bounding_box,
                                 positions_buffer->len,
                                 positions_data);
  g_free (positions_data);

  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_BOX]);
  FORGER_EXIT;
}

/**
 * forger_bounding_box_ray_intersects:
 * @self: a #ForgerBoundingBox.
 * @model_matrix: a @graphene_matrix_t: the model transformation matrix.
 * @ray: a @graphene_ray_t: the ray to test the intersection with.
 *
 * Checks to see whether @ray intersects @self.
 *
 * Returns: %TRUE if @ray intersects @self, %FALSE otherwise.
 */
gboolean
forger_bounding_box_ray_intersects (ForgerBoundingBox *self,
                                    graphene_matrix_t *model_matrix,
                                    graphene_ray_t    *ray)
{
  graphene_box_t transformed_box;

  FORGER_ENTRY;
  g_return_val_if_fail (FORGER_IS_BOUNDING_BOX (self), FALSE);
  g_return_val_if_fail (model_matrix != NULL, FALSE);
  g_return_val_if_fail (ray != NULL, FALSE);

  /* transform box by entity->model_matrix */
  graphene_matrix_transform_box (model_matrix,
                                 &self->bounding_box, &transformed_box);

  FORGER_RETURN ( graphene_ray_intersects_box (ray, &transformed_box) );
}
