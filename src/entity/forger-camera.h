/* forger-camera.h
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#if !defined(FORGER_INSIDE) && !defined(LIBFORGER_COMPILATION)
#error "Only <libforger.h> can be included directly."
#endif

#include <gtk/gtk.h>
#include <graphene-gobject.h>

#include "forger-entity.h"
#include "forger-version-macros.h"

G_BEGIN_DECLS

#define FORGER_TYPE_CAMERA (forger_camera_get_type())

FORGER_AVAILABLE_IN_ALL
G_DECLARE_FINAL_TYPE (ForgerCamera, forger_camera, FORGER, CAMERA, ForgerEntity)


FORGER_AVAILABLE_IN_ALL
ForgerCamera *      forger_camera_new               (const gchar *   name,
                                                     const gchar *   description,
                                                     const gfloat    fov,
                                                     const gfloat    near_clip,
                                                     const gfloat    far_clip) G_GNUC_WARN_UNUSED_RESULT;
FORGER_AVAILABLE_IN_ALL
ForgerCamera *      forger_camera_new_default       (const gchar *   name,
                                                     const gchar *   description) G_GNUC_WARN_UNUSED_RESULT;

FORGER_AVAILABLE_IN_ALL
gchar *             forger_camera_get_description   (ForgerCamera *  self);
FORGER_AVAILABLE_IN_ALL
void                forger_camera_set_description   (ForgerCamera *  self,
                                                     const gchar *   desc);

FORGER_AVAILABLE_IN_ALL
graphene_matrix_t * forger_camera_get_vpmatrix      (ForgerCamera *  self);
FORGER_AVAILABLE_IN_ALL
graphene_matrix_t * forger_camera_get_pmatrix       (ForgerCamera *  self);
FORGER_AVAILABLE_IN_ALL
graphene_matrix_t * forger_camera_get_vmatrix       (ForgerCamera *  self);

FORGER_AVAILABLE_IN_ALL
GdkRectangle *      forger_camera_get_viewport      (ForgerCamera *  self);
FORGER_AVAILABLE_IN_ALL
void                forger_camera_set_viewport      (ForgerCamera *  self,
                                                     GdkRectangle *  viewport);

FORGER_AVAILABLE_IN_ALL
gboolean            forger_camera_is_orthographic   (ForgerCamera *  self);
FORGER_AVAILABLE_IN_ALL
void                forger_camera_set_orthographic  (ForgerCamera *  self,
                                                     gboolean        is_ortho);

FORGER_AVAILABLE_IN_ALL
void                forger_camera_handle_update     (ForgerCamera *  self,
                                                     guint           time_step);
FORGER_AVAILABLE_IN_ALL
gboolean            forger_camera_handle_zoom_event (ForgerCamera *            self,
                                                     GtkEventControllerScroll *scroll,
                                                     const gdouble             offset_x,
                                                     const gdouble             offset_y);
FORGER_AVAILABLE_IN_ALL
void                forger_camera_handle_pan_event  (ForgerCamera *            self,
                                                     GtkGestureDrag *          drag_gesture,
                                                     const gdouble             offset_x,
                                                     const gdouble             offset_y);


G_END_DECLS

