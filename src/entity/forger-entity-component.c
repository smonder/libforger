/*
 * forger-entity-component.c
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "forger-entity-component"

#include "config.h"
#include "forger-debug.h"

#include "forger-entity-component.h"

G_DEFINE_ABSTRACT_TYPE (ForgerEntityComponent, forger_entity_component, G_TYPE_OBJECT)

static void
forger_entity_component_class_init (ForgerEntityComponentClass *klass)
{
}

static void
forger_entity_component_init (ForgerEntityComponent *self)
{
}


/**
 * forger_entity_component_get_display_name:
 *
 * Gets the name of @self that is going to be displayed in GUI widgets
 *
 * Returns: (transfer full): A static string represents the name.
 */
const gchar *
forger_entity_component_get_display_name (ForgerEntityComponent *self)
{
  g_return_val_if_fail (FORGER_IS_ENTITY_COMPONENT (self), NULL);

  if (FORGER_ENTITY_COMPONENT_GET_CLASS (self)->get_display_name)
    return FORGER_ENTITY_COMPONENT_GET_CLASS (self)->get_display_name (self);

  return NULL;
}

/**
 * forger_entity_component_get_icon_name:
 *
 * Gets the name of the icon that is going to be displayed in GUI widgets
 * near the name of the component.
 * This is so helpful when we need to represent the component as a GUI widget.
 *
 * Returns: (transfer full): A static string represents the name of the icon.
 */
const gchar *
forger_entity_component_get_icon_name (ForgerEntityComponent *self)
{
  g_return_val_if_fail (FORGER_IS_ENTITY_COMPONENT (self), NULL);

  if (FORGER_ENTITY_COMPONENT_GET_CLASS (self)->get_icon_name)
    return FORGER_ENTITY_COMPONENT_GET_CLASS (self)->get_icon_name (self);

  return NULL;
}

/**
 * forger_entity_component_get_description:
 *
 * Gets a short description about the component.
 * This is so helpful for displaying tool-tip texts.
 *
 * Returns: (transfer full): A static string represents the description.
 */
const gchar *
forger_entity_component_get_description (ForgerEntityComponent *self)
{
  g_return_val_if_fail (FORGER_IS_ENTITY_COMPONENT (self), NULL);

  if (FORGER_ENTITY_COMPONENT_GET_CLASS (self)->get_description)
    return FORGER_ENTITY_COMPONENT_GET_CLASS (self)->get_description (self);

  return NULL;
}

/**
 * forger_entity_component_draw:
 *
 * This method determines how the component is going to be rendered in the canvas
 * if it has the ability for being so.
 *
 */
void
forger_entity_component_draw (ForgerEntityComponent *self)
{
  g_return_if_fail (FORGER_IS_ENTITY_COMPONENT (self));

  if (FORGER_ENTITY_COMPONENT_GET_CLASS (self)->draw)
    FORGER_ENTITY_COMPONENT_GET_CLASS (self)->draw (self);
}

/**
 * forger_entity_component_prepare:
 * @model_matrix: (out): a #graphene_matrix_t: a pointer to the model matrix.
 * @view_projection_matrix: (out): a #graphene_matrix_t: a pointer to the view_projection matrix.
 *
 * This method is going to be called before the draw call to prepare
 * any component that could affect the rendering process.
 *
 */
void
forger_entity_component_prepare (ForgerEntityComponent *self,
                                 graphene_matrix_t     *model_matrix,
                                 graphene_matrix_t     *view_projection_matrix)
{
  g_return_if_fail (FORGER_IS_ENTITY_COMPONENT (self));

  if (FORGER_ENTITY_COMPONENT_GET_CLASS (self)->prepare)
    FORGER_ENTITY_COMPONENT_GET_CLASS (self)->prepare (self,
                                                       model_matrix,
                                                       view_projection_matrix);
}
