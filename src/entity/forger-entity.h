/*
 * forger-entity.h
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#if !defined(FORGER_INSIDE) && !defined(LIBFORGER_COMPILATION)
#error "Only <libforger.h> can be included directly."
#endif

#include <graphene-gobject.h>

#include "forger-entity-component.h"
#include "forger-types.h"
#include "forger-version-macros.h"


G_BEGIN_DECLS

#define FORGER_TYPE_ENTITY (forger_entity_get_type())

FORGER_AVAILABLE_IN_ALL
G_DECLARE_DERIVABLE_TYPE (ForgerEntity, forger_entity, FORGER, ENTITY, GObject)

struct _ForgerEntityClass
{
  GObjectClass parent_class;

  /* A helper to call by the GUI pages to represent the entity */
  const gchar *(* get_icon_name)       (ForgerEntity *       entity);

  /* The draw call for each buffer in the entity: this is handled by
   * children according to the type of the entity.
   */
  void         (* update_buffers)      (ForgerEntity *       entity);

  /* A Helper for collision detection and mouse picker API */
  gboolean     (* ray_intersects)      (ForgerEntity *       entity,
                                        graphene_ray_t *     ray);

  /* A Helper for mouse picker API: return point index */
  gint         (* get_closeset_point)  (ForgerEntity *       entity,
                                        graphene_point3d_t * ray_hit,
                                        const float          ray_sensitivity);

  /*< private */
  gpointer _reserved[16];
};

FORGER_AVAILABLE_IN_ALL
const gchar *  forger_entity_get_icon_name      (ForgerEntity *          self);

FORGER_AVAILABLE_IN_ALL
void           forger_entity_update_buffers     (ForgerEntity *          self);
FORGER_AVAILABLE_IN_ALL
gboolean       forger_entity_ray_intersects     (ForgerEntity *          self,
                                                 graphene_ray_t *        ray);
FORGER_AVAILABLE_IN_ALL
gint           forger_entity_get_closeset_point (ForgerEntity *          self,
                                                 graphene_point3d_t *    ray_hit,
                                                 const float             ray_sensitivity);

FORGER_AVAILABLE_IN_ALL
gchar *        forger_entity_dup_name           (ForgerEntity *          self);
FORGER_AVAILABLE_IN_ALL
void           forger_entity_set_name           (ForgerEntity *          self,
                                                 const gchar *           name);

FORGER_AVAILABLE_IN_ALL
ForgerEntity * forger_entity_get_parent         (ForgerEntity *          self);

FORGER_AVAILABLE_IN_ALL
graphene_point3d_t * forger_entity_get_origin   (ForgerEntity *          self);
FORGER_AVAILABLE_IN_ALL
void           forger_entity_set_origin         (ForgerEntity *          self,
                                                 graphene_point3d_t *    setting);

FORGER_AVAILABLE_IN_ALL
gboolean       forger_entity_is_sensitive       (ForgerEntity *          self);
FORGER_AVAILABLE_IN_ALL
void           forger_entity_set_sensitive      (ForgerEntity *          self,
                                                 gboolean                sensitive);

FORGER_AVAILABLE_IN_ALL
gboolean       forger_entity_is_visible         (ForgerEntity *          self);
FORGER_AVAILABLE_IN_ALL
void           forger_entity_set_visible        (ForgerEntity *          self,
                                                 gboolean                visible);

FORGER_AVAILABLE_IN_ALL
ForgerEntityComponent * forger_entity_get       (ForgerEntity *          self,
                                                 ForgerComponentType     type_);
FORGER_AVAILABLE_IN_ALL
void                    forger_entity_set       (ForgerEntity *          self,
                                                 ForgerComponentType     type_,
                                                 ForgerEntityComponent * component);

G_END_DECLS

