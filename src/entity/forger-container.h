/*
 * forger-container.h
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#if !defined(FORGER_INSIDE) && !defined(LIBFORGER_COMPILATION)
#error "Only <libforger.h> can be included directly."
#endif

#include "forger-entity.h"
#include "forger-version-macros.h"

G_BEGIN_DECLS

#define FORGER_TYPE_CONTAINER (forger_container_get_type())

FORGER_AVAILABLE_IN_ALL
G_DECLARE_DERIVABLE_TYPE (ForgerContainer, forger_container, FORGER, CONTAINER, ForgerEntity)

struct _ForgerContainerClass
{
  ForgerEntityClass parent_class;

  /*< private */
  gpointer _reserved[16];
};

FORGER_AVAILABLE_IN_ALL
ForgerEntity *  forger_container_new                    (const gchar *     name) G_GNUC_WARN_UNUSED_RESULT;

FORGER_AVAILABLE_IN_ALL
const gchar *   forger_container_get_expanded_icon_name (ForgerContainer * self);
FORGER_AVAILABLE_IN_ALL
void            forger_container_set_expanded_icon_name (ForgerContainer * self,
                                                         const gchar *     setting);

FORGER_AVAILABLE_IN_ALL
void            forger_container_append_child           (ForgerContainer * self,
                                                         ForgerEntity *    child);
FORGER_AVAILABLE_IN_ALL
void            forger_container_prepend_child          (ForgerContainer * self,
                                                         ForgerEntity *    child);
FORGER_AVAILABLE_IN_ALL
void            forger_container_insert_child           (ForgerContainer * self,
                                                         const gint        index_,
                                                         ForgerEntity *    child);
FORGER_AVAILABLE_IN_ALL
void            forger_container_remove_child           (ForgerContainer * self,
                                                         ForgerEntity *    child);
FORGER_AVAILABLE_IN_ALL
ForgerEntity *  forger_container_peek_nth_child         (ForgerContainer * self,
                                                         const guint       index_);

FORGER_AVAILABLE_IN_ALL
gboolean        forger_container_contains               (ForgerContainer * self,
                                                         ForgerEntity *    child);
FORGER_AVAILABLE_IN_ALL
void            forger_container_move_child             (ForgerContainer * self,
                                                         ForgerEntity *    child,
                                                         ForgerContainer * dest);
FORGER_AVAILABLE_IN_ALL
void            forger_container_copy_child             (ForgerContainer * self,
                                                         ForgerEntity *    child,
                                                         ForgerContainer * dest);

FORGER_AVAILABLE_IN_ALL
void            forger_container_traverse               (ForgerContainer *        self,
                                                         GTraverseType            traverse_type,
                                                         GTraverseFlags           traverse_flags,
                                                         gint                     max_depth,
                                                         ForgerSceneTraverseFunc  traverse_func,
                                                         gpointer                 user_data);

G_END_DECLS
