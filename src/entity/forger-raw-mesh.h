/*
 * forger-raw-mesh.h
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#if !defined(FORGER_INSIDE) && !defined(LIBFORGER_COMPILATION)
#error "Only <libforger.h> can be included directly."
#endif

#include "forger-entity.h"
#include "forger-vertex.h"
#include "forger-types.h"
#include "forger-version-macros.h"

G_BEGIN_DECLS

#define FORGER_TYPE_RAW_MESH (forger_raw_mesh_get_type())

FORGER_AVAILABLE_IN_ALL
G_DECLARE_FINAL_TYPE (ForgerRawMesh, forger_raw_mesh, FORGER, RAW_MESH, ForgerEntity)

FORGER_AVAILABLE_IN_ALL
ForgerEntity * forger_raw_mesh_new             (const gchar *     name) G_GNUC_WARN_UNUSED_RESULT;

FORGER_AVAILABLE_IN_ALL
guint          forger_raw_mesh_get_n_vertices  (ForgerRawMesh *   self);


FORGER_AVAILABLE_IN_ALL
void           forger_raw_mesh_push_vertex     (ForgerRawMesh *   self,
                                                ForgerVertex *    vert);
FORGER_AVAILABLE_IN_ALL
void           forger_raw_mesh_push_n_vertices (ForgerRawMesh *   self,
                                                const guint       n_vertex,
                                                gpointer          vertices);

FORGER_AVAILABLE_IN_ALL
void           forger_raw_mesh_remove_vertex   (ForgerRawMesh *   self,
                                                const guint       index_);

FORGER_AVAILABLE_IN_ALL
void           forger_raw_mesh_push_index      (ForgerRawMesh *   self,
                                                const guint       index_);
FORGER_AVAILABLE_IN_ALL
void           forger_raw_mesh_push_n_index    (ForgerRawMesh *   self,
                                                const guint       n_indices,
                                                guint *           indices);
FORGER_AVAILABLE_IN_ALL
void           forger_raw_mesh_remove_index    (ForgerRawMesh *   self,
                                                const guint       loc);

G_END_DECLS
