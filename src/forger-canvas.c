/*
 * forger-canvas.c
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
#define G_LOG_DOMAIN "forger-canvas"
#define MIN_ASPECT_RATIO 0.8f
#define MAX_ASPECT_RATIO 3.0f

#include "config.h"
#include "forger-debug.h"

#include "forger-canvas.h"
#include "renderer/forger-engine.h"

struct _ForgerCanvas
{
  GtkGLArea parent_instance;

  ForgerContext * context;
  ForgerEngine * engine;
  GdkGLContext * gl_context;

  /* Visual properties */
  GdkRGBA bgcolor;

  /* Used by tick function */
  GtkLabel * fps_label;
  gint64     first_frame_time;
  guint      tick;

  /* GdkPaintable */
  GdkPaintable * paintable;
  double         cached_aspect_ratio;
};

static void paintable_iface_init (GdkPaintableInterface *iface);
G_DEFINE_FINAL_TYPE_WITH_CODE (ForgerCanvas, forger_canvas, GTK_TYPE_GL_AREA,
                               G_IMPLEMENT_INTERFACE (GDK_TYPE_PAINTABLE, paintable_iface_init))

enum {
  PROP_0,
  PROP_GL_CONTEXT,
  PROP_FORGER_CONTEXT,
  PROP_BGCOLOR,
  PROP_BGCOLOR_NAME,
  PROP_PAINTABLE,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];
static GdkPaintableInterface *parent_paintable_iface;


/*
 * -----< FORGER_CANVAS CONSTRUCT PROPERTIES SETTERS >----- *
 */
static void
forger_canvas_set_gl_context (ForgerCanvas *self,
                              GdkGLContext *context)
{
  g_return_if_fail (FORGER_IS_CANVAS (self));
  g_return_if_fail (!context || GDK_IS_GL_CONTEXT (context));

  if (g_set_object (&self->gl_context, context))
    g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_GL_CONTEXT]);
}


/*
 * -----< SIGNAL HANDLERS >----- *
 */
static gboolean
forger_canvas_update (GtkWidget     *widget,
                      GdkFrameClock *frame_clock,
                      gpointer       user_data)
{
  ForgerCanvas *self = (ForgerCanvas *)widget;
  GdkFrameTimings *previous_timings;
  gint64 previous_frame_time;
  gint64 frame_time;
  gint64 history_start, history_len;
  gint64 frame;
  char *s;

  g_assert (FORGER_IS_CANVAS (self));

  frame = gdk_frame_clock_get_frame_counter (frame_clock);
  frame_time = gdk_frame_clock_get_frame_time (frame_clock);

  if (self->first_frame_time == 0)
    {
      /* No need for changes on first frame */
      self->first_frame_time = frame_time;
      if (self->fps_label)
        gtk_label_set_label (self->fps_label, "FPS: ---");
      return G_SOURCE_CONTINUE;
    }

  forger_context_handle_event_update (self->context, frame_time);
  gtk_widget_queue_draw (widget);

  history_start = gdk_frame_clock_get_history_start (frame_clock);

  if (self->fps_label && frame % 60 == 0)
    {
      history_len = frame - history_start;
      if (history_len > 0)
        {
          previous_timings = gdk_frame_clock_get_timings (frame_clock, frame - history_len);
          previous_frame_time = gdk_frame_timings_get_frame_time (previous_timings);

          s = g_strdup_printf ("FPS: %-4.1f", (G_USEC_PER_SEC * history_len) / (double)(frame_time - previous_frame_time));
          gtk_label_set_label (self->fps_label, s);
          g_free (s);
        }
    }

  return G_SOURCE_CONTINUE;
}


/*
 * -----< GDK_PAINTABLE INTERFACE IMPLEMENTATION >----- *
 */
static GdkPaintable *
forger_canvas_get_current_image (GdkPaintable *paintable)
{
  ForgerCanvas *self = (ForgerCanvas *) paintable;
  GtkSnapshot *snapshot = gtk_snapshot_new ();
  gint width, height;

  g_assert (FORGER_IS_CANVAS (self));

  width = gtk_widget_get_width (GTK_WIDGET (self));
  height = gtk_widget_get_height (GTK_WIDGET (self));

  gdk_paintable_snapshot (paintable, GDK_SNAPSHOT (snapshot), width, height);

  return gtk_snapshot_free_to_paintable (snapshot,
                                         &GRAPHENE_SIZE_INIT (width, height));
}

static double
get_unclamped_aspect_ratio (ForgerCanvas *self)
{
  if (!self->paintable)
    return self->cached_aspect_ratio;

  return gdk_paintable_get_intrinsic_aspect_ratio (self->paintable);
}

static double
forger_canvas_get_intrinsic_aspect_ratio (GdkPaintable *paintable)
{
  ForgerCanvas *self = (ForgerCanvas *) paintable;
  double ratio = get_unclamped_aspect_ratio (self);

  g_assert (FORGER_IS_CANVAS (self));

  return CLAMP (ratio, MIN_ASPECT_RATIO, MAX_ASPECT_RATIO);
}

static void
snapshot_paintable (GtkSnapshot  *snapshot,
                    double        width,
                    double        height,
                    GdkPaintable *paintable,
                    double        paintable_ratio,
                    double        xalign,
                    double        yalign)
{
  double snapshot_ratio = width / height;

  if (paintable_ratio > snapshot_ratio) {
    double new_width = height * paintable_ratio;

    gtk_snapshot_translate (snapshot,
                            &GRAPHENE_POINT_INIT ((float) (width - new_width) * xalign, 0));

    width = new_width;
  } else if (paintable_ratio < snapshot_ratio) {
    double new_height = width / paintable_ratio;

    gtk_snapshot_translate (snapshot,
                            &GRAPHENE_POINT_INIT (0, (float) (height - new_height) * yalign));

    height = new_height;
  }

  gdk_paintable_snapshot (paintable, snapshot, width, height);
}

static void
forger_canvas_snapshot (GdkPaintable *paintable,
                        GdkSnapshot  *snapshot,
                        double        width,
                        double        height)
{
  ForgerCanvas *self = (ForgerCanvas *) paintable;
  gdouble xalign = 0.0f, yalign = 0.0f;

  g_assert (FORGER_IS_CANVAS (self));

  if (self->paintable) {
    snapshot_paintable (GTK_SNAPSHOT (snapshot), width, height,
                        self->paintable, self->cached_aspect_ratio,
                        xalign, yalign);
    return;
  }

  gtk_snapshot_append_color (GTK_SNAPSHOT (snapshot), &self->bgcolor,
                             &GRAPHENE_RECT_INIT (0, 0, width, height));
}

static void
paintable_iface_init (GdkPaintableInterface *iface)
{
  parent_paintable_iface = g_type_interface_peek_parent (iface);

  iface->get_current_image = forger_canvas_get_current_image;
  iface->get_intrinsic_aspect_ratio = forger_canvas_get_intrinsic_aspect_ratio;
  iface->snapshot = forger_canvas_snapshot;
}


/*
 * -----< GTK_GLAREA CLASS IMPLEMENTATION >----- *
 */
static void
forger_canvas_resize (GtkGLArea *gl_area,
                      gint       width,
                      gint       height)
{
  ForgerCanvas *self = (ForgerCanvas *)gl_area;
  GdkRectangle viewport;

  g_assert (FORGER_IS_CANVAS (self));
  g_assert (GTK_IS_GL_AREA (gl_area));

  viewport.x = 0;
  viewport.y = 0;
  viewport.width = width;
  viewport.height = height;

  forger_engine_update_viewport (self->engine, &viewport);
}

static gboolean
forger_canvas_render (GtkGLArea    *gl_area,
                      GdkGLContext *context)
{
  ForgerCanvas *self = (ForgerCanvas *)gl_area;

  g_assert (FORGER_IS_CANVAS (self));
  g_assert (GTK_IS_GL_AREA (gl_area));
  g_assert (GDK_IS_GL_CONTEXT (context));

  if (gtk_gl_area_get_error (gl_area) != NULL)
    return FALSE;

  forger_engine_clear (self->engine, &self->bgcolor);
  forger_engine_render (self->engine);
  forger_engine_flush (self->engine);

  return TRUE;
}

static GdkGLContext *
forger_canvas_create_context (GtkGLArea *gl_area)
{
  ForgerCanvas *self = (ForgerCanvas *)gl_area;
  GtkWidget *widget = GTK_WIDGET (gl_area);
  GError *error = NULL;

  g_assert (FORGER_IS_CANVAS (self));
  g_assert (GTK_IS_GL_AREA (gl_area));

  FORGER_ENTRY;

  if (self->gl_context == NULL)
    {
      g_warning ("Creating a default GL context");

      self->gl_context = gdk_surface_create_gl_context (gtk_native_get_surface (gtk_widget_get_native (widget)), &error);
      if (error != NULL)
        {
          gtk_gl_area_set_error (gl_area, error);
          g_clear_object (&self->gl_context);
          g_clear_error (&error);
          FORGER_RETURN (NULL);
        }
    }

  gdk_gl_context_set_allowed_apis (self->gl_context, GDK_GL_API_GL);
  gdk_gl_context_set_required_version (self->gl_context,
                                       OPENGL_VERSION_MAJOR_REQUIRED,
                                       OPENGL_VERSION_MINOR_REQUIRED);
  gdk_gl_context_set_forward_compatible (self->gl_context, FALSE);

#ifdef ENABLE_TRACING
  gdk_gl_context_set_debug_enabled (self->gl_context, TRUE);
#endif

  gdk_gl_context_realize (self->gl_context, &error);
  if (error != NULL)
    {
      gtk_gl_area_set_error (gl_area, error);
      g_clear_object (&self->gl_context);
      g_clear_error (&error);
      FORGER_RETURN (NULL);
    }

  FORGER_RETURN (self->gl_context);
}


/*
 * -----< GTK_WIDGET CLASS IMPLEMENTATION >----- *
 */
static void
forger_canvas_realize (GtkWidget *widget)
{
  ForgerCanvas *self = (ForgerCanvas *)widget;
  GtkGLArea *gl_area = (GtkGLArea *)widget;

  g_assert (FORGER_IS_CANVAS (self));
  g_assert (GTK_IS_GL_AREA (gl_area));

  GTK_WIDGET_CLASS (forger_canvas_parent_class)->realize (widget);

  /* We need to make the context current if we want to call GL API */
  gtk_gl_area_make_current (gl_area);

  /* If there were errors during the initialization or when trying to make
   * the context current, this function will return a GError for you to catch.
   */
  if (gtk_gl_area_get_error (gl_area) != NULL)
    return;

  forger_engine_prepare (self->engine,
                         gtk_gl_area_get_context (gl_area));
}

static void
forger_canvas_unrealize (GtkWidget *widget)
{
  ForgerCanvas *self = (ForgerCanvas *)widget;
  GtkGLArea *gl_area = (GtkGLArea *)widget;

  g_assert (FORGER_IS_CANVAS (self));
  g_assert (GTK_IS_GL_AREA (gl_area));

  if (gtk_gl_area_get_error (GTK_GL_AREA (widget)) != NULL)
    return;

  g_clear_object (&self->engine);

  GTK_WIDGET_CLASS (forger_canvas_parent_class)->unrealize (widget);
}


/*
 * -----< G_OBJECT CLASS IMPLEMENTATION >----- *
 */
static void
forger_canvas_finalize (GObject *object)
{
  ForgerCanvas *self = (ForgerCanvas *)object;

  gtk_widget_remove_tick_callback (GTK_WIDGET (self), self->tick);

  g_clear_pointer ((GtkWidget **)&self->fps_label, gtk_widget_unparent);
  g_clear_object (&self->gl_context);
  g_clear_object (&self->context);

  G_OBJECT_CLASS (forger_canvas_parent_class)->finalize (object);
}

static void
forger_canvas_get_property (GObject    *object,
                            guint       prop_id,
                            GValue     *value,
                            GParamSpec *pspec)
{
  ForgerCanvas *self = FORGER_CANVAS (object);

  switch (prop_id)
    {
    case PROP_FORGER_CONTEXT:
      g_value_set_object (value, forger_canvas_get_context (self));
      break;

    case PROP_BGCOLOR:
      g_value_set_boxed (value, forger_canvas_get_bgcolor (self));
      break;

    case PROP_PAINTABLE:
      g_value_set_object (value, forger_canvas_get_paintable (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
forger_canvas_set_property (GObject      *object,
                            guint         prop_id,
                            const GValue *value,
                            GParamSpec   *pspec)
{
  ForgerCanvas *self = FORGER_CANVAS (object);

  switch (prop_id)
    {
    case PROP_GL_CONTEXT:
      forger_canvas_set_gl_context (self, g_value_get_object (value));
      break;

    case PROP_FORGER_CONTEXT:
      forger_canvas_set_context (self, g_value_get_object (value));
      break;

    case PROP_BGCOLOR:
      forger_canvas_set_bgcolor (self, g_value_get_boxed (value));
      break;

    case PROP_BGCOLOR_NAME:
      forger_canvas_set_bgcolor_name (self, g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
forger_canvas_class_init (ForgerCanvasClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);
  GtkGLAreaClass *glarea_class = GTK_GL_AREA_CLASS (klass);

  object_class->finalize = forger_canvas_finalize;
  object_class->get_property = forger_canvas_get_property;
  object_class->set_property = forger_canvas_set_property;

  widget_class->realize = forger_canvas_realize;
  widget_class->unrealize = forger_canvas_unrealize;

  glarea_class->render = forger_canvas_render;
  glarea_class->resize = forger_canvas_resize;
  glarea_class->create_context = forger_canvas_create_context;

  /**
   * ForgerCanvas:gl-context:
   *
   * The "gl-context" property is the #GdkGLContext used by the #ForgerCanvas widget.
   *
   * The #ForgerCanvas widget is responsible for creating the #GdkGLContext instance
   * in case the user didn't provide any by this property.
   *
   * Since: 0.1
   */
  properties [PROP_GL_CONTEXT] =
    g_param_spec_object ("gl-context", NULL, NULL,
                         GDK_TYPE_GL_CONTEXT,
                         (G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS));

  /**
   * ForgerCanvas:forger-context:
   *
   * The "forger-context" property is the #ForgerContext used by the #ForgerCanvas widget.
   *
   * Since: 0.1
   */
  properties [PROP_FORGER_CONTEXT] =
    g_param_spec_object ("forger-context", NULL, NULL,
                         FORGER_TYPE_CONTEXT,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  /**
   * ForgerCanvas:background-color:
   *
   * The "background-color" property is the #GdkRGBA color used in the
   * background of #ForgerCanvas.
   *
   * Since: 0.1
   */
  properties [PROP_BGCOLOR] =
    g_param_spec_boxed ("background-color", NULL, NULL,
                        GDK_TYPE_RGBA,
                        (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  /**
   * ForgerCanvas:background-color-name:
   *
   * The "background-color-name" property is the same as "background-color"
   * property, however the color is provided in string format.
   *
   * Since: 0.1
   */
  properties [PROP_BGCOLOR_NAME] =
    g_param_spec_string ("background-color-name", NULL, NULL,
                         NULL,
                         (G_PARAM_WRITABLE | G_PARAM_STATIC_STRINGS));

  /**
   * ForgerCanvas:paintable:
   *
   * The "paintable" property is the current image of the #ForgerCanvas widget.
   * This is highly used in thumbnails.
   *
   * Since: 0.1
   */
  properties [PROP_PAINTABLE] =
    g_param_spec_object ("paintable", NULL, NULL,
                         GDK_TYPE_PAINTABLE,
                         (G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
forger_canvas_init (ForgerCanvas *self)
{
  gtk_gl_area_set_allowed_apis (GTK_GL_AREA (self), GDK_GL_API_GL);
  gtk_gl_area_set_has_depth_buffer (GTK_GL_AREA (self), FALSE);
  gtk_gl_area_set_auto_render (GTK_GL_AREA (self), TRUE);

  gtk_widget_set_hexpand (GTK_WIDGET (self), TRUE);
  gtk_widget_set_vexpand (GTK_WIDGET (self), TRUE);
  gtk_widget_set_focusable (GTK_WIDGET (self), TRUE);
  gtk_widget_set_focus_on_click (GTK_WIDGET (self), TRUE);
  gtk_widget_set_can_focus (GTK_WIDGET (self), TRUE);

  self->gl_context = NULL;
  self->engine = forger_engine_new ();
  self->tick = gtk_widget_add_tick_callback (GTK_WIDGET (self),
                                             forger_canvas_update,
                                             NULL, NULL);
}


/*
 * -----< CONSTRUCTORS >----- *
 */
/**
 * forger_canvas_new:
 *
 * Creates a new #ForgerCanvas.
 *
 * Returns: (transfer full): a newly created #GtkWidget.
 */
GtkWidget *
forger_canvas_new (void)
{
  return g_object_new (FORGER_TYPE_CANVAS, NULL);
}

/**
 * forger_canvas_new_for_gl_context:
 * @gl_context: (nullable): a #GdkGLContext.
 *
 * Creates a new #ForgerCanvas and set its "gl-context" property
 * to @gl_context.
 * if %NULL is provided for @gl_context, he canvas is going to
 * create a default one.
 * %gl_context should not necessarily be realized.
 *
 * Returns: (transfer full): a newly created #GtkWidget.
 */
GtkWidget *
forger_canvas_new_for_gl_context (GdkGLContext *gl_context)
{
  return g_object_new (FORGER_TYPE_CANVAS,
                       "gl-context", gl_context, NULL);
}


/*
 * -----< PROPERTIES >----- *
 */
/**
 * forger_canvas_get_context:
 * @self: a #ForgerCanvas.
 *
 * Gets the "forger-context" property.
 *
 * Returns: (transfer full): a #ForgerContext or %NULL.
 */
ForgerContext *
forger_canvas_get_context (ForgerCanvas *self)
{
  g_return_val_if_fail (FORGER_IS_CANVAS (self), NULL);
  return self->context;
}

/**
 * forger_canvas_set_context:
 * @self: a #ForgerCanvas.
 * @context: a #ForgerContext
 *
 * Sets the "forger-context" property.
 *
 */
void
forger_canvas_set_context (ForgerCanvas  *self,
                           ForgerContext *context)
{
  FORGER_ENTRY;
  g_return_if_fail (FORGER_IS_CANVAS (self));
  g_return_if_fail (FORGER_IS_CONTEXT (context));

  if (g_set_object (&self->context, context))
    {
      forger_engine_set_context (self->engine, self->context);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_FORGER_CONTEXT]);
    }

  FORGER_EXIT;
}

/**
 * forger_canvas_get_bgcolor:
 * @self: a #ForgerCanvas.
 *
 * Gets the background color property of @self.
 *
 * Returns: (transfer full): a #GdkRGBA or %NULL.
 */
GdkRGBA *
forger_canvas_get_bgcolor (ForgerCanvas *self)
{
  g_return_val_if_fail (FORGER_IS_CANVAS (self), NULL);
  return &self->bgcolor;
}

/**
 * forger_canvas_set_bgcolor:
 * @self: a #ForgerCanvas.
 * @color: a #GdkRGBA.
 *
 * Sets the background color property of @self.
 *
 */
void
forger_canvas_set_bgcolor (ForgerCanvas *self,
                           GdkRGBA      *color)
{
  FORGER_ENTRY;
  g_return_if_fail (FORGER_IS_CANVAS (self));
  g_return_if_fail (color);

  if (!gdk_rgba_equal (color, &self->bgcolor))
    {
      self->bgcolor.red = color->red;
      self->bgcolor.green = color->green;
      self->bgcolor.blue = color->blue;
      self->bgcolor.alpha = color->alpha;

      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_BGCOLOR]);
    }
  FORGER_EXIT;
}

/**
 * forger_canvas_set_bgcolor_name:
 * @self: a #ForgerCanvas.
 * @color: a #gchar: the color in text format.
 *
 * Sets the background color property of @self in text format.
 *
 */
void
forger_canvas_set_bgcolor_name (ForgerCanvas *self,
                                const gchar  *color)
{
  GdkRGBA g_color;

  FORGER_ENTRY;
  g_return_if_fail (FORGER_IS_CANVAS (self));

  if (gdk_rgba_parse (&g_color, color))
    forger_canvas_set_bgcolor (self, &g_color);
  else
    g_critical ("Failed to setup the Background color of the canvas. Parsing color failed!");

  FORGER_EXIT;
}

/**
 * forger_canvas_get_paintable:
 * @self: a #ForgerCanvas
 *
 * Gets the "paintable" property.
 *
 * Returns: (transfer full): a #GdkPaintable
 */
GdkPaintable *
forger_canvas_get_paintable (ForgerCanvas *self)
{
  g_return_val_if_fail (FORGER_IS_CANVAS (self), NULL);

  if (!self->paintable)
    self->paintable = gtk_widget_paintable_new (GTK_WIDGET (self));

  return self->paintable;
}
