/*
 * forger-context.c
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
#define G_LOG_DOMAIN "forger-context"

#include "config.h"
#include "forger-debug.h"

#include "forger-context.h"
#include "operations/forger-operation-private.h"

struct _ForgerContext
{
  GObject parent_instance;

  /* Each context object should have at least one ForgerScene component */
  ForgerScene * scene;

  GQueue  selected_entities;

  /* The active entity property is a weak reference to the head of the selected
   * entities queue
   */
  ForgerEntity *       active_entity;

  ForgerCamera *       active_camera;
  ForgerOperation *    operation;
};

G_DEFINE_FINAL_TYPE (ForgerContext, forger_context, G_TYPE_OBJECT)

enum {
  PROP_0,
  PROP_SCENE,
  PROP_ACTIVE_ENTITY,
  PROP_ACTIVE_CAMERA,
  PROP_OPERATION,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

/*
 * -----< G_OBJECT CLASS IMPLEMENTATION >----- *
 */
static void
forger_context_dispose (GObject *object)
{
  ForgerContext *self = (ForgerContext *)object;

  g_clear_weak_pointer (&self->operation);

  G_OBJECT_CLASS (forger_context_parent_class)->dispose (object);
}

static void
forger_context_finalize (GObject *object)
{
  ForgerContext *self = (ForgerContext *)object;

  g_clear_weak_pointer (&self->active_camera);
  g_clear_weak_pointer (&self->active_entity);

  G_OBJECT_CLASS (forger_context_parent_class)->finalize (object);
}

static void
forger_context_get_property (GObject    *object,
                             guint       prop_id,
                             GValue     *value,
                             GParamSpec *pspec)
{
  ForgerContext *self = FORGER_CONTEXT (object);

  switch (prop_id)
    {
    case PROP_SCENE:
      g_value_set_object (value, forger_context_get_scene (self));
      break;

    case PROP_ACTIVE_ENTITY:
      g_value_set_object (value, forger_context_get_active_entity (self));
      break;

    case PROP_OPERATION:
      g_value_set_object (value, forger_context_get_operation (self));
      break;

    case PROP_ACTIVE_CAMERA:
      g_value_set_object (value, forger_context_get_active_camera (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
forger_context_set_property (GObject      *object,
                             guint         prop_id,
                             const GValue *value,
                             GParamSpec   *pspec)
{
  ForgerContext *self = FORGER_CONTEXT (object);

  switch (prop_id)
    {
    case PROP_ACTIVE_ENTITY:
      forger_context_select_entity (self, g_value_get_object (value));
      break;

    case PROP_OPERATION:
      forger_context_bind_operation (self, g_value_get_object (value));
      break;

    case PROP_ACTIVE_CAMERA:
      forger_context_bind_camera (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
forger_context_class_init (ForgerContextClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->dispose = forger_context_dispose;
  object_class->finalize = forger_context_finalize;
  object_class->get_property = forger_context_get_property;
  object_class->set_property = forger_context_set_property;

  /**
   * ForgerContext:scene:
   *
   * The "scene" property is the #ForgerScene that %self is created for.
   *
   * Since: 0.1
   */
  properties [PROP_SCENE] =
    g_param_spec_object ("scene", NULL, NULL,
                         FORGER_TYPE_SCENE,
                         (G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));

  /**
   * ForgerContext:active-entity:
   *
   * The "active-entity" property is the #ForgerEntity that is
   * currently selected in %self.
   *
   * Since: 0.1
   */
  properties [PROP_ACTIVE_ENTITY] =
    g_param_spec_object ("active-entity", NULL, NULL,
                         FORGER_TYPE_ENTITY,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY |
                          G_PARAM_STATIC_STRINGS));

  /**
   * ForgerContext:active-camera:
   *
   * The "active-camera" property is the #ForgerCamera that is
   * currently bound in %self.
   *
   * Since: 0.1
   */
  properties [PROP_ACTIVE_CAMERA] =
    g_param_spec_object ("active-camera",
                         "Active Camera",
                         "The currently active ForgerCamera.",
                         FORGER_TYPE_CAMERA,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  /**
   * ForgerContext:operation:
   *
   * The "operation" property is the #ForgerOperation that is
   * currently bound in %self.
   *
   * Since: 0.1
   */
  properties [PROP_OPERATION] =
    g_param_spec_object ("operation",
                         "Operation",
                         "The current active operation.",
                         FORGER_TYPE_OPERATION,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
forger_context_init (ForgerContext *self)
{
}


/*
 * -----< CONSTRUCTORS >----- *
 */
/**
 * forger_context_new:
 *
 * Creates a new #ForgerContext object.
 *
 * Returns: (transfer full): a newly created #ForgerContext
 */
ForgerContext *
forger_context_new (void)
{
  g_autoptr(ForgerContext) self = NULL;
  self = g_object_new (FORGER_TYPE_CONTEXT, NULL);
  self->scene = forger_scene_new ();
  return g_steal_pointer (&self);
}

/**
 * forger_context_new_for_scene:
 * @scene: a #ForgerScene.
 *
 * Creates a new #ForgerContext object for the @scene.
 * If you want to create a new context with a new scene, you can use
 * forger_context_new() instead.
 *
 * Returns: (transfer full) (nullable): a newly created #ForgerContext
 */
ForgerContext *
forger_context_new_for_scene (ForgerScene *scene)
{
  g_autoptr(ForgerContext) self = NULL;

  g_return_val_if_fail (FORGER_IS_SCENE (scene), NULL);

  self = g_object_new (FORGER_TYPE_CONTEXT, NULL);
  g_set_object (&self->scene, scene);

  return g_steal_pointer (&self);
}


/*
 * -----< PROPERTIES >----- *
 */
/**
 * forger_context_get_scene:
 *
 * Get the "scene" property value.
 *
 * Returns: (transfer full): a #ForgerScene or %NULL
 */
ForgerScene *
forger_context_get_scene (ForgerContext *self)
{
  g_return_val_if_fail (FORGER_IS_CONTEXT (self), NULL);
  return self->scene;
}

/**
 * forger_context_get_active_entity:
 * @self: a #ForgerContext
 *
 * Get the currently active (selected) entity.
 *
 * Returns: (transfer full) (nullable): a #ForgerEntity or %NULL
 */
ForgerEntity *
forger_context_get_active_entity (ForgerContext * self)
{
  g_return_val_if_fail (FORGER_IS_CONTEXT (self), NULL);
  return self->active_entity;
}

/**
 * forger_context_foreach_selected_entities:
 * @self: a #ForgerContext
 * @func_: (scope call): a #GFunc callback
 * @user_data: a pointer to data to pass to the callback.
 *
 * Calls %func_ on each selected entity.
 *
 */
void
forger_context_foreach_selected_entities (ForgerContext *self,
                                          GFunc          func_,
                                          gpointer       user_data)
{
  g_return_if_fail (FORGER_IS_CONTEXT (self));
  g_queue_foreach (&self->selected_entities, func_, user_data);
}

/**
 * forger_context_is_entity_selected:
 * @self: a #ForgerContext
 * @entity: a #ForgerEntity
 *
 * Checks to see whether %entity is selected by searching for entity
 * in the selected entities queue
 *
 * Returns: %TRUE if entity is selected, %FALSE otherwise
 */
gboolean
forger_context_is_entity_selected (ForgerContext *self,
                                   ForgerEntity  *entity)
{
  g_return_val_if_fail (FORGER_IS_CONTEXT (self), FALSE);
  g_return_val_if_fail (FORGER_IS_ENTITY (entity), FALSE);

  return g_queue_find (&self->selected_entities, entity) != NULL;
}

/**
 * forger_context_select_entity:
 * @self: a #ForgerContext
 * @entity: a #ForgerEntity or %NULL to clear the selection
 *
 * Marks @entity as selected, thus the engine will draw a highlight over it.
 * also tools and other operations may work on it.
 */
void
forger_context_select_entity (ForgerContext *self,
                              ForgerEntity  *entity)
{
  g_return_if_fail (FORGER_IS_CONTEXT (self));
  g_return_if_fail (!entity || FORGER_IS_ENTITY (entity));

  g_queue_push_head (&self->selected_entities, g_object_ref (entity));

  g_set_weak_pointer (&self->active_entity, g_queue_peek_head (&self->selected_entities));
  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_ACTIVE_ENTITY]);
}

/**
 * forger_context_deselect_entity:
 * @self: a #ForgerContext
 * @entity: a #ForgerEntity or %NULL to clear the selection
 *
 * De-selects @entity and remove it from selection queue.
 */
void
forger_context_deselect_entity (ForgerContext *self,
                                ForgerEntity  *entity)
{
  g_return_if_fail (FORGER_IS_CONTEXT (self));
  g_return_if_fail (FORGER_IS_ENTITY (entity));

  if (!forger_context_is_entity_selected (self, entity))
    return;

  if (g_queue_remove (&self->selected_entities, entity))
    {
      g_set_weak_pointer (&self->active_entity, g_queue_peek_head (&self->selected_entities));
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_ACTIVE_ENTITY]);
    }
}

/**
 * forger_context_deselect_all:
 * @self: a #ForgerContext
 *
 * De-selects all selected entities and clear the selection queue.
 */
void
forger_context_deselect_all (ForgerContext *self)
{
  g_return_if_fail (FORGER_IS_CONTEXT (self));

  g_queue_clear (&self->selected_entities);
  g_clear_weak_pointer (&self->active_entity);
}

/**
 * forger_context_get_active_camera:
 * @self: a #ForgerContext.
 *
 * Gets the currently active camera.
 *
 * Returns: (transfer full): a #ForgerCamera or %NULL.
 *
 * Since: 0.1
 */
ForgerCamera *
forger_context_get_active_camera (ForgerContext *self)
{
  g_return_val_if_fail (FORGER_IS_CONTEXT (self), NULL);
  return self->active_camera;
}

/**
 * forger_context_bind_camera:
 * @self: a #ForgerContext.
 * @camera: a #ForgerCamera.
 *
 * Sets the currently active camera to be @camera.
 * @self doesn't take ownership of @camera.
 *
 * Since: 0.1
 */
void
forger_context_bind_camera (ForgerContext *self,
                              ForgerCamera    *camera)
{
  g_return_if_fail (FORGER_IS_CONTEXT (self));
  g_return_if_fail (FORGER_IS_CAMERA (camera));

  if (g_set_weak_pointer (&self->active_camera, camera))
    g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_ACTIVE_CAMERA]);
}

/**
 * forger_context_get_operation:
 * @self: a #ForgerContext.
 *
 * Gets the currently active operation.
 *
 * Returns: (transfer full): a #ForgerOperation or %NULL.
 *
 * Since: 0.1
 */
ForgerOperation *
forger_context_get_operation (ForgerContext *self)
{
  g_return_val_if_fail (FORGER_IS_CONTEXT (self), NULL);

  return self->operation;
}

/**
 * forger_context_bind_operation:
 * @self: a #ForgerContext.
 * @op: a #ForgerOperation.
 *
 * Sets the currently active operation to be @op.
 * @self doesn't take ownership of @op.
 *
 * Since: 0.1
 */
void
forger_context_bind_operation (ForgerContext   *self,
                               ForgerOperation *op)
{
  FORGER_ENTRY;
  g_return_if_fail (FORGER_IS_CONTEXT (self));
  g_return_if_fail (FORGER_IS_OPERATION (op));

  if (self->operation == op)
    FORGER_EXIT;

  /* clear the context property of the previous operation */
  _forger_operation_set_context (self->operation, NULL);

  if (g_set_weak_pointer (&self->operation, op))
    {
      _forger_operation_set_context (self->operation, self);
      g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_OPERATION]);
    }
}


/*
 * -----< EVENT HANDLING METHODS >----- *
 */
/**
 * forger_context_handle_event_scroll:
 * @self: a #ForgerContext
 * @scroll: a #GtkEventControllerScroll: the event controller that controls the
 * scroll.
 * @dx: a #gdouble: X delta
 * @dy: a #gdouble: Y delta
 *
 * Handles the scroll event over the widget.
 *
 * Returns: a #gboolean: %TRUE if the scroll event was handled, %FALSE otherwise.
 */
gboolean
forger_context_handle_event_scroll (ForgerContext            *self,
                                    GtkEventControllerScroll *scroll,
                                    gdouble                   dx,
                                    gdouble                   dy)
{
  g_return_val_if_fail (FORGER_IS_CONTEXT (self), FALSE);

  if (self->active_camera)
    return forger_camera_handle_zoom_event (self->active_camera, scroll, dx, dy);

  return FALSE;
}

/**
 * forger_context_handle_event_drag_update:
 * @self: a #ForgerContext
 * @drag_gesture: a #GtkGestureDrag: the event controller that controls the drag.
 * @offset_x: a #gdouble: X component of the offset vector
 * @offset_y: a #gdouble: Y component of the offset vector
 *
 * Handles the drap update event over the widget.
 *
 */
void
forger_context_handle_event_drag_update (ForgerContext  *self,
                                         GtkGestureDrag *drag_gesture,
                                         gdouble         offset_x,
                                         gdouble         offset_y)
{
  guint mouse_button;

  g_return_if_fail (FORGER_IS_CONTEXT (self));

  mouse_button = gtk_gesture_single_get_current_button (GTK_GESTURE_SINGLE (drag_gesture));
  if (mouse_button == GDK_BUTTON_MIDDLE &&
      self->active_camera)
    forger_camera_handle_pan_event (self->active_camera,
                                    drag_gesture, offset_x, offset_y);
  else
    forger_operation_drag_update (self->operation,
                                  drag_gesture, offset_x, offset_y);
}

/**
 * forger_context_handle_event_click_pressed:
 * @self: a #ForgerContext
 * @click_gesture: a #GtkGestureClick: the event controller that controls the click.
 * @x: a #gdouble: X component of the click point location (in widget coordinates)
 * @y: a #gdouble: Y component of the click point location (in widget coordinates)
 *
 * Handles the click.pressed event over the widget.
 *
 */
void
forger_context_handle_event_click_pressed (ForgerContext   *self,
                                           GtkGestureClick *click_gesture,
                                           gint             n_press,
                                           gdouble          x,
                                           gdouble          y)
{
  g_return_if_fail (FORGER_IS_CONTEXT (self));
  forger_operation_click_pressed (self->operation,
                                  click_gesture, n_press, x, y);
}

/**
 * forger_context_handle_event_update:
 * @self: a #ForgerContext
 * @frame_time: a #gint64: the frame time
 *
 * Handles the update event: any animation should be handled here.
 *
 */
void
forger_context_handle_event_update (ForgerContext *self,
                                    gint64         frame_time)
{
  g_return_if_fail (FORGER_IS_CONTEXT (self));

  if (self->operation)
    forger_operation_update (self->operation, frame_time);

  if (self->active_camera)
    forger_camera_handle_update (self->active_camera, frame_time);
}
