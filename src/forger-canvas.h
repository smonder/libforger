/*
 * forger-canvas.h
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#if !defined(FORGER_INSIDE) && !defined(LIBFORGER_COMPILATION)
#error "Only <libforger.h> can be included directly."
#endif

#include <gtk/gtk.h>

#include "forger-context.h"
#include "forger-version-macros.h"

G_BEGIN_DECLS

#define FORGER_TYPE_CANVAS (forger_canvas_get_type())
#define forger_canvas_make_current(self_) (gtk_gl_area_make_current(GTK_GL_AREA ((self_))))

FORGER_AVAILABLE_IN_ALL
G_DECLARE_FINAL_TYPE (ForgerCanvas, forger_canvas, FORGER, CANVAS, GtkGLArea)

FORGER_AVAILABLE_IN_ALL
GtkWidget *     forger_canvas_new                (void) G_GNUC_WARN_UNUSED_RESULT;
FORGER_AVAILABLE_IN_ALL
GtkWidget *     forger_canvas_new_for_gl_context (GdkGLContext *  gl_context) G_GNUC_WARN_UNUSED_RESULT;

FORGER_AVAILABLE_IN_ALL
ForgerContext * forger_canvas_get_context        (ForgerCanvas *  self);
FORGER_AVAILABLE_IN_ALL
void            forger_canvas_set_context        (ForgerCanvas *  self,
                                                  ForgerContext * context);

FORGER_AVAILABLE_IN_ALL
GdkRGBA *       forger_canvas_get_bgcolor        (ForgerCanvas *  self);
FORGER_AVAILABLE_IN_ALL
void            forger_canvas_set_bgcolor        (ForgerCanvas *  self,
				                                          GdkRGBA *       color);
FORGER_AVAILABLE_IN_ALL
void            forger_canvas_set_bgcolor_name   (ForgerCanvas *  self,
				                                          const gchar *   color);
FORGER_AVAILABLE_IN_ALL
GdkPaintable *  forger_canvas_get_paintable      (ForgerCanvas *  self);

G_END_DECLS
