/*
 * forger-version-macros.h
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <glib.h>

#include "forger-version.h"

#ifndef _FORGER_EXTERN
# define _FORGER_EXTERN extern
#endif

#ifdef FORGER_DISABLE_DEPRECATION_WARNINGS
# define FORGER_DEPRECATED _FORGER_EXTERN
# define FORGER_DEPRECATED_FOR(f) _FORGER_EXTERN
# define FORGER_UNAVAILABLE(maj,min) _FORGER_EXTERN
#else
# define FORGER_DEPRECATED G_DEPRECATED _FORGER_EXTERN
# define FORGER_DEPRECATED_FOR(f) G_DEPRECATED_FOR(f) _FORGER_EXTERN
# define FORGER_UNAVAILABLE(maj,min) G_UNAVAILABLE(maj,min) _FORGER_EXTERN
#endif

#define FORGER_VERSION_1_0 (G_ENCODE_VERSION (1, 0))

#if (FORGER_MINOR_VERSION == 99)
# define FORGER_VERSION_CUR_STABLE (G_ENCODE_VERSION (FORGER_MAJOR_VERSION + 1, 0))
#elif (FORGER_MINOR_VERSION % 2)
# define FORGER_VERSION_CUR_STABLE (G_ENCODE_VERSION (FORGER_MAJOR_VERSION, FORGER_MINOR_VERSION + 1))
#else
# define FORGER_VERSION_CUR_STABLE (G_ENCODE_VERSION (FORGER_MAJOR_VERSION, FORGER_MINOR_VERSION))
#endif

#if (FORGER_MINOR_VERSION == 99)
# define FORGER_VERSION_PREV_STABLE (G_ENCODE_VERSION (FORGER_MAJOR_VERSION + 1, 0))
#elif (FORGER_MINOR_VERSION % 2)
# define FORGER_VERSION_PREV_STABLE (G_ENCODE_VERSION (FORGER_MAJOR_VERSION, FORGER_MINOR_VERSION - 1))
#else
# define FORGER_VERSION_PREV_STABLE (G_ENCODE_VERSION (FORGER_MAJOR_VERSION, FORGER_MINOR_VERSION - 2))
#endif

/**
 * FORGER_VERSION_MIN_REQUIRED:
 *
 * A macro that should be defined by the user prior to including
 * the libforger.h header.
 *
 * The definition should be one of the predefined Drafting version
 * macros: %FORGER_VERSION_1_0, ...
 *
 * This macro defines the lower bound for the Drafting API to use.
 *
 * If a function has been deprecated in a newer version of Drafting,
 * it is possible to use this symbol to avoid the compiler warnings
 * without disabling warning for every deprecated function.
 */
#ifndef FORGER_VERSION_MIN_REQUIRED
# define FORGER_VERSION_MIN_REQUIRED (FORGER_VERSION_CUR_STABLE)
#endif

/**
 * FORGER_VERSION_MAX_ALLOWED:
 *
 * A macro that should be defined by the user prior to including
 * the libforger.h header.

 * The definition should be one of the predefined Drafting version
 * macros: %FORGER_VERSION_1_0, %FORGER_VERSION_1_2,...
 *
 * This macro defines the upper bound for the Drafting API to use.
 *
 * If a function has been introduced in a newer version of Drafting,
 * it is possible to use this symbol to get compiler warnings when
 * trying to use that function.
 */
#ifndef FORGER_VERSION_MAX_ALLOWED
# if FORGER_VERSION_MIN_REQUIRED > FORGER_VERSION_PREV_STABLE
#  define FORGER_VERSION_MAX_ALLOWED (FORGER_VERSION_MIN_REQUIRED)
# else
#  define FORGER_VERSION_MAX_ALLOWED (FORGER_VERSION_CUR_STABLE)
# endif
#endif

#if FORGER_VERSION_MAX_ALLOWED < FORGER_VERSION_MIN_REQUIRED
#error "FORGER_VERSION_MAX_ALLOWED must be >= FORGER_VERSION_MIN_REQUIRED"
#endif
// #if FORGER_VERSION_MIN_REQUIRED < FORGER_VERSION_1_0
// #error "FORGER_VERSION_MIN_REQUIRED must be >= FORGER_VERSION_1_0"
// #endif

#define FORGER_AVAILABLE_IN_ALL                  _FORGER_EXTERN

#if FORGER_VERSION_MAX_ALLOWED < FORGER_VERSION_1_0
# define FORGER_AVAILABLE_IN_1_0                 FORGER_UNAVAILABLE(1, 0)
#else
# define FORGER_AVAILABLE_IN_1_0                 _FORGER_EXTERN
#endif

FORGER_AVAILABLE_IN_1_0
guint    forger_get_major_version (void);
FORGER_AVAILABLE_IN_1_0
guint    forger_get_minor_version (void);
FORGER_AVAILABLE_IN_1_0
guint    forger_get_micro_version (void);
FORGER_AVAILABLE_IN_1_0
gboolean forger_check_version     (guint major,
                                  guint minor,
                                  guint micro);
