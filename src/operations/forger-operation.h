/*
 * forger-operation.h
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#if !defined(FORGER_INSIDE) && !defined(LIBFORGER_COMPILATION)
#error "Only <libforger.h> can be included directly."
#endif

#include <gtk/gtk.h>

#include "forger-types.h"
#include "forger-version-macros.h"

G_BEGIN_DECLS

#define FORGER_TYPE_OPERATION (forger_operation_get_type())

FORGER_AVAILABLE_IN_ALL
G_DECLARE_DERIVABLE_TYPE (ForgerOperation, forger_operation, FORGER, OPERATION, GObject)

struct _ForgerOperationClass
{
  GObjectClass parent_class;

  /* for GUI */
  gchar *        (* get_name)         (ForgerOperation *           operation);
  GIcon *        (* get_icon)         (ForgerOperation *           operation);
  gchar *        (* get_tooltip)      (ForgerOperation *           operation);

  /* Event handling */
  void           (* on_update)        (ForgerOperation *           operation,
                                       guint                       time_step);

  void           (* on_drag_update)   (ForgerOperation *           operation,
                                       GtkGestureDrag   *          drag_gesture,
                                       const gdouble               offset_x,
                                       const gdouble               offset_y);

  gboolean       (* on_scroll_update) (ForgerOperation *           operation,
                                       GtkEventControllerScroll *  scroll,
                                       const gdouble               offset_x,
                                       const gdouble               offset_y);

  void           (* on_click_pressed) (ForgerOperation *           operation,
                                       GtkGestureClick *           click_gesture,
                                       const gint                  n_press,
                                       const gdouble               x,
                                       const gdouble               y);
};


FORGER_AVAILABLE_IN_ALL
ForgerContext * forger_operation_get_context       (ForgerOperation *          self);

FORGER_AVAILABLE_IN_ALL
gchar *         forger_operation_get_name          (ForgerOperation *          self);
FORGER_AVAILABLE_IN_ALL
GIcon *         forger_operation_get_icon          (ForgerOperation *          self);
FORGER_AVAILABLE_IN_ALL
gchar *         forger_operation_get_tooltip       (ForgerOperation *          self);

FORGER_AVAILABLE_IN_ALL
void            forger_operation_update            (ForgerOperation *          self,
                                                    guint                      time_step);
FORGER_AVAILABLE_IN_ALL
void            forger_operation_drag_update       (ForgerOperation *          self,
                                                    GtkGestureDrag *           drag_gesture,
                                                    const gdouble              offset_x,
                                                    const gdouble              offset_y);
FORGER_AVAILABLE_IN_ALL
gboolean        forger_operation_scroll_update     (ForgerOperation *          self,
                                                    GtkEventControllerScroll * scroll,
                                                    const gdouble              offset_x,
                                                    const gdouble              offset_y);
FORGER_AVAILABLE_IN_ALL
void            forger_operation_click_pressed     (ForgerOperation *          self,
                                                    GtkGestureClick *          click_gesture,
                                                    const gint                 n_press,
                                                    const gdouble              x,
                                                    const gdouble              y);

G_END_DECLS
