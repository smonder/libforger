/*
 * forger-operation.c
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
#define G_LOG_DOMAIN "forger-operation"

#include "config.h"
#include "forger-debug.h"

#include <glib/gi18n.h>

#include "forger-context.h"
#include "forger-operation-private.h"

typedef struct
{
  ForgerContext * context;

} ForgerOperationPrivate;

G_DEFINE_ABSTRACT_TYPE_WITH_PRIVATE (ForgerOperation, forger_operation, G_TYPE_OBJECT)

enum {
  PROP_0,
  PROP_CONTEXT,
  PROP_NAME,
  PROP_ICON,
  PROP_TOOLTIP,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

/*
 * -----< G_OBJECT CLASS IMPLEMENTATION >----- *
 */
static void
forger_operation_get_property (GObject    *object,
                               guint       prop_id,
                               GValue     *value,
                               GParamSpec *pspec)
{
  ForgerOperation *self = FORGER_OPERATION (object);

  switch (prop_id)
    {
    case PROP_CONTEXT:
      g_value_set_object (value, forger_operation_get_context (self));
      break;

    case PROP_NAME:
      g_value_set_string (value, forger_operation_get_name (self));
      break;

    case PROP_ICON:
      g_value_set_object (value, forger_operation_get_icon (self));
      break;

    case PROP_TOOLTIP:
      g_value_set_string (value, forger_operation_get_tooltip (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
forger_operation_class_init (ForgerOperationClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->get_property = forger_operation_get_property;

  /**
   * ForgerMousePicker:context:
   *
   * The "context" property is the #ForgerContext where the operation
   * should query.
   *
   * Since: 0.1
   */
  properties [PROP_CONTEXT] =
    g_param_spec_object ("context", NULL, NULL,
                         FORGER_TYPE_CONTEXT,
                         (G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));

  /**
   * ForgerOperation:name:
   *
   * The "name" property is the name used to identify the operation.
   *
   * Since: 0.1
   */
  properties [PROP_NAME] =
    g_param_spec_string ("name", NULL, NULL,
                         _("dummy"),
                         (G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));

  /**
   * ForgerOperation:icon:
   *
   * The "icon" property is the icon used to represent the operation in GUI.
   *
   * Since: 0.1
   */
  properties [PROP_ICON] =
    g_param_spec_object ("icon", NULL, NULL,
                         G_TYPE_ICON,
                         (G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));

  /**
   * ForgerOperation:tooltip:
   *
   * The "tooltip" property is the tooltip text of the operation.
   *
   * Since: 0.1
   */
  properties [PROP_TOOLTIP] =
    g_param_spec_string ("tooltip", NULL, NULL,
                         _("dummy"),
                         (G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
forger_operation_init (ForgerOperation *self)
{
}


/*
 * -----< CLASS VIRTUAL METHODS >----- *
 */
/**
 * forger_operation_get_name:
 * @self: a #ForgerOperation.
 *
 * Gets the "name" property.
 *
 * Returns: (transfer full): a #gchar.
 */
gchar *
forger_operation_get_name (ForgerOperation *self)
{
  return FORGER_OPERATION_GET_CLASS (self)->get_name (self);
}

/**
 * forger_operation_get_icon:
 * @self: a #ForgerOperation
 *
 * Gets the "icon" property
 *
 * Returns: (transfer full): a #GIcon
 */
GIcon *
forger_operation_get_icon (ForgerOperation *self)
{
  return FORGER_OPERATION_GET_CLASS (self)->get_icon (self);
}

/**
 * forger_operation_get_tooltip:
 * @self: a #ForgerOperation.
 *
 * Gets the "tooltip" property of @self.
 *
 * Returns: (transfer full): a #gchar.
 */
gchar *
forger_operation_get_tooltip (ForgerOperation *self)
{
  return FORGER_OPERATION_GET_CLASS (self)->get_tooltip (self);
}

/**
 * forger_operation_update:
 * @self: a #ForgerOperation.
 * @time_step: a #guint.
 *
 * Handles the on_update() event.
 * This function will get called in #ForgerCanvas::tick() callback
 * so that the handler is going to update #ForgerCanvas on each frame.
 *
 * Since: 0.1
 */
void
forger_operation_update (ForgerOperation *self,
                         guint            time_step)
{
  g_return_if_fail (!self || FORGER_IS_OPERATION (self));

  if (self == NULL)
    return;

  if (FORGER_OPERATION_GET_CLASS (self)->on_update)
    FORGER_OPERATION_GET_CLASS (self)->on_update (self, time_step);
}

/**
 * forger_operation_drag_update:
 * @self: a #ForgerOperation.
 * @drag_gesture: a #GtkGestureDrag.
 * @offset_x: a #gdouble: X offset, relative to the start point.
 * @offset_y: a #gdouble: Y offset, relative to the start point.
 *
 * Handles the drag_update() event.
 * This function will get called in #GtkGestureDrag::drag_update()
 * signal callback of the #ForgerCanvas.
 *
 * Since: 0.1
 */
void
forger_operation_drag_update (ForgerOperation *self,
                              GtkGestureDrag  *drag_gesture,
                              const gdouble    offset_x,
                              const gdouble    offset_y)
{
  g_return_if_fail (!self || FORGER_IS_OPERATION (self));

  if (self == NULL)
    return;

  if (FORGER_OPERATION_GET_CLASS (self)->on_drag_update)
    FORGER_OPERATION_GET_CLASS (self)->on_drag_update (self,
                                                       drag_gesture,
                                                       offset_x,
                                                       offset_y);
}

/**
 * forger_operation_scroll_update:
 * @self: a #ForgerOperation.
 * @scroll: a #GtkEventControllerScroll.
 * @offset_x: a #gdouble: X delta.
 * @offset_y: a #gdouble: Y delta.
 *
 * Handles the scroll() event.
 * This function will get called in #GtkEventControllerScroll::scroll()
 * signal callback of the #ForgerCanvas.
 *
 * Returns: %TRUE if the event is handled, %FALSE otherwise.
 *
 * Since: 0.1
 */
gboolean
forger_operation_scroll_update (ForgerOperation          *self,
                                GtkEventControllerScroll *scroll,
                                const gdouble             offset_x,
                                const gdouble             offset_y)
{
  g_return_val_if_fail (!self || FORGER_IS_OPERATION (self), FALSE);

  if (self == NULL)
    return FALSE;

  if (FORGER_OPERATION_GET_CLASS (self)->on_scroll_update)
    return FORGER_OPERATION_GET_CLASS (self)->on_scroll_update (self,
                                                                scroll,
                                                                offset_x,
                                                                offset_y);

  return FALSE;
}

/**
 * forger_operation_click_pressed:
 * @self: a #ForgerOperation.
 * @click_gesture: a #GtkGestureClick.
 * @n_press: a #gint: How many touch/button presses happened with this one.
 * @x: a #gdouble: The X coordinate, in widget allocation coordinates.
 * @y: a #gdouble: The Y coordinate, in widget allocation coordinates.
 *
 * Handles the click_pressed() event.
 * This function will get called in #GtkGestureClick::pressed()
 * signal callback of the #ForgerCanvas.
 *
 * Since: 0.1
 */
void
forger_operation_click_pressed (ForgerOperation *self,
                                GtkGestureClick *click_gesture,
                                const gint       n_press,
                                const gdouble    x,
                                const gdouble    y)
{
  g_return_if_fail (!self || FORGER_IS_OPERATION (self));

  if (self == NULL)
    return;

  if (FORGER_OPERATION_GET_CLASS (self)->on_click_pressed)
    FORGER_OPERATION_GET_CLASS (self)->on_click_pressed (self,
                                                         click_gesture,
                                                         n_press, x, y);
}


/*
 * -----< PROPERTIES >----- *
 */
/**
 * forger_operation_get_context:
 *
 * Get the "context" property
 *
 * Returns: (transfer full): a #ForgerContext
 */
ForgerContext *
forger_operation_get_context (ForgerOperation *self)
{
  ForgerOperationPrivate *priv = forger_operation_get_instance_private (self);
  g_return_val_if_fail (FORGER_IS_OPERATION (self), NULL);
  return priv->context;
}


/*
 * -----< PRIVATE API >----- *
 */
void
_forger_operation_set_context (ForgerOperation *self,
                               ForgerContext   *context)
{
  ForgerOperationPrivate *priv = forger_operation_get_instance_private (self);

  FORGER_ENTRY;
  g_return_if_fail (FORGER_IS_OPERATION (self));
  g_return_if_fail (!context || FORGER_IS_CONTEXT (context));

  if (priv->context == context)
    FORGER_EXIT;

  if (g_set_weak_pointer (&priv->context, context))
    g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_CONTEXT]);

  FORGER_EXIT;
}
