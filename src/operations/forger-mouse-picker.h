/*
 * forger-mouse-picker.h
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include "forger-context.h"
#include "forger-operation.h"
#include "forger-version-macros.h"

G_BEGIN_DECLS

#define FORGER_TYPE_MOUSE_PICKER (forger_mouse_picker_get_type())

FORGER_AVAILABLE_IN_ALL
G_DECLARE_FINAL_TYPE (ForgerMousePicker, forger_mouse_picker, FORGER, MOUSE_PICKER, ForgerOperation)

FORGER_AVAILABLE_IN_ALL
ForgerOperation *    forger_mouse_picker_get_default     (void) G_GNUC_WARN_UNUSED_RESULT;

FORGER_AVAILABLE_IN_ALL
graphene_ray_t *     forger_mouse_picker_get_mouse_ray   (ForgerMousePicker * self);
FORGER_AVAILABLE_IN_ALL
graphene_point3d_t * forger_mouse_picker_get_mouse_hit   (ForgerMousePicker * self);

G_END_DECLS
