/*
 * forger-mouse-picker.c
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
#define G_LOG_DOMAIN "forger-mouse-picker"

#include "config.h"
#include "forger-debug.h"

#include <glib/gi18n.h>

#include "forger-mouse-picker.h"

struct _ForgerMousePicker
{
  ForgerOperation    parent_instance;

  graphene_ray_t     mouse_ray;
  graphene_point3d_t mouse_hit;
};

G_DEFINE_FINAL_TYPE (ForgerMousePicker, forger_mouse_picker, FORGER_TYPE_OPERATION)

enum {
  PROP_0,
  PROP_MOUSE_RAY,
  PROP_MOUSE_HIT,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

/*
 * -----< FORGER_OPERATION CLASS IMPLEMENTATION >----- *
 */
static gchar *
forger_mouse_picker_get_name (G_GNUC_UNUSED ForgerOperation *operation)
{
  return _("Select Tool");
}

static GIcon *
forger_mouse_picker_get_icon (G_GNUC_UNUSED ForgerOperation *operation)
{
  return g_themed_icon_new ("input-mouse-symbolic");
}

static gchar *
forger_mouse_picker_get_tooltip (G_GNUC_UNUSED ForgerOperation *operation)
{
  return _("Click on canvas to select or pick up points");
}

static ForgerEntityVisit
on_entites_foreach_cb (ForgerEntity *entity,
                       gpointer      user_data)
{
  ForgerMousePicker *self = user_data;
  ForgerContext *context;

  g_assert (FORGER_IS_MOUSE_PICKER (self));
  g_assert (FORGER_IS_ENTITY (entity));

  if (!forger_entity_is_visible (entity) ||
      FORGER_IS_CAMERA (entity))
    return FORGER_ENTITY_VISIT_CONTINUE;

  if (FORGER_IS_CONTAINER (entity))
    return FORGER_ENTITY_VISIT_CHILDREN;

  if (!forger_entity_ray_intersects (entity, &self->mouse_ray))
    {
      /* forger_context_deselect_all (self->context); */
      return FORGER_ENTITY_VISIT_CONTINUE;
    }

  context = forger_operation_get_context (FORGER_OPERATION (self));
  if (forger_context_is_entity_selected (context, entity))
    forger_context_deselect_entity (context, entity);
  else
    forger_context_select_entity (context, entity);

  return FORGER_ENTITY_VISIT_CONTINUE;
}
static void
forger_mouse_picker_handle_click_pressed (ForgerOperation *operation,
                                          GtkGestureClick *click_gesture,
                                          const gint       n_press,
                                          const gdouble    x,
                                          const gdouble    y)
{
  ForgerMousePicker *self = FORGER_MOUSE_PICKER (operation);
  ForgerContext *context;
  ForgerScene * scene;
  ForgerCamera * camera;
  graphene_matrix_t * vp_matrix;
  GdkRectangle * viewport;
  graphene_point3d_t mouse_ray_origin;
  graphene_vec3_t mouse_ray_direction;

  FORGER_ENTRY;
  g_return_if_fail (FORGER_IS_MOUSE_PICKER (self));
  g_return_if_fail (GTK_IS_GESTURE_CLICK (click_gesture));

  context = forger_operation_get_context (FORGER_OPERATION (self));
  if (!context)
    FORGER_EXIT;

  scene = forger_context_get_scene (context);
  camera = forger_context_get_active_camera (context);
  viewport = forger_camera_get_viewport (camera);
  vp_matrix = forger_camera_get_vpmatrix (camera);

  /* Convert to Normalized device coordinates */
  mouse_ray_origin.x = (2.0f * x) / (gfloat)viewport->width - 1.0f;
  mouse_ray_origin.y = -((2.0f * y) / (gfloat)viewport->height - 1.0f);
  mouse_ray_origin.z = -1.0f;

  /* Convert to world space coordinates */
  graphene_matrix_t inv_vpmat;
  graphene_matrix_inverse (vp_matrix, &inv_vpmat);
  graphene_matrix_transform_point3d (&inv_vpmat, &mouse_ray_origin, &mouse_ray_origin);

#ifdef ENABLE_TRACING
  g_message ("MOUSE RAY %f %f %f", mouse_ray_origin.x, mouse_ray_origin.y, mouse_ray_origin.z);
#endif

  graphene_vec3_init (&mouse_ray_direction, 0.0f, 0.0f, -1.0f);
  graphene_ray_init (&self->mouse_ray, &mouse_ray_origin, &mouse_ray_direction);

  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_MOUSE_RAY]);

  forger_container_traverse (forger_scene_get_root (scene),
                             G_PRE_ORDER,
                             G_TRAVERSE_ALL, -1,
                             on_entites_foreach_cb,
                             self);
  FORGER_EXIT;
}


/*
 * -----< G_OBJECT CLASS IMPLEMENTATION >----- *
 */
static GObject *
forger_mouse_picker_constructor (GType                  type,
                                 guint                  n_construct_params,
                                 GObjectConstructParam *construct_params)
{
  static GWeakRef self_wref;
  GObject *self = NULL;

  if ((self = g_weak_ref_get (&self_wref)) == NULL)
    {
      self = G_OBJECT_CLASS (forger_mouse_picker_parent_class)->constructor (type,
                                                                             n_construct_params,
                                                                             construct_params);
      g_object_add_weak_pointer (self, (gpointer)&self);
      g_weak_ref_set (&self_wref, self);

      return self;
    }

  return g_object_ref (self);
}

static void
forger_mouse_picker_get_property (GObject    *object,
                                  guint       prop_id,
                                  GValue     *value,
                                  GParamSpec *pspec)
{
  ForgerMousePicker *self = FORGER_MOUSE_PICKER (object);

  switch (prop_id)
    {
    case PROP_MOUSE_RAY:
      g_value_set_boxed (value, forger_mouse_picker_get_mouse_ray (self));
      break;

    case PROP_MOUSE_HIT:
      g_value_set_boxed (value, forger_mouse_picker_get_mouse_hit (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
forger_mouse_picker_class_init (ForgerMousePickerClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  ForgerOperationClass *operation_class = FORGER_OPERATION_CLASS (klass);

  object_class->constructor = forger_mouse_picker_constructor;
  object_class->get_property = forger_mouse_picker_get_property;

  operation_class->get_name = forger_mouse_picker_get_name;
  operation_class->get_icon = forger_mouse_picker_get_icon;
  operation_class->get_tooltip = forger_mouse_picker_get_tooltip;
  operation_class->on_click_pressed = forger_mouse_picker_handle_click_pressed;

  /**
   * ForgerMousePicker:mouse-ray:
   *
   * The "mouse-ray" property is the #graphene_ray_t representing the ray
   * fired from mouse location towards screen in world coordinates.
   * Therefore, there is no need to transform the ray obtained by the getter
   * since it is already transformed to world space coordinates.
   *
   * Since: 0.1
   */
  properties [PROP_MOUSE_RAY] =
    g_param_spec_boxed ("mouse-ray", NULL, NULL,
                        GRAPHENE_TYPE_RAY,
                        (G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));

  /**
   * ForgerMousePicker:mouse-hit:
   *
   * The "mouse-hit" property is the point where the mouse ray
   * hits the ground level,
   *
   * Since: 0.1
   */
  properties [PROP_MOUSE_HIT] =
    g_param_spec_boxed ("mouse-hit", NULL, NULL,
                        GRAPHENE_TYPE_POINT3D,
                        (G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
forger_mouse_picker_init (ForgerMousePicker *self)
{
}


/*
 * -----< CONSTRUCTORS >----- *
 */
/**
 * forger_mouse_picker_get_default:
 *
 * Gets the default instance of a #ForgerMousePicker.
 * First call to this method will create a new instance.
 *
 * Returns: (transfer full): a #ForgerOperation
 */
ForgerOperation *
forger_mouse_picker_get_default (void)
{
  return g_object_new (FORGER_TYPE_MOUSE_PICKER, NULL);
}


/*
 * -----< PROPERTIES >----- *
 */
/**
 * forger_mouse_picker_get_mouse_ray:
 * @self: a #ForgerMousePicker
 *
 * Get the "mouse-ray" property.
 *
 * Returns: (transfer full): a pointer to #graphene_ray_t
 */
graphene_ray_t *
forger_mouse_picker_get_mouse_ray (ForgerMousePicker *self)
{
  g_return_val_if_fail (FORGER_IS_MOUSE_PICKER (self), NULL);
  return &self->mouse_ray;
}

/**
 * forger_mouse_picker_get_mouse_hit:
 * @self: a #ForgerMousePicker
 *
 * Get the "mouse-hit" property.
 *
 * Returns: (transfer full): a pointer to #graphene_point3d_t
 */
graphene_point3d_t *
forger_mouse_picker_get_mouse_hit (ForgerMousePicker *self)
{
  g_return_val_if_fail (FORGER_IS_MOUSE_PICKER (self), NULL);
  return &self->mouse_hit;
}


