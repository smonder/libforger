/*
 * forger-wire-render-node.c
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
#define G_LOG_DOMAIN "forger-wire-render-node"

#include "config.h"
#include "forger-debug.h"

#include <epoxy/gl.h>

#include "forger-wire-render-node.h"

struct _ForgerWireRenderNode
{
  ForgerMeshRenderNode parent_instance;

  guint8  line_width;
};

G_DEFINE_FINAL_TYPE (ForgerWireRenderNode, forger_wire_render_node, FORGER_TYPE_MESH_RENDER_NODE)

enum {
  PROP_0,
  PROP_LINE_WIDTH,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];



/*
 * -----< FORGER_RENDER_NODE CLASS IMPLEMENTATION >----- *
 */
static void
forger_wire_render_node_real_draw (ForgerRenderNode * node)
{
  ForgerWireRenderNode *self = (ForgerWireRenderNode *)node;

  g_assert (FORGER_IS_WIRE_RENDER_NODE (self));

  glLineWidth (self->line_width);
  glPolygonMode (GL_FRONT_AND_BACK, GL_LINE);
  FORGER_RENDER_NODE_CLASS (forger_wire_render_node_parent_class)->draw (node);
  glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);
  glLineWidth (1);
}


/*
 * -----< G_OBJECT CLASS IMPLEMENTATION >----- *
 */
static void
forger_wire_render_node_get_property (GObject    *object,
                                      guint       prop_id,
                                      GValue     *value,
                                      GParamSpec *pspec)
{
  ForgerWireRenderNode *self = FORGER_WIRE_RENDER_NODE (object);

  switch (prop_id)
    {
    case PROP_LINE_WIDTH:
      g_value_set_uint (value, forger_wire_render_node_get_line_width (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
forger_wire_render_node_set_property (GObject      *object,
                                      guint         prop_id,
                                      const GValue *value,
                                      GParamSpec   *pspec)
{
  ForgerWireRenderNode *self = FORGER_WIRE_RENDER_NODE (object);

  switch (prop_id)
    {
    case PROP_LINE_WIDTH:
      forger_wire_render_node_set_line_width (self, g_value_get_uint (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
forger_wire_render_node_class_init (ForgerWireRenderNodeClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  ForgerRenderNodeClass *rnode_class = FORGER_RENDER_NODE_CLASS (klass);

  object_class->get_property = forger_wire_render_node_get_property;
  object_class->set_property = forger_wire_render_node_set_property;

  rnode_class->draw = forger_wire_render_node_real_draw;

  properties [PROP_LINE_WIDTH] =
    g_param_spec_uint ("line-width", NULL, NULL,
                       1, 7, 1,
                       (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
forger_wire_render_node_init (ForgerWireRenderNode *self)
{
  self->line_width = 1;
}


/*
 * -----< CONSTRUCTORS >----- *
 */
ForgerRenderNode *
forger_wire_render_node_new (const guint n_max_entity)
{
  return g_object_new (FORGER_TYPE_WIRE_RENDER_NODE,
                       "capacity", n_max_entity, NULL);
}


/*
 * -----< PROPERTIES >----- *
 */
guint
forger_wire_render_node_get_line_width (ForgerWireRenderNode *self)
{
  g_return_val_if_fail (FORGER_IS_WIRE_RENDER_NODE (self), 0);
  return self->line_width;
}

void
forger_wire_render_node_set_line_width (ForgerWireRenderNode *self,
                                        const guint           value)
{
  g_return_if_fail (FORGER_IS_WIRE_RENDER_NODE (self));

  if (self->line_width != value)
    {
      self->line_width = value;
      g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_LINE_WIDTH]);
    }
}

