/*
 * forger-mix-render-node.c
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "forger-mix-render-node"

#include "config.h"
#include "forger-debug.h"

#include "forger-mix-render-node.h"

struct _ForgerMixRenderNode
{
  ForgerRenderNode parent_instance;

  GQueue * children;
};

G_DEFINE_FINAL_TYPE (ForgerMixRenderNode, forger_mix_render_node, FORGER_TYPE_RENDER_NODE)


static void
forger_mix_render_node_real_draw (ForgerRenderNode * node)
{
  ForgerMixRenderNode *self = (ForgerMixRenderNode *)node;

  g_queue_foreach (self->children, (GFunc)forger_render_node_draw, NULL);
}

static void
forger_mix_render_node_finalize (GObject *object)
{
  ForgerMixRenderNode *self = (ForgerMixRenderNode *)object;

  g_queue_free (self->children);

  G_OBJECT_CLASS (forger_mix_render_node_parent_class)->finalize (object);
}

static void
forger_mix_render_node_class_init (ForgerMixRenderNodeClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  ForgerRenderNodeClass *node_class = FORGER_RENDER_NODE_CLASS (klass);

  object_class->finalize = forger_mix_render_node_finalize;

  node_class->draw = forger_mix_render_node_real_draw;
}

static void
forger_mix_render_node_init (ForgerMixRenderNode *self)
{
  self->children = g_queue_new ();
}


ForgerRenderNode *
forger_mix_render_node_new (void)
{
  return g_object_new (FORGER_TYPE_MIX_RENDER_NODE, NULL);
}

void
forger_mix_render_node_append_child (ForgerMixRenderNode *self,
                                     ForgerRenderNode    *child)
{
  g_return_if_fail (FORGER_IS_MIX_RENDER_NODE (self));
  g_return_if_fail (FORGER_IS_RENDER_NODE (child));

  g_queue_push_head (self->children, child);
}

void
forger_mix_render_node_prepend_child (ForgerMixRenderNode *self,
                                      ForgerRenderNode    *child)
{
  g_return_if_fail (FORGER_IS_MIX_RENDER_NODE (self));
  g_return_if_fail (FORGER_IS_RENDER_NODE (child));

  g_queue_push_tail (self->children, child);
}

void
forger_mix_render_node_insert_child (ForgerMixRenderNode *self,
                                     const gint           index_,
                                     ForgerRenderNode    *child)
{
  g_return_if_fail (FORGER_IS_MIX_RENDER_NODE (self));
  g_return_if_fail (FORGER_IS_RENDER_NODE (child));

  g_queue_push_nth (self->children, child, index_);
}

/**
 * forger_mix_render_node_foreach_child:
 * @self: a #ForgerMixRenderNode
 * @func: (scope call): a #GFunc.
 *
 * Calls %func on every child of %self.
 *
 */
void
forger_mix_render_node_foreach_child (ForgerMixRenderNode *self,
                                      GFunc                func,
                                      gpointer             user_data)
{
  g_return_if_fail (FORGER_IS_MIX_RENDER_NODE (self));

  g_queue_foreach (self->children, func, user_data);
}

