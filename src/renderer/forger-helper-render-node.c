/*
 * forger-helper-render-node.c
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "forger-helper-render-node"

#include "config.h"
#include "forger-debug.h"

#include <graphene.h>
#include <epoxy/gl.h>

#include "forger-helper-render-node.h"

enum
{
  VERTEX_POSITIONS,
  INDEX_BUFFER,

  N_GL_BUFFER
};

typedef struct
{
  /* GL INFO */
  guint VAO;

  guint gl_buffers [N_GL_BUFFER];

  guint capacity;
} ForgerHelperRenderNodePrivate;

G_DEFINE_TYPE_WITH_PRIVATE (ForgerHelperRenderNode, forger_helper_render_node, FORGER_TYPE_RENDER_NODE)

enum {
  PROP_0,
  PROP_CAPACITY,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];


static void
forger_helper_render_node_real_draw (ForgerRenderNode * node)
{
  ForgerHelperRenderNode *self = (ForgerHelperRenderNode *)node;
  ForgerHelperRenderNodePrivate *priv = forger_helper_render_node_get_instance_private (self);

  g_assert (FORGER_IS_HELPER_RENDER_NODE (self));

  glBindVertexArray (priv->VAO);

  glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, priv->gl_buffers [INDEX_BUFFER]);

  glPolygonMode (GL_FRONT_AND_BACK, GL_LINE);
  glLineWidth (4);
  glDrawElements (GL_TRIANGLES, priv->capacity, GL_UNSIGNED_INT, NULL);
  glLineWidth (1);
  glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);

  glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, 0);
  glBindVertexArray (0);
}

static void
forger_helper_render_node_real_update_positions (ForgerRenderNode *node,
                                                 gconstpointer     data,
                                                 const gsize       n_vertex)
{
  ForgerHelperRenderNode *self = (ForgerHelperRenderNode *)node;
  ForgerHelperRenderNodePrivate *priv = forger_helper_render_node_get_instance_private (self);

  g_assert (FORGER_IS_HELPER_RENDER_NODE (self));

  glBindVertexArray (priv->VAO);
  glBindBuffer (GL_ARRAY_BUFFER, priv->gl_buffers[VERTEX_POSITIONS]);
  glBufferSubData (GL_ARRAY_BUFFER, 0, n_vertex * sizeof(graphene_point3d_t), data);
}

static void
forger_helper_render_node_real_update_indices (ForgerRenderNode *node,
                                               gconstpointer     data,
                                               const gsize       n_index)
{
  ForgerHelperRenderNode *self = (ForgerHelperRenderNode *)node;
  ForgerHelperRenderNodePrivate *priv = forger_helper_render_node_get_instance_private (self);

  g_assert (FORGER_IS_HELPER_RENDER_NODE (self));

  glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, priv->gl_buffers[INDEX_BUFFER]);
  glBufferSubData (GL_ELEMENT_ARRAY_BUFFER, 0, n_index * sizeof(guint), data);
  glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, 0);
}

static void
forger_helper_render_node_finalize (GObject *object)
{
  ForgerHelperRenderNode *self = (ForgerHelperRenderNode *)object;
  ForgerHelperRenderNodePrivate *priv = forger_helper_render_node_get_instance_private (self);

  glDeleteBuffers (N_GL_BUFFER, priv->gl_buffers);
  glDeleteVertexArrays (1, &priv->VAO);

  G_OBJECT_CLASS (forger_helper_render_node_parent_class)->finalize (object);
}

static void
forger_helper_render_node_constructed (GObject *object)
{
  ForgerHelperRenderNode *self = (ForgerHelperRenderNode *)object;
  ForgerHelperRenderNodePrivate *priv = forger_helper_render_node_get_instance_private (self);

  G_OBJECT_CLASS (forger_helper_render_node_parent_class)->constructed (object);

  glGenVertexArrays (1, &priv->VAO);
  glBindVertexArray (priv->VAO);

  glGenBuffers (N_GL_BUFFER, priv->gl_buffers);

  glBindBuffer (GL_ARRAY_BUFFER, priv->gl_buffers[VERTEX_POSITIONS]);
  glBufferData (GL_ARRAY_BUFFER, priv->capacity * sizeof(graphene_point3d_t), NULL, GL_DYNAMIC_DRAW);
  glEnableVertexAttribArray (0);
  glVertexAttribPointer (0, 3, GL_FLOAT, GL_FALSE, 0, 0);
  glBindBuffer (GL_ARRAY_BUFFER, 0);

  glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, priv->gl_buffers[INDEX_BUFFER]);
  glBufferData (GL_ELEMENT_ARRAY_BUFFER, priv->capacity * sizeof(guint), NULL, GL_DYNAMIC_DRAW);
  glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, 0);

  glBindVertexArray (0);
}

static void
forger_helper_render_node_set_property (GObject      *object,
                                        guint         prop_id,
                                        const GValue *value,
                                        GParamSpec   *pspec)
{
  ForgerHelperRenderNode *self = FORGER_HELPER_RENDER_NODE (object);
  ForgerHelperRenderNodePrivate *priv = forger_helper_render_node_get_instance_private (self);

  switch (prop_id)
    {
    case PROP_CAPACITY:
      priv->capacity = g_value_get_uint (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
forger_helper_render_node_class_init (ForgerHelperRenderNodeClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  ForgerRenderNodeClass *rnode_class = FORGER_RENDER_NODE_CLASS (klass);

  object_class->constructed = forger_helper_render_node_constructed;
  object_class->finalize = forger_helper_render_node_finalize;
  object_class->set_property = forger_helper_render_node_set_property;

  rnode_class->update_positions = forger_helper_render_node_real_update_positions;
  rnode_class->update_indices = forger_helper_render_node_real_update_indices;
  rnode_class->draw = forger_helper_render_node_real_draw;

  properties [PROP_CAPACITY] =
    g_param_spec_uint ("capacity", NULL, NULL,
                       0, G_MAXUINT, 1000,
                       (G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY |
                        G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
forger_helper_render_node_init (ForgerHelperRenderNode *self)
{
}

/**
 * forger_helper_render_node_new:
 *
 * Create a new #ForgerHelperRenderNode.
 *
 * Returns: (transfer full): a newly created #ForgerRenderNode
 */

ForgerRenderNode *
forger_helper_render_node_new (const guint n_max_entity)
{
  g_return_val_if_fail (n_max_entity > 0, NULL);
  return g_object_new (FORGER_TYPE_HELPER_RENDER_NODE,
                       "capacity", n_max_entity,
                       NULL);
}
