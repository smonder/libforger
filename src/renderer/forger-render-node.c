/*
 * forger-render-node.c
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "forger-render-node"

#include "config.h"
#include "forger-debug.h"

#include <glib/gi18n.h>

#include "forger-render-node.h"


G_DEFINE_ABSTRACT_TYPE (ForgerRenderNode, forger_render_node, FORGER_TYPE_ENTITY_COMPONENT)


/*
 * -----< FORGER_ENTITY_COMPONENT CLASS IMPLEMENTATION >----- *
 */
static const gchar *
forger_render_node_get_display_name (ForgerEntityComponent *component)
{
  return _("Graphics Node");
}

static const gchar *
forger_render_node_get_icon_name (ForgerEntityComponent *component)
{
  return "media-record-symbolic";
}


/*
 * -----< G_OBJECT CLASS IMPLEMENTATION >----- *
 */
static void
forger_render_node_class_init (ForgerRenderNodeClass *klass)
{
  ForgerEntityComponentClass *fec_class = FORGER_ENTITY_COMPONENT_CLASS (klass);

  fec_class->get_display_name = forger_render_node_get_display_name;
  fec_class->get_icon_name = forger_render_node_get_icon_name;
}

static void
forger_render_node_init (ForgerRenderNode *self)
{
}


/*
 * -----< VIRTUAL METHODS >----- *
 */
void
forger_render_node_update_positions (ForgerRenderNode *self,
                                     gconstpointer     data,
                                     const gsize       n_vertex)
{
  g_return_if_fail (FORGER_IS_RENDER_NODE (self));

  if (FORGER_RENDER_NODE_GET_CLASS (self)->update_positions != NULL)
    FORGER_RENDER_NODE_GET_CLASS (self)->update_positions (self, data, n_vertex);
}

void
forger_render_node_update_normals (ForgerRenderNode *self,
                                   gconstpointer     data,
                                   const gsize       n_vertex)
{
  g_return_if_fail (FORGER_IS_RENDER_NODE (self));

  if (FORGER_RENDER_NODE_GET_CLASS (self)->update_normals != NULL)
    FORGER_RENDER_NODE_GET_CLASS (self)->update_normals (self, data, n_vertex);
}

void
forger_render_node_update_uvmap (ForgerRenderNode *self,
                                 gconstpointer     data,
                                 const gsize       n_vertex)
{
  g_return_if_fail (FORGER_IS_RENDER_NODE (self));

  if (FORGER_RENDER_NODE_GET_CLASS (self)->update_uvmap != NULL)
    FORGER_RENDER_NODE_GET_CLASS (self)->update_uvmap (self, data, n_vertex);
}

void
forger_render_node_update_indices (ForgerRenderNode *self,
                                   gconstpointer     data,
                                   const gsize       n_index)
{
  g_return_if_fail (FORGER_IS_RENDER_NODE (self));

  if (FORGER_RENDER_NODE_GET_CLASS (self)->update_indices != NULL)
    FORGER_RENDER_NODE_GET_CLASS (self)->update_indices (self, data, n_index);
}


void
forger_render_node_draw (ForgerRenderNode *self)
{
  g_return_if_fail (FORGER_IS_RENDER_NODE (self));

  if (FORGER_RENDER_NODE_GET_CLASS (self)->draw != NULL)
    FORGER_RENDER_NODE_GET_CLASS (self)->draw (self);
}

