/* forger-engine.c
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "forger-engine"

#include "config.h"
#include "forger-debug.h"

#include <epoxy/gl.h>

#include "forger-engine.h"
#include "entity/forger-container.h"
#include "entity/forger-gizmo.h"
#include "material/forger-material.h"
#include "material/forger-shader-helper.h"
#include "forger-render-node.h"

struct _ForgerEngine
{
  GObject parent_instance;

  ForgerContext * context;
  GdkGLContext *  current_context;

  ForgerShader * selection_box_shader;
};

G_DEFINE_FINAL_TYPE (ForgerEngine, forger_engine, G_TYPE_OBJECT)

enum {
  PROP_0,
  PROP_CONTEXT,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

static void
opengl_message_callback (unsigned    source,
                         unsigned    type,
                         unsigned    id,
                         unsigned    severity,
                         int         length,
                         const char *message,
                         const void *userParam)
{
  switch (severity)
    {
		case GL_DEBUG_SEVERITY_HIGH:
      g_error ("FORGER Engine: %s", message);
      return;

		case GL_DEBUG_SEVERITY_MEDIUM:
      g_critical ("FORGER Engine: %s", message);
      return;

		case GL_DEBUG_SEVERITY_LOW:
      g_warning ("FORGER Engine: %s", message);
      return;

		case GL_DEBUG_SEVERITY_NOTIFICATION:
      g_message ("FORGER Engine: %s", message);
      return;

    default:
		  g_critical ("FORGER Engine: Unknown severity level!");
      return;
		}
}

static void
forger_engine_finalize (GObject *object)
{
  ForgerEngine *self = (ForgerEngine *)object;

  g_clear_object (&self->selection_box_shader);
  g_clear_object (&self->current_context);
  g_clear_object (&self->context);

  G_OBJECT_CLASS (forger_engine_parent_class)->finalize (object);
}

static void
forger_engine_get_property (GObject    *object,
                          guint       prop_id,
                          GValue     *value,
                          GParamSpec *pspec)
{
  ForgerEngine *self = FORGER_ENGINE (object);

  switch (prop_id)
    {
    case PROP_CONTEXT:
      g_value_set_object (value, forger_engine_get_context (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
forger_engine_set_property (GObject      *object,
                            guint         prop_id,
                            const GValue *value,
                            GParamSpec   *pspec)
{
  ForgerEngine *self = FORGER_ENGINE (object);

  switch (prop_id)
    {
    case PROP_CONTEXT:
      forger_engine_set_context (self, g_value_get_object (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
forger_engine_class_init (ForgerEngineClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = forger_engine_finalize;
  object_class->get_property = forger_engine_get_property;
  object_class->set_property = forger_engine_set_property;

  /**
   * ForgerEngine:context:
   *
   * The "context" property is the #ForgerContext that is currently
   * linked with %self.
   *
   * Since: 0.1
   */
  properties [PROP_CONTEXT] =
    g_param_spec_object ("context",
                         "Context",
                         "The ForgerContext to link with.",
                         FORGER_TYPE_CONTEXT,
                         (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
forger_engine_init (ForgerEngine *self)
{
}


/**
 * forger_engine_new:
 *
 * Creates a new #ForgerEngine for @context.
 *
 * Returns: a newly created #ForgerEngine.
 *
 * Since: 0.1
 */
ForgerEngine *
forger_engine_new (void)
{
  return g_object_new (FORGER_TYPE_ENGINE, NULL);
}

/**
 * forger_engine_get_context:
 * @self: a #ForgerEngine.
 *
 * Gets the currently linked #ForgerContext.
 *
 * Returns: (transfer full): a #ForgerContext or %NULL.
 *
 * Since: 0.1
 */
ForgerContext *
forger_engine_get_context (ForgerEngine *self)
{
  g_return_val_if_fail (FORGER_IS_ENGINE (self), NULL);

  return self->context;
}

void
forger_engine_set_context (ForgerEngine  *self,
                           ForgerContext *context)
{
  g_return_if_fail (FORGER_IS_ENGINE (self));
  g_return_if_fail (!context || FORGER_IS_CONTEXT (context));

  if (g_set_object (&self->context, context))
    g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_CONTEXT]);
}

/**
 * forger_engine_prepare:
 * @self: a #ForgerEngine.
 * @gl_context: the current #GdkGLContext.
 *
 * This function will wrap around all glEnable() calls and what
 * they need to be working properly.
 * This takes also a #GdkGLContext as an argument so that we can
 * make sure the engine will never perform if there is no current
 * GL Context.
 *
 * Since: 0.1
 */
void
forger_engine_prepare (ForgerEngine *self,
                       GdkGLContext *gl_context)
{
  GError *error = NULL;
  g_return_if_fail (FORGER_IS_ENGINE (self));
  g_return_if_fail (GDK_IS_GL_CONTEXT (gl_context));

  FORGER_ENTRY;

  if (g_set_object (&self->current_context, gl_context))
    {
      gdk_gl_context_make_current (self->current_context);

      glEnable (GL_BLEND);
      glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

      glEnable (GL_LINE_SMOOTH);
      glEnable (GL_DEPTH_TEST);

#ifdef ENABLE_TRACING
      gint context_flags = 0;

      glGetIntegerv (GL_CONTEXT_FLAGS, &context_flags);
      if (context_flags & GL_CONTEXT_FLAG_DEBUG_BIT)
        {
          g_message ("Running OpenGL in DEBUG mode");
          glEnable (GL_DEBUG_OUTPUT);
          glEnable (GL_DEBUG_OUTPUT_SYNCHRONOUS);
          glDebugMessageCallback (opengl_message_callback, NULL); /* callback, user_data */
          glDebugMessageControl (GL_DONT_CARE,                    /* source */
                                 GL_DONT_CARE,                    /* type */
                                 GL_DEBUG_SEVERITY_NOTIFICATION,  /* severity */
                                 0, NULL, GL_TRUE);
        }
#endif
    }

  self->selection_box_shader =
    forger_shader_helper_new ("Bounding_Box",
                              "/io/sam/libforger/helper-vertex.glsl",
                              "/io/sam/libforger/helper-pixel.glsl",
                              &error);

  if (!self->selection_box_shader)
    g_error ("Helper shader couldn't be initialized: %s", error->message);

  FORGER_EXIT;
}

/**
 * forger_engine_update_viewport:
 * @self: a #ForgerEngine.
 * @viewport: a #GdkRectangle: the viewport dimensions.
 *
 * Updates the glViewport and Camera viewport data.
 *
 * Since: 0.1
 */
void
forger_engine_update_viewport (ForgerEngine   *self,
                               GdkRectangle *viewport)
{
  g_return_if_fail (FORGER_IS_ENGINE (self));
  g_return_if_fail (viewport);

  glViewport (viewport->x,
              viewport->y,
              viewport->width,
              viewport->height);

  forger_camera_set_viewport (forger_context_get_active_camera (self->context), viewport);
}

/**
 * forger_engine_clear:
 * @self: a #ForgerEngine.
 * @color: a #GdkRGBA.
 *
 * Clears the canvas and set its background color to @color.
 *
 * Since: 0.1
 */
void
forger_engine_clear (ForgerEngine *self,
                     GdkRGBA    *color)
{
  g_return_if_fail (FORGER_IS_ENGINE (self));
  g_return_if_fail (color);

  glClearColor (color->red,
                color->green,
                color->blue,
                color->alpha);

  glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

/**
 * forger_engine_flush:
 * @self: a #ForgerEngine.
 *
 * A wrapper around glFlush() call to keep the API consistent.
 * The @self parameter is just to make sure that this function
 * will never be called if there is no #ForgerEngine.
 *
 * Since: 0.1
 */
void
forger_engine_flush (ForgerEngine * self)
{
  g_return_if_fail (FORGER_IS_ENGINE (self));

  glFlush ();
}

static ForgerEntityVisit
render_all_scene_entities_cb (ForgerEntity *entity,
                              gpointer      user_data)
{
  ForgerEngine *self = user_data;
  ForgerCamera *camera;
  ForgerGizmo *gizmo;
  ForgerMaterial *material;
  ForgerRenderNode *entity_node;
  ForgerRenderNode *edges_node;
  ForgerMaterial *edges_material;

  g_assert (FORGER_IS_ENGINE (self));
  g_assert (FORGER_IS_ENTITY (entity));

  if (!forger_entity_is_visible (entity) ||
      FORGER_IS_CAMERA (entity))
    return FORGER_ENTITY_VISIT_CONTINUE;

  if (FORGER_IS_CONTAINER (entity))
    return FORGER_ENTITY_VISIT_CHILDREN;

  camera = forger_context_get_active_camera (self->context);
  entity_node = FORGER_RENDER_NODE (forger_entity_get (entity, FORGER_COMPONENT_RENDER_NODE));
  gizmo = FORGER_GIZMO (forger_entity_get (entity, FORGER_COMPONENT_GIZMO));
  material = FORGER_MATERIAL (forger_entity_get (entity, FORGER_COMPONENT_MATERIAL));

  edges_node = FORGER_RENDER_NODE (forger_entity_get (entity, FORGER_COMPONENT_EDGES_RENDER_NODE));
  edges_material = FORGER_MATERIAL (forger_entity_get (entity, FORGER_COMPONENT_EDGES_MATERIAL));

  forger_entity_update_buffers (entity);

  forger_entity_component_prepare (FORGER_ENTITY_COMPONENT (material),
                                   forger_gizmo_get_matrix (gizmo),
                                   forger_camera_get_vpmatrix (camera));

  forger_render_node_draw (entity_node);

  if (edges_node &&
      edges_material)
    {
      forger_entity_component_prepare (FORGER_ENTITY_COMPONENT (edges_material),
                                       forger_gizmo_get_matrix (gizmo),
                                       forger_camera_get_vpmatrix (camera));
      forger_render_node_draw (edges_node);
    }

  return FORGER_ENTITY_VISIT_CONTINUE;
}

static void
foreach_selected_entity_render_cb (ForgerEntity *selected_entity,
                                   gpointer      user_data)
{
  ForgerEngine *self = user_data;
  ForgerCamera *camera;
  ForgerGizmo *gizmo;

  g_assert (FORGER_IS_ENGINE (self));
  g_assert (FORGER_IS_ENTITY (selected_entity));

  camera = forger_context_get_active_camera (self->context);
  gizmo = FORGER_GIZMO (forger_entity_get (selected_entity,
                                           FORGER_COMPONENT_GIZMO));
  forger_shader_bind (self->selection_box_shader);
  forger_shader_set_uniform_matrix (self->selection_box_shader,
                                    "u_ViewProjection",
                                    forger_camera_get_vpmatrix (camera));
  forger_shader_set_uniform_matrix (self->selection_box_shader,
                                    "u_Model",
                                    forger_gizmo_get_matrix (gizmo));
  forger_shader_set_uniform_color (self->selection_box_shader,
                                   "u_Color",
                                   &(GdkRGBA){0.996f, 0.145f, 0.004f, 1.0f});

  forger_entity_component_draw (forger_entity_get (selected_entity, FORGER_COMPONENT_BOUNDING_BOX));
  forger_shader_unbind (self->selection_box_shader);
}

/**
 * forger_engine_render:
 * @self: a #ForgerEngine.
 *
 * Makes @self perform a render call on all the
 * contents of "@self:context".
 *
 * Since: 0.1
 */
void
forger_engine_render (ForgerEngine *self)
{
  ForgerScene *current_scene;

  g_return_if_fail (FORGER_IS_ENGINE (self));

  if (!self->context)
    return;

  current_scene = forger_context_get_scene (self->context);
  forger_container_traverse (forger_scene_get_root (current_scene),
                             G_PRE_ORDER,
                             G_TRAVERSE_ALL,
                             -1,
                             render_all_scene_entities_cb,
                             self);

  forger_context_foreach_selected_entities (self->context,
                                            (GFunc)foreach_selected_entity_render_cb,
                                            self);
}

/**
 * forger_engine_get_glcontext:
 * @self: a #ForgerEngine
 *
 * Gets the active GLContext that %self is linked to.
 *
 * Returns: (transfer full) (nullable): a #GdkGLContext.
 */
GdkGLContext *
forger_engine_get_glcontext (ForgerEngine *self)
{
  g_return_val_if_fail (FORGER_IS_ENGINE (self), NULL);
  return self->current_context;
}

