/*
 * forger-spline-render-node.c
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "forger-spline-render-node"

#include "config.h"
#include "forger-debug.h"

#include <graphene.h>
#include <gdk/gdk.h>
#include <epoxy/gl.h>

#include "forger-spline-render-node.h"

enum
{
  VERTEX_POSITIONS,  /* The positions buffer */
  VERTEX_BVCOLOR,     /* The by-vertex color buffers */

  N_GL_BUFFER
};

typedef struct
{
  /* GL INFO */
  guint VAO;

  guint gl_buffers [N_GL_BUFFER];

  guint capacity;
  guint n_points;
  gboolean cyclic;
  guint8  line_width;
} ForgerSplineRenderNodePrivate;

G_DEFINE_TYPE_WITH_PRIVATE (ForgerSplineRenderNode, forger_spline_render_node, FORGER_TYPE_RENDER_NODE)

enum {
  PROP_0,
  PROP_CAPACITY,
  PROP_LINE_WIDTH,
  PROP_N_POINTS,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];


static void
forger_spline_render_node_real_draw (ForgerRenderNode * node)
{
  ForgerSplineRenderNode *self = (ForgerSplineRenderNode *)node;
  ForgerSplineRenderNodePrivate *priv = forger_spline_render_node_get_instance_private (self);

  g_assert (FORGER_IS_SPLINE_RENDER_NODE (self));

  glLineWidth (priv->line_width);
  glBindVertexArray (priv->VAO);
  glDrawArrays (priv->cyclic? GL_LINE_LOOP : GL_LINE_STRIP,
                0, priv->n_points);
  glBindVertexArray (0);
  glLineWidth (1);
}

static void
forger_spline_render_node_real_update_positions (ForgerRenderNode *node,
                                               gconstpointer     data,
                                               const gsize       n_vertex)
{
  ForgerSplineRenderNode *self = (ForgerSplineRenderNode *)node;
  ForgerSplineRenderNodePrivate *priv = forger_spline_render_node_get_instance_private (self);

  g_assert (FORGER_IS_SPLINE_RENDER_NODE (self));

  glBindVertexArray (priv->VAO);
  glBindBuffer (GL_ARRAY_BUFFER, priv->gl_buffers[VERTEX_POSITIONS]);
  glBufferSubData (GL_ARRAY_BUFFER, 0, n_vertex * sizeof(graphene_point3d_t), data);
  glBindBuffer (GL_ARRAY_BUFFER, 0);
  glBindVertexArray (0);
}

static void
forger_spline_render_node_real_update_normals (ForgerRenderNode *node,
                                             gconstpointer     data,
                                             const gsize       n_vertex)
{
  ForgerSplineRenderNode *self = (ForgerSplineRenderNode *)node;
  ForgerSplineRenderNodePrivate *priv = forger_spline_render_node_get_instance_private (self);

  g_assert (FORGER_IS_SPLINE_RENDER_NODE (self));

  glBindVertexArray (priv->VAO);
  glBindBuffer (GL_ARRAY_BUFFER, priv->gl_buffers[VERTEX_BVCOLOR]);
  glBufferSubData (GL_ARRAY_BUFFER, 0, n_vertex * sizeof(GdkRGBA), data);
  glBindBuffer (GL_ARRAY_BUFFER, 0);
  glBindVertexArray (0);
}

static void
forger_spline_render_node_finalize (GObject *object)
{
  ForgerSplineRenderNode *self = (ForgerSplineRenderNode *)object;
  ForgerSplineRenderNodePrivate *priv = forger_spline_render_node_get_instance_private (self);

  glDeleteBuffers (N_GL_BUFFER, priv->gl_buffers);
  glDeleteVertexArrays (1, &priv->VAO);

  G_OBJECT_CLASS (forger_spline_render_node_parent_class)->finalize (object);
}

static void
forger_spline_render_node_constructed (GObject *object)
{
  ForgerSplineRenderNode *self = (ForgerSplineRenderNode *)object;
  ForgerSplineRenderNodePrivate *priv = forger_spline_render_node_get_instance_private (self);

  G_OBJECT_CLASS (forger_spline_render_node_parent_class)->constructed (object);

  glGenVertexArrays (1, &priv->VAO);
  glBindVertexArray (priv->VAO);

  glGenBuffers (N_GL_BUFFER, priv->gl_buffers);

  glBindBuffer (GL_ARRAY_BUFFER, priv->gl_buffers[VERTEX_POSITIONS]);
  glBufferData (GL_ARRAY_BUFFER,
                priv->capacity * sizeof(graphene_point3d_t), NULL,
                GL_DYNAMIC_DRAW);
  glEnableVertexAttribArray (0);
  glVertexAttribPointer (0, 3, GL_FLOAT, GL_FALSE, 0, 0);
  glBindBuffer (GL_ARRAY_BUFFER, 0);

  glBindBuffer (GL_ARRAY_BUFFER, priv->gl_buffers[VERTEX_BVCOLOR]);
  glBufferData (GL_ARRAY_BUFFER,
                priv->capacity * sizeof(GdkRGBA), NULL,
                GL_DYNAMIC_DRAW);
  glEnableVertexAttribArray (1);
  glVertexAttribPointer (1, 4, GL_FLOAT, GL_FALSE, 0, 0);
  glBindBuffer (GL_ARRAY_BUFFER, 0);

  glBindVertexArray (0);
}

static void
forger_spline_render_node_get_property (GObject    *object,
                                        guint       prop_id,
                                        GValue     *value,
                                        GParamSpec *pspec)
{
  ForgerSplineRenderNode *self = FORGER_SPLINE_RENDER_NODE (object);

  switch (prop_id)
    {
    case PROP_LINE_WIDTH:
      g_value_set_uint (value, forger_spline_render_node_get_line_width (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
forger_spline_render_node_set_property (GObject      *object,
                                      guint         prop_id,
                                      const GValue *value,
                                      GParamSpec   *pspec)
{
  ForgerSplineRenderNode *self = FORGER_SPLINE_RENDER_NODE (object);
  ForgerSplineRenderNodePrivate *priv = forger_spline_render_node_get_instance_private (self);

  switch (prop_id)
    {
    case PROP_CAPACITY:
      priv->capacity = g_value_get_uint (value);
      break;

    case PROP_N_POINTS:
      forger_spline_render_node_set_n_point (self, g_value_get_uint (value));
      break;

    case PROP_LINE_WIDTH:
      forger_spline_render_node_set_line_width (self, g_value_get_uint (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
forger_spline_render_node_class_init (ForgerSplineRenderNodeClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  ForgerRenderNodeClass *rnode_class = FORGER_RENDER_NODE_CLASS (klass);

  object_class->constructed = forger_spline_render_node_constructed;
  object_class->finalize = forger_spline_render_node_finalize;
  object_class->get_property = forger_spline_render_node_get_property;
  object_class->set_property = forger_spline_render_node_set_property;

  rnode_class->update_positions = forger_spline_render_node_real_update_positions;
  rnode_class->update_normals = forger_spline_render_node_real_update_normals;
  rnode_class->draw = forger_spline_render_node_real_draw;

  properties [PROP_CAPACITY] =
    g_param_spec_uint ("capacity", NULL, NULL,
                       0, G_MAXUINT, 1000,
                       (G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY |
                        G_PARAM_STATIC_STRINGS));

  properties [PROP_LINE_WIDTH] =
    g_param_spec_uint ("line-width", NULL, NULL,
                       1, 7, 1,
                       (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  properties [PROP_N_POINTS] =
    g_param_spec_uint ("n-points", NULL, NULL,
                       0, G_MAXUINT, 0,
                       (G_PARAM_WRITABLE | G_PARAM_CONSTRUCT |
                        G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
forger_spline_render_node_init (ForgerSplineRenderNode *self)
{
  ForgerSplineRenderNodePrivate *priv = forger_spline_render_node_get_instance_private (self);
  priv->cyclic = FALSE;
  priv->line_width = 1;
}

/**
 * forger_spline_render_node_new:
 *
 * Create a new #ForgerSplineRenderNode.
 *
 * Returns: (transfer full): a newly created #ForgerRenderNode
 */

ForgerRenderNode *
forger_spline_render_node_new (const guint n_max_entity)
{
  g_return_val_if_fail (n_max_entity > 0, NULL);
  return g_object_new (FORGER_TYPE_SPLINE_RENDER_NODE,
                       "capacity", n_max_entity,
                       NULL);
}

gboolean
forger_spline_render_node_is_cyclic (ForgerSplineRenderNode *self)
{
  ForgerSplineRenderNodePrivate *priv = forger_spline_render_node_get_instance_private (self);

  g_return_val_if_fail (FORGER_IS_SPLINE_RENDER_NODE (self), FALSE);

  return priv->cyclic;
}

void
forger_spline_render_node_set_cyclic (ForgerSplineRenderNode *self,
                                    gboolean              setting)
{
  ForgerSplineRenderNodePrivate *priv = forger_spline_render_node_get_instance_private (self);

  g_return_if_fail (FORGER_IS_SPLINE_RENDER_NODE (self));

  priv->cyclic = setting;
}

guint
forger_spline_render_node_get_line_width (ForgerSplineRenderNode *self)
{
  ForgerSplineRenderNodePrivate *priv = forger_spline_render_node_get_instance_private (self);
  g_return_val_if_fail (FORGER_IS_SPLINE_RENDER_NODE (self), 0);
  return priv->line_width;
}

void
forger_spline_render_node_set_line_width (ForgerSplineRenderNode *self,
                                          const guint             value)
{
  ForgerSplineRenderNodePrivate *priv = forger_spline_render_node_get_instance_private (self);
  g_return_if_fail (FORGER_IS_SPLINE_RENDER_NODE (self));

  if (priv->line_width == value)
    return;

  priv->line_width = value;
  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_LINE_WIDTH]);
}

void
forger_spline_render_node_set_n_point (ForgerSplineRenderNode *self,
                                       const guint             setting)
{
  ForgerSplineRenderNodePrivate *priv = forger_spline_render_node_get_instance_private (self);
  g_return_if_fail (FORGER_IS_SPLINE_RENDER_NODE (self));
  priv->n_points = setting;
}
