/*
 * forger-mesh-render-node.c
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "forger-mesh-render-node"

#include "config.h"
#include "forger-debug.h"

#include <graphene.h>
#include <epoxy/gl.h>

#include "entity/forger-mesh-components.h"
#include "forger-mesh-render-node.h"

enum
{
  VERTEX_POSITIONS,
  VERTEX_NORMALS,
  VERTEX_UVMAP,
  INDEX_BUFFER,

  N_GL_BUFFER
};

typedef struct
{
  /* GL INFO */
  guint VAO;

  guint gl_buffers [N_GL_BUFFER];

  guint capacity;
} ForgerMeshRenderNodePrivate;

G_DEFINE_TYPE_WITH_PRIVATE (ForgerMeshRenderNode, forger_mesh_render_node, FORGER_TYPE_RENDER_NODE)

enum {
  PROP_0,
  PROP_CAPACITY,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];


static void
forger_mesh_render_node_real_draw (ForgerRenderNode * node)
{
  ForgerMeshRenderNode *self = (ForgerMeshRenderNode *)node;
  ForgerMeshRenderNodePrivate *priv = forger_mesh_render_node_get_instance_private (self);

  g_assert (FORGER_IS_MESH_RENDER_NODE (self));

  glBindVertexArray (priv->VAO);
  glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, priv->gl_buffers [INDEX_BUFFER]);
  glDrawElements (GL_TRIANGLES, priv->capacity, GL_UNSIGNED_INT, NULL);
  glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, 0);
  glBindVertexArray (0);
}

static void
forger_mesh_render_node_real_update_positions (ForgerRenderNode *node,
                                               gconstpointer     data,
                                               const gsize       n_vertex)
{
  ForgerMeshRenderNode *self = (ForgerMeshRenderNode *)node;
  ForgerMeshRenderNodePrivate *priv = forger_mesh_render_node_get_instance_private (self);

  g_assert (FORGER_IS_MESH_RENDER_NODE (self));

  glBindVertexArray (priv->VAO);
  glBindBuffer (GL_ARRAY_BUFFER, priv->gl_buffers[VERTEX_POSITIONS]);
  glBufferSubData (GL_ARRAY_BUFFER, 0, n_vertex * sizeof(graphene_point3d_t), data);
}

static void
forger_mesh_render_node_real_update_normals (ForgerRenderNode *node,
                                             gconstpointer     data,
                                             const gsize       n_vertex)
{
  ForgerMeshRenderNode *self = (ForgerMeshRenderNode *)node;
  ForgerMeshRenderNodePrivate *priv = forger_mesh_render_node_get_instance_private (self);

  g_assert (FORGER_IS_MESH_RENDER_NODE (self));

  glBindVertexArray (priv->VAO);
  glBindBuffer (GL_ARRAY_BUFFER, priv->gl_buffers[VERTEX_NORMALS]);
  glBufferSubData (GL_ARRAY_BUFFER, 0, n_vertex * sizeof(graphene_point3d_t), data);
}

static void
forger_mesh_render_node_real_update_uvmap (ForgerRenderNode *node,
                                           gconstpointer     data,
                                           const gsize       n_vertex)
{
  ForgerMeshRenderNode *self = (ForgerMeshRenderNode *)node;
  ForgerMeshRenderNodePrivate *priv = forger_mesh_render_node_get_instance_private (self);

  g_assert (FORGER_IS_MESH_RENDER_NODE (self));

  glBindVertexArray (priv->VAO);
  glBindBuffer (GL_ARRAY_BUFFER, priv->gl_buffers[VERTEX_UVMAP]);
  glBufferSubData (GL_ARRAY_BUFFER, 0, n_vertex * sizeof(graphene_point_t), data);
}

static void
forger_mesh_render_node_real_update_indices (ForgerRenderNode *node,
                                             gconstpointer     data,
                                             const gsize       n_index)
{
  ForgerMeshRenderNode *self = (ForgerMeshRenderNode *)node;
  ForgerMeshRenderNodePrivate *priv = forger_mesh_render_node_get_instance_private (self);

  g_assert (FORGER_IS_MESH_RENDER_NODE (self));

  glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, priv->gl_buffers[INDEX_BUFFER]);
  glBufferSubData (GL_ELEMENT_ARRAY_BUFFER, 0, n_index * sizeof(ForgerTriangle), data);
  glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, 0);
}

static void
forger_mesh_render_node_finalize (GObject *object)
{
  ForgerMeshRenderNode *self = (ForgerMeshRenderNode *)object;
  ForgerMeshRenderNodePrivate *priv = forger_mesh_render_node_get_instance_private (self);

  glDeleteBuffers (N_GL_BUFFER, priv->gl_buffers);
  glDeleteVertexArrays (1, &priv->VAO);

  G_OBJECT_CLASS (forger_mesh_render_node_parent_class)->finalize (object);
}

static void
forger_mesh_render_node_constructed (GObject *object)
{
  ForgerMeshRenderNode *self = (ForgerMeshRenderNode *)object;
  ForgerMeshRenderNodePrivate *priv = forger_mesh_render_node_get_instance_private (self);

  G_OBJECT_CLASS (forger_mesh_render_node_parent_class)->constructed (object);

  glGenVertexArrays (1, &priv->VAO);
  glBindVertexArray (priv->VAO);

  glGenBuffers (N_GL_BUFFER, priv->gl_buffers);

  glBindBuffer (GL_ARRAY_BUFFER, priv->gl_buffers[VERTEX_POSITIONS]);
  glBufferData (GL_ARRAY_BUFFER, priv->capacity * sizeof(graphene_point3d_t), NULL, GL_DYNAMIC_DRAW);
  glEnableVertexAttribArray (0);
  glVertexAttribPointer (0, 3, GL_FLOAT, GL_FALSE, 0, 0);
  glBindBuffer (GL_ARRAY_BUFFER, 0);

  glBindBuffer (GL_ARRAY_BUFFER, priv->gl_buffers[VERTEX_NORMALS]);
  glBufferData (GL_ARRAY_BUFFER, priv->capacity * sizeof(graphene_point3d_t), NULL, GL_DYNAMIC_DRAW);
  glEnableVertexAttribArray (1);
  glVertexAttribPointer (1, 3, GL_FLOAT, GL_FALSE, 0, 0);
  glBindBuffer (GL_ARRAY_BUFFER, 0);

  glBindBuffer (GL_ARRAY_BUFFER, priv->gl_buffers[VERTEX_UVMAP]);
  glBufferData (GL_ARRAY_BUFFER, priv->capacity * sizeof(graphene_point_t), NULL, GL_DYNAMIC_DRAW);
  glEnableVertexAttribArray (2);
  glVertexAttribPointer (2, 2, GL_FLOAT, GL_FALSE, 0, 0);
  glBindBuffer (GL_ARRAY_BUFFER, 0);

  glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, priv->gl_buffers[INDEX_BUFFER]);
  glBufferData (GL_ELEMENT_ARRAY_BUFFER, priv->capacity * sizeof(ForgerTriangle), NULL, GL_DYNAMIC_DRAW);
  glBindBuffer (GL_ELEMENT_ARRAY_BUFFER, 0);

  glBindVertexArray (0);
}

static void
forger_mesh_render_node_set_property (GObject      *object,
                                      guint         prop_id,
                                      const GValue *value,
                                      GParamSpec   *pspec)
{
  ForgerMeshRenderNode *self = FORGER_MESH_RENDER_NODE (object);
  ForgerMeshRenderNodePrivate *priv = forger_mesh_render_node_get_instance_private (self);

  switch (prop_id)
    {
    case PROP_CAPACITY:
      priv->capacity = g_value_get_uint (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
forger_mesh_render_node_class_init (ForgerMeshRenderNodeClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  ForgerRenderNodeClass *rnode_class = FORGER_RENDER_NODE_CLASS (klass);

  object_class->constructed = forger_mesh_render_node_constructed;
  object_class->finalize = forger_mesh_render_node_finalize;
  object_class->set_property = forger_mesh_render_node_set_property;

  rnode_class->update_positions = forger_mesh_render_node_real_update_positions;
  rnode_class->update_normals = forger_mesh_render_node_real_update_normals;
  rnode_class->update_uvmap = forger_mesh_render_node_real_update_uvmap;
  rnode_class->update_indices = forger_mesh_render_node_real_update_indices;
  rnode_class->draw = forger_mesh_render_node_real_draw;

  properties [PROP_CAPACITY] =
    g_param_spec_uint ("capacity", NULL, NULL,
                       0, G_MAXUINT, 1000,
                       (G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY |
                        G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
forger_mesh_render_node_init (ForgerMeshRenderNode *self)
{
}

/**
 * forger_mesh_render_node_new:
 *
 * Create a new #ForgerMeshRenderNode.
 *
 * Returns: (transfer full): a newly created #ForgerRenderNode
 */

ForgerRenderNode *
forger_mesh_render_node_new (const guint n_max_entity)
{
  g_return_val_if_fail (n_max_entity > 0, NULL);
  return g_object_new (FORGER_TYPE_MESH_RENDER_NODE,
                       "capacity", n_max_entity,
                       NULL);
}
