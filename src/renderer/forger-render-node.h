/*
 * forger-render-node.h
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#if !defined(FORGER_INSIDE) && !defined(LIBFORGER_COMPILATION)
#error "Only <libforger.h> can be included directly."
#endif

#include "entity/forger-entity-component.h"

#include "forger-version-macros.h"

G_BEGIN_DECLS

#define FORGER_TYPE_RENDER_NODE (forger_render_node_get_type())

FORGER_AVAILABLE_IN_ALL
G_DECLARE_DERIVABLE_TYPE (ForgerRenderNode, forger_render_node, FORGER, RENDER_NODE, ForgerEntityComponent)

struct _ForgerRenderNodeClass
{
  ForgerEntityComponentClass parent_class;

  void         (* update_positions) (ForgerRenderNode * node,
                                     gconstpointer      data,
                                     const gsize        n_vertex);
  void         (* update_normals)   (ForgerRenderNode * node,
                                     gconstpointer      data,
                                     const gsize        n_vertex);
  void         (* update_uvmap)     (ForgerRenderNode * node,
                                     gconstpointer      data,
                                     const gsize        n_vertex);
  void         (* update_indices)   (ForgerRenderNode * node,
                                     gconstpointer      data,
                                     const gsize        n_index);

  void         (* draw)             (ForgerRenderNode * node);
};


FORGER_AVAILABLE_IN_ALL
void         forger_render_node_update_positions  (ForgerRenderNode * self,
                                                   gconstpointer      data,
                                                   const gsize        n_vertex);
FORGER_AVAILABLE_IN_ALL
void         forger_render_node_update_normals    (ForgerRenderNode * self,
                                                   gconstpointer      data,
                                                   const gsize        n_vertex);
FORGER_AVAILABLE_IN_ALL
void         forger_render_node_update_uvmap      (ForgerRenderNode * self,
                                                   gconstpointer      data,
                                                   const gsize        n_vertex);
FORGER_AVAILABLE_IN_ALL
void         forger_render_node_update_indices    (ForgerRenderNode * self,
                                                   gconstpointer      data,
                                                   const gsize        n_index);

FORGER_AVAILABLE_IN_ALL
void         forger_render_node_draw              (ForgerRenderNode * self);

G_END_DECLS
