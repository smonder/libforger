/* forger-engine.h
 *
 * Copyright 2022 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#if !defined(FORGER_INSIDE) && !defined(LIBFORGER_COMPILATION)
#error "Only <libforger.h> can be included directly."
#endif

#include <gtk/gtk.h>

#include "forger-context.h"
#include "forger-version-macros.h"

G_BEGIN_DECLS

#define FORGER_TYPE_ENGINE (forger_engine_get_type())

FORGER_AVAILABLE_IN_ALL
G_DECLARE_FINAL_TYPE (ForgerEngine, forger_engine, FORGER, ENGINE, GObject)


FORGER_AVAILABLE_IN_ALL
ForgerEngine *       forger_engine_new             (void) G_GNUC_WARN_UNUSED_RESULT;

FORGER_AVAILABLE_IN_ALL
ForgerContext *      forger_engine_get_context     (ForgerEngine *  self);
FORGER_AVAILABLE_IN_ALL
void                 forger_engine_set_context     (ForgerEngine *  self,
                                                    ForgerContext * context);

FORGER_AVAILABLE_IN_ALL
GdkGLContext *       forger_engine_get_glcontext   (ForgerEngine *  self);

FORGER_AVAILABLE_IN_ALL
void                 forger_engine_prepare         (ForgerEngine *  self,
                                                    GdkGLContext *  gl_context);
FORGER_AVAILABLE_IN_ALL
void                 forger_engine_update_viewport (ForgerEngine *  self,
                                                    GdkRectangle *  viewport);
FORGER_AVAILABLE_IN_ALL
void                 forger_engine_clear           (ForgerEngine *  self,
                                                    GdkRGBA *       color);
FORGER_AVAILABLE_IN_ALL
void                 forger_engine_flush           (ForgerEngine *  self);
FORGER_AVAILABLE_IN_ALL
void                 forger_engine_render          (ForgerEngine *  self);


G_END_DECLS
