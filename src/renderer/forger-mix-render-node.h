/*
 * forger-mix-render-node.h
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#if !defined(FORGER_INSIDE) && !defined(LIBFORGER_COMPILATION)
#error "Only <libforger.h> can be included directly."
#endif

#include "forger-render-node.h"
#include "forger-version-macros.h"

G_BEGIN_DECLS

#define FORGER_TYPE_MIX_RENDER_NODE (forger_mix_render_node_get_type())

FORGER_AVAILABLE_IN_ALL
G_DECLARE_FINAL_TYPE (ForgerMixRenderNode, forger_mix_render_node, FORGER, MIX_RENDER_NODE, ForgerRenderNode)

FORGER_AVAILABLE_IN_ALL
ForgerRenderNode * forger_mix_render_node_new            (void) G_GNUC_WARN_UNUSED_RESULT;

FORGER_AVAILABLE_IN_ALL
void               forger_mix_render_node_append_child   (ForgerMixRenderNode * self,
                                                          ForgerRenderNode *    child);
FORGER_AVAILABLE_IN_ALL
void               forger_mix_render_node_prepend_child  (ForgerMixRenderNode * self,
                                                          ForgerRenderNode *    child);
FORGER_AVAILABLE_IN_ALL
void               forger_mix_render_node_insert_child   (ForgerMixRenderNode * self,
                                                          const gint            index_,
                                                          ForgerRenderNode *    child);
FORGER_AVAILABLE_IN_ALL
void               forger_mix_render_node_foreach_child  (ForgerMixRenderNode * self,
                                                          GFunc                 func,
                                                          gpointer              user_data);

G_END_DECLS
