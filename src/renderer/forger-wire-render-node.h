/*
 * forger-wire-render-node.h
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#if !defined(FORGER_INSIDE) && !defined(LIBFORGER_COMPILATION)
#error "Only <libforger.h> can be included directly."
#endif

#include "forger-mesh-render-node.h"
#include "forger-version-macros.h"

G_BEGIN_DECLS

#define FORGER_TYPE_WIRE_RENDER_NODE (forger_wire_render_node_get_type())

FORGER_AVAILABLE_IN_ALL
G_DECLARE_FINAL_TYPE (ForgerWireRenderNode, forger_wire_render_node, FORGER, WIRE_RENDER_NODE, ForgerMeshRenderNode)

FORGER_AVAILABLE_IN_ALL
ForgerRenderNode * forger_wire_render_node_new    (const guint n_max_entity) G_GNUC_WARN_UNUSED_RESULT;

FORGER_AVAILABLE_IN_ALL
guint              forger_wire_render_node_get_line_width (ForgerWireRenderNode * self);
FORGER_AVAILABLE_IN_ALL
void               forger_wire_render_node_set_line_width (ForgerWireRenderNode * self,
                                                           const guint            value);


G_END_DECLS
