/* libforger.h
 *
 * Copyright 2023 Salim Monder
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <glib.h>

G_BEGIN_DECLS

#define FORGER_INSIDE

# include "forger-canvas.h"
# include "forger-context.h"
# include "forger-enums.h"
# include "forger-types.h"

# include "entity/forger-bounding-box.h"
# include "entity/forger-camera.h"
# include "entity/forger-container.h"
# include "entity/forger-entity.h"
# include "entity/forger-entity-component.h"
# include "entity/forger-mesh.h"
# include "entity/forger-mesh-components.h"
# include "entity/forger-gizmo.h"
# include "entity/forger-raw-mesh.h"
# include "entity/forger-raw-spline.h"
# include "entity/forger-scene.h"
# include "entity/forger-vertex.h"

# include "material/forger-map.h"
# include "material/forger-map-image.h"
# include "material/forger-material.h"
# include "material/forger-material-diffuse.h"
# include "material/forger-material-error.h"
# include "material/forger-material-spline.h"
# include "material/forger-shader.h"
# include "material/forger-shader-helper.h"
# include "material/forger-shader-module.h"

# include "operations/forger-operation.h"
# include "operations/forger-mouse-picker.h"

# include "renderer/forger-edges-render-node.h"
# include "renderer/forger-engine.h"
# include "renderer/forger-helper-render-node.h"
# include "renderer/forger-mesh-render-node.h"
# include "renderer/forger-mix-render-node.h"
# include "renderer/forger-raw-mesh-render-node.h"
# include "renderer/forger-render-node.h"
# include "renderer/forger-spline-render-node.h"
# include "renderer/forger-wire-render-node.h"

#undef FORGER_INSIDE

G_END_DECLS
