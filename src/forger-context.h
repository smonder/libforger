/*
 * forger-context.h
 *
 * Copyright 2023 Salim Monder <salim.monder@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#if !defined(FORGER_INSIDE) && !defined(LIBFORGER_COMPILATION)
#error "Only <libforger.h> can be included directly."
#endif

#include <glib-object.h>

#include "entity/forger-camera.h"
#include "entity/forger-entity.h"
#include "entity/forger-container.h"
#include "entity/forger-scene.h"
#include "operations/forger-operation.h"

#include "forger-version-macros.h"

G_BEGIN_DECLS

#define FORGER_TYPE_CONTEXT (forger_context_get_type())

FORGER_AVAILABLE_IN_ALL
G_DECLARE_FINAL_TYPE (ForgerContext, forger_context, FORGER, CONTEXT, GObject)

FORGER_AVAILABLE_IN_ALL
ForgerContext *      forger_context_new               (void) G_GNUC_WARN_UNUSED_RESULT;
FORGER_AVAILABLE_IN_ALL
ForgerContext *      forger_context_new_for_scene     (ForgerScene *        scene) G_GNUC_WARN_UNUSED_RESULT;


FORGER_AVAILABLE_IN_ALL
ForgerScene *        forger_context_get_scene         (ForgerContext *      self);


FORGER_AVAILABLE_IN_ALL
void                 forger_context_foreach_selected_entities (ForgerContext * self,
                                                               GFunc           func_,
                                                               gpointer        user_data);
FORGER_AVAILABLE_IN_ALL
gboolean             forger_context_is_entity_selected        (ForgerContext * self,
                                                               ForgerEntity *  entity);

FORGER_AVAILABLE_IN_ALL
void                 forger_context_select_entity     (ForgerContext *      self,
                                                       ForgerEntity *       entity);
FORGER_AVAILABLE_IN_ALL
void                 forger_context_deselect_entity   (ForgerContext *      self,
                                                       ForgerEntity *       entity);
FORGER_AVAILABLE_IN_ALL
void                 forger_context_deselect_all      (ForgerContext *      self);

FORGER_AVAILABLE_IN_ALL
ForgerEntity *       forger_context_get_active_entity (ForgerContext *      self);

FORGER_AVAILABLE_IN_ALL
ForgerCamera *       forger_context_get_active_camera (ForgerContext *      self);
FORGER_AVAILABLE_IN_ALL
void                 forger_context_bind_camera       (ForgerContext *      self,
                                                       ForgerCamera *       camera);

FORGER_AVAILABLE_IN_ALL
ForgerOperation *    forger_context_get_operation     (ForgerContext *      self);
FORGER_AVAILABLE_IN_ALL
void                 forger_context_bind_operation    (ForgerContext *      self,
                                                       ForgerOperation *    op);

FORGER_AVAILABLE_IN_ALL
gboolean  forger_context_handle_event_scroll          (ForgerContext *            self,
                                                       GtkEventControllerScroll * scroll,
                                                       gdouble                    dx,
                                                       gdouble                    dy);
FORGER_AVAILABLE_IN_ALL
void      forger_context_handle_event_drag_update     (ForgerContext *            self,
                                                       GtkGestureDrag *           drag_gesture,
                                                       gdouble                    offset_x,
                                                       gdouble                    offset_y);
FORGER_AVAILABLE_IN_ALL
void      forger_context_handle_event_click_pressed   (ForgerContext *            self,
                                                       GtkGestureClick *          click_gesture,
                                                       gint                       n_press,
                                                       gdouble                    x,
                                                       gdouble                    y);
FORGER_AVAILABLE_IN_ALL
void      forger_context_handle_event_update          (ForgerContext  *           self,
                                                        gint64                    frame_time);

G_END_DECLS
