#version 440 core
// PIXEL_SHADER:
// diffuse-pixel.glsl

out vec4 a_Color;

uniform sampler2D u_MapSlot;

in vec3 v_Normal;
in vec2 v_UVMap;

uniform int u_UseMap;
uniform vec4 u_DiffuseColor;

void main ()
{
  if (u_UseMap == 0)
    a_Color = texture (u_MapSlot, v_UVMap);
  else
    a_Color = u_DiffuseColor;
}

