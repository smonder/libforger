#version 450
// PIXEL_SHADER:
// helper-pixel.glsl

out vec4 a_Color;
uniform vec4 u_Color;

void main ()
{
  a_Color = u_Color;
}
