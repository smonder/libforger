# LIBFORGER

Libforger is a graphics library that uses OpenGL as its backend, and is designed
for simple use and node based rendering.
It offers ECS and Material system out of the box.

LibForger can handle 2D/3D rendering operations, as well as, simple animation
and provides simple input events handling.

